﻿Public Class StockClass
    Dim dtset As StockSet
    Dim connect As ConnecDBRYH
    Dim typeStr As String

    Public Sub New(ByRef dt As StockSet, str As String)
        connect = ConnecDBRYH.NewConnection
        dtset = dt
        typeStr = str
    End Sub
    Public Sub StockUsePill(Start As Date, Todate As Date)
        Dim sql As String
        dtset.Tables("StockUse").Clear()
        If typeStr = "0" Then
            sql = "SELECT stock.stkid,frnprdstk.prdcode,code,prdname,qtytotal,date_format(dateprd,'%M-%y') as DATEPRD ,avg_cost,stkcode,stkname  FROM (SELECT stkid,prdcode,dateprd,QTYTOTAL FROM frnprdstk WHERE status = 1 AND dateprd >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateprd < '" & Start.Year & Todate.ToString("-MM-dd") & " 23:59:59" & "'  ) AS frnprdstk  JOIN masproduct ON frnprdstk.prdcode = masproduct.prdcode JOIN stock ON stock.stkid = frnprdstk.stkid;"
        ElseIf typeStr = "1" Then
            sql = "SELECT stock.stkid,frnprdstk.prdcode,code,prdname,qtytotal,date_format(dateprd,'%M-%y-%d') as DATEPRD ,avg_cost,stkcode,stkname  FROM (SELECT stkid,prdcode,dateprd,QTYTOTAL FROM frnprdstk WHERE status = 1  AND dateprd >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateprd < '" & Start.Year & Todate.ToString("-MM-dd") & " 23:59:59" & "' ) AS frnprdstk  JOIN masproduct ON frnprdstk.prdcode = masproduct.prdcode JOIN stock ON stock.stkid = frnprdstk.stkid"

        End If
        connect.GetTable(sql, dtset.Tables("StockUse"))

    End Sub
    Public Sub MasDrug()
        Dim sql As String
        dtset.Tables("MASDRUG").Clear()

        If typeStr = "0" Then
            sql = "SELECT drugitem.drgcode, drugitem.prdcode , genericname, tradename,  avg_cost , last_cost,opdprc,ipdprc FROM  (SELECT * FROM  masproduct WHERE status = 1 AND prdcat = 1  ) AS  masproduct JOIN (  SELECT * FROM  drugitem WHERE f_ms = 0)  AS drugitem  ON masproduct.prdcode = drugitem.prdcode"
        ElseIf typeStr = "1" Then
            sql = "SELECT drugitem.drgcode, drugitem.prdcode , genericname, tradename,  avg_cost , last_cost,opdprc,ipdprc FROM  (SELECT * FROM  masproduct WHERE status = 1 AND prdcat = 1  ) AS  masproduct JOIN (  SELECT * FROM  drugitem WHERE f_ms = 0)  AS drugitem  ON masproduct.prdcode = drugitem.prdcode;"
        End If
        connect.GetTable(sql, dtset.Tables("MASDRUG"))


    End Sub
    Public Sub MASMEDICAL()
        Dim sql As String
        dtset.Tables("MASDRUG").Clear()

        If typeStr = "0" Then
            sql = "SELECT drugitem.drgcode, drugitem.prdcode , genericname, tradename,  avg_cost , last_cost,opdprc,ipdprc FROM  (SELECT * FROM  masproduct WHERE status = 1 AND prdcat = 1  ) AS  masproduct JOIN (  SELECT * FROM  drugitem WHERE f_ms = 1)  AS drugitem  ON masproduct.prdcode = drugitem.prdcode"
        ElseIf typeStr = "1" Then
            sql = "SELECT drugitem.drgcode, drugitem.prdcode , genericname, tradename,  avg_cost , last_cost,opdprc,ipdprc FROM  (SELECT * FROM  masproduct WHERE  prdcat = 1  ) AS  masproduct JOIN (  SELECT * FROM  drugitem WHERE f_ms = 1)  AS drugitem  ON masproduct.prdcode = drugitem.prdcode;"
        End If
        connect.GetTable(sql, dtset.Tables("MASDRUG"))
    End Sub
    Public Sub MASVALIDDATE(Start As Date, Todate As Date)
        Dim sql As String
        dtset.Tables("StockValidate").Clear()
        sql = "SELECT stockonhand.prdcode,qtytotal,lotno,expdate,code,prdname,avg_cost,last_cost,opdprc,ipdprc FROM (SELECT * FROM  stockonhand WHERE qtytotal > 0  ) AS stockonhand JOIN ( SELECT * FROM  prdlotcon WHERE   expdate >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND expdate < '" & Start.Year & Todate.ToString("-MM-dd") & " 23:59:59" & "'  ) AS prdlotcon  ON  ( stockonhand.prdcode = prdlotcon.prdcode AND stockonhand.lotid = prdlotcon.lotid  ) JOIN masproduct  ON masproduct.prdcode = stockonhand.prdcode;"

        connect.GetTable(sql, dtset.Tables("StockValidate"))

    End Sub
    Public Sub MASEXPIRE()
        Dim sql As String
        dtset.Tables("StockValidate").Clear()
        sql = "SELECT stockonhand.prdcode,qtytotal,lotno,expdate,code,prdname,avg_cost,last_cost,opdprc,ipdprc FROM (SELECT * FROM  stockonhand WHERE qtytotal > 0  ) AS stockonhand JOIN ( SELECT * FROM  prdlotcon WHERE   expdate < NOW()  ) AS prdlotcon  ON stockonhand.prdcode = prdlotcon.prdcode AND stockonhand.lotid = prdlotcon.lotid JOIN masproduct  ON masproduct.prdcode = stockonhand.prdcode;"

        connect.GetTable(sql, dtset.Tables("StockValidate"))
    End Sub
End Class
