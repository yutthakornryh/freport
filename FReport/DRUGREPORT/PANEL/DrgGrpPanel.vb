﻿Public Class DrgGrpPanel
    Dim dtset As New DrgSet
    Dim analyze As New DRGCLASS(dtset, "")

    Private Sub DrgGrpPanel_Load(sender As Object, e As EventArgs) Handles Me.Load
        DateStart.EditValue = Now
        DateTo.EditValue = Now
        analyze.getGroupDrugCode()
        gridGroupDrugCode.DataSource = dtset

    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Dim Rows As New ArrayList()

        For i As Integer = 0 To GridView1.SelectedRowsCount - 1
            Rows.Add(GridView1.GetDataRow(GridView1.GetSelectedRows()(i)))
            analyze.getDrgPrdorderDt(DateStart.EditValue, DateTo.EditValue, Rows(i)("gdcode").ToString)

        Next
      

        GridControl2.DataSource = dtset
    End Sub
End Class
