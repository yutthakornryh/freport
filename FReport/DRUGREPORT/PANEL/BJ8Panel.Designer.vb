﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BJ8Panel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DocumentViewer = New DevExpress.XtraPrinting.Preview.DocumentViewer()
        Me.dfstarted = New DevExpress.XtraEditors.DateEdit()
        Me.dfFinished = New DevExpress.XtraEditors.DateEdit()
        Me.cbgrpdrug = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.btnApplyFilter = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        CType(Me.dfstarted.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dfstarted.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dfFinished.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dfFinished.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'DocumentViewer
        '
        Me.DocumentViewer.AutoSize = True
        Me.DocumentViewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DocumentViewer.IsMetric = True
        Me.DocumentViewer.Location = New System.Drawing.Point(2, 2)
        Me.DocumentViewer.Name = "DocumentViewer"
        Me.DocumentViewer.Size = New System.Drawing.Size(1044, 436)
        Me.DocumentViewer.TabIndex = 0
        Me.DocumentViewer.Zoom = 0.85!
        '
        'dfstarted
        '
        Me.dfstarted.EditValue = Nothing
        Me.dfstarted.Location = New System.Drawing.Point(109, 32)
        Me.dfstarted.Name = "dfstarted"
        Me.dfstarted.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dfstarted.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dfstarted.Size = New System.Drawing.Size(100, 20)
        Me.dfstarted.TabIndex = 1
        '
        'dfFinished
        '
        Me.dfFinished.EditValue = Nothing
        Me.dfFinished.Location = New System.Drawing.Point(307, 33)
        Me.dfFinished.Name = "dfFinished"
        Me.dfFinished.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dfFinished.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dfFinished.Size = New System.Drawing.Size(100, 20)
        Me.dfFinished.TabIndex = 2
        '
        'cbgrpdrug
        '
        Me.cbgrpdrug.DisplayMember = "Text"
        Me.cbgrpdrug.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cbgrpdrug.FormattingEnabled = True
        Me.cbgrpdrug.ItemHeight = 14
        Me.cbgrpdrug.Location = New System.Drawing.Point(145, 59)
        Me.cbgrpdrug.Name = "cbgrpdrug"
        Me.cbgrpdrug.Size = New System.Drawing.Size(262, 20)
        Me.cbgrpdrug.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.cbgrpdrug.TabIndex = 3
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LabelX1.Location = New System.Drawing.Point(28, 30)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 23)
        Me.LabelX1.TabIndex = 4
        Me.LabelX1.Text = "วันที่เริ่มต้น"
        '
        'PanelControl1
        '
        Me.PanelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelControl1.Appearance.Options.UseFont = True
        Me.PanelControl1.Controls.Add(Me.btnApplyFilter)
        Me.PanelControl1.Controls.Add(Me.LabelX3)
        Me.PanelControl1.Controls.Add(Me.LabelX2)
        Me.PanelControl1.Controls.Add(Me.LabelX1)
        Me.PanelControl1.Controls.Add(Me.dfstarted)
        Me.PanelControl1.Controls.Add(Me.cbgrpdrug)
        Me.PanelControl1.Controls.Add(Me.dfFinished)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1048, 100)
        Me.PanelControl1.TabIndex = 5
        '
        'btnApplyFilter
        '
        Me.btnApplyFilter.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnApplyFilter.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnApplyFilter.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnApplyFilter.Location = New System.Drawing.Point(437, 30)
        Me.btnApplyFilter.Name = "btnApplyFilter"
        Me.btnApplyFilter.Size = New System.Drawing.Size(85, 49)
        Me.btnApplyFilter.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.btnApplyFilter.TabIndex = 7
        Me.btnApplyFilter.Text = "แสดงรายงาน"
        Me.btnApplyFilter.TextColor = System.Drawing.Color.Black
        '
        'LabelX3
        '
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LabelX3.Location = New System.Drawing.Point(28, 55)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(111, 23)
        Me.LabelX3.TabIndex = 6
        Me.LabelX3.Text = "ประเภทวัตถุออกฤทธิ์"
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.LabelX2.Location = New System.Drawing.Point(226, 30)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(75, 23)
        Me.LabelX2.TabIndex = 5
        Me.LabelX2.Text = "วันที่สิ้นสุด"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.DocumentViewer)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(0, 100)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(1048, 440)
        Me.PanelControl2.TabIndex = 6
        '
        'BJ8Panel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Name = "BJ8Panel"
        Me.Size = New System.Drawing.Size(1048, 540)
        CType(Me.dfstarted.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dfstarted.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dfFinished.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dfFinished.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DocumentViewer As DevExpress.XtraPrinting.Preview.DocumentViewer
    Friend WithEvents dfstarted As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dfFinished As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbgrpdrug As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnApplyFilter As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl

End Class
