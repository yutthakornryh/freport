﻿Imports DevExpress.XtraReports.UI

Public Class DrgExtForm
    Dim report As XtraReport

    Dim bj8Panel As BJ8Panel
    Dim localPath As String = "F://FProject//"
    Dim reportname As String

    Private Sub DrgExtForm_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
    End Sub

    Private Sub NavBarItem1_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem1.LinkClicked

        bj8Panel = New BJ8Panel
        AddHandler bj8Panel.btnApplyFilter.Click, AddressOf bj8PanelShowing
        bj8Panel.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(bj8Panel)
        reportname = "bj8"
    End Sub

    Private Sub bj8PanelShowing(sender As Object, e As EventArgs)
        Me.report = bj8Panel.report
        'RemoveHandler bj8Panel.btnApplyFilter.Click, AddressOf bj8PanelShowing
    End Sub
    Private Sub PanelControl2_Paint(sender As Object, e As PaintEventArgs)

    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        If report IsNot Nothing Then
            ExportReport(report, localPath & reportname & ".xls", "xls")
        End If
    End Sub

    Private Sub BarButtonItem6_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        If report IsNot Nothing Then
            ExportReport(report, localPath & reportname & ".pdf", "pdf")
        End If
    End Sub

    Private Sub BarButtonItem3_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs)
        If report IsNot Nothing Then
            ExportReport(report, localPath & reportname & ".jpg", "jpg")
        End If
    End Sub

    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        If report IsNot Nothing Then
            Dim printTool As New ReportPrintTool(report)
            printTool.PrintDialog()
        End If
    End Sub

    Private Sub BarButtonItem4_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        If report IsNot Nothing Then
            Dim printTool As New ReportPrintTool(report)
            printTool.Print()
            printTool.PrintingSystem.ShowMarginsWarning = False
        End If
    End Sub


    Private Sub NavBarControl1_Click(sender As Object, e As EventArgs) Handles NavBarControl1.Click
        Dim medicalCertPanel As New MadicalCertPanel

        medicalCertPanel.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(medicalCertPanel)
        Me.report = medicalCertPanel.report
        reportname = "medicalCert"
    End Sub
End Class