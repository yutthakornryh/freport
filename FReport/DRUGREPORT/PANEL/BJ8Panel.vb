﻿Imports DevExpress.XtraReports.UI
Public Class BJ8Panel

    Dim getSql As SQLClass = New SQLClass
    Public Property report As XtraReport
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
       
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub ShowReport(ByVal startedDate As String, ByVal finishedDate As String, ByVal drgCategory As Integer)
        Dim sqlList As List(Of String) = getSql.getBj8QuerySQL(startedDate, finishedDate, drgCategory)
        report = New BJ8
        Dim masbj8 As REPORTCLASS = New REPORTCLASS(report, sqlList, "BJ8", {"ISSUEDDATE"})
        DocumentViewer.DocumentSource = report

    End Sub

    Private Function GetDate(ByVal df As DateTime) As String
        Dim dt As String
        dt = df.Year & df.ToString("-MM-dd")
        Return dt
    End Function

    Private Sub BJ8Panel_Load(sender As Object, e As EventArgs) Handles Me.Load
        dfstarted.EditValue = Date.Now.ToString("dd-MM-") & Date.Now.Year + 543
        dfFinished.EditValue = Date.Now.AddDays(1).ToString("dd-MM-") & (Date.Now.Year) + 543

        fillCategorytoComboBox()
    End Sub

    Private Sub fillCategorytoComboBox()
        Dim condb As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = "SELECT GDCODE, GRPDRG FROM masgrpdrg WHERE status=1;"
        Dim dtable As DataTable = condb.GetTable(sql)
        condb.Dispose()
        If dtable.Rows.Count > 0 Then
            cbgrpdrug.DataSource = dtable
            cbgrpdrug.DisplayMember = "GRPDRG"
            cbgrpdrug.ValueMember = "GDCODE"
        End If

    End Sub

    Private Sub btnApplyFilter_Click(sender As Object, e As EventArgs) Handles btnApplyFilter.Click
        ShowReport(GetDate(dfstarted.EditValue), GetDate(dfFinished.EditValue), cbgrpdrug.SelectedValue)
    End Sub
End Class
