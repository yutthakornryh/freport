﻿Imports System.Data
Imports System.Drawing.Printing
Imports System.Globalization

Public Class BJ8
    Dim totalQty As Decimal = 0
    Dim recieveQty As Decimal = 0
    Dim payQty As Decimal = 0

    Public Sub New()

        InitializeComponent()

    End Sub
    Private Sub XrTableCell35_BeforePrint(sender As Object, e As Printing.PrintEventArgs) Handles XrTableCell35.BeforePrint

        XrTableCell35.Text = getCurrentRemain()
        calculateTotalQty()
    End Sub

    Private Function getCurrentRemain() As String


        recieveQty = If(IsDBNull(GetCurrentRow().Row("PRDQL_RECIEVE")), 0, GetCurrentRow().Row("PRDQL_RECIEVE"))
        payQty = If(IsDBNull(GetCurrentRow().Row("PRDQL_PAY")), 0, GetCurrentRow().Row("PRDQL_PAY"))
        Return (totalQty + recieveQty - payQty).ToString("N0")
    End Function

    Private Sub calculateTotalQty()
        totalQty += (recieveQty - payQty)
    End Sub

    Private Sub XrTableCell33_BeforePrint(sender As Object, e As PrintEventArgs) Handles XrTableCell33.BeforePrint
        recieveQty = If(IsDBNull(GetCurrentRow().Row("PRDQL_RECIEVE")), 0, GetCurrentRow().Row("PRDQL_RECIEVE"))
        XrTableCell33.Text = recieveQty.ToString("N0") '& " " & GetCurrentRow().Row("UNIT").ToString
    End Sub

    Private Sub XrTableCell34_BeforePrint(sender As Object, e As PrintEventArgs) Handles XrTableCell34.BeforePrint
        recieveQty = If(IsDBNull(GetCurrentRow().Row("PRDQL_PAY")), 0, GetCurrentRow().Row("PRDQL_PAY"))
        XrTableCell34.Text = recieveQty.ToString("N0") '& " " & GetCurrentRow().Row("UNIT").ToString
    End Sub

    Private Sub XrTableCell26_BeforePrint(sender As Object, e As PrintEventArgs) Handles XrTableCell26.BeforePrint
        Dim isued_date As DateTime = If(IsDBNull(GetCurrentRow().Row("ISSUEDDATE")), Nothing, GetCurrentRow().Row("ISSUEDDATE"))
        If isued_date.Year < 1000 Then
            XrTableCell26.Text = " "
        Else
            XrTableCell26.Text = isued_date.ToString("dd/MM/") & isued_date.Year + 1086
        End If

    End Sub
End Class