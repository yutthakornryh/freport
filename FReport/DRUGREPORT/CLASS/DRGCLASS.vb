﻿Public Class DRGCLASS
    Dim dtset As DrgSet
    Dim connect As ConnecDBRYH
    Dim typeStr As String

    Public Sub New(ByRef dt As DrgSet, str As String)
        connect = ConnecDBRYH.NewConnection
        dtset = dt
        typeStr = str
    End Sub
    Public Sub getGroupDrugCode()
        Dim sql As String
        sql = "SELECT grpdrg,gdcode FROM masgrpdrg WHERE status = 1"
        connect.GetTable(sql, dtset.Tables("MASGRPDRG"))
    End Sub
    Public Sub getDrgPrdorderDt(Start As Date, ToDate As Date, GDCODE As String)
        Dim sql As String
        dtset.Tables("DrugOrder").Clear()
        sql = "SELECT prdcode FROM drugitem JOIN masgrpdrg ON drugitem.gdcode = masgrpdrg.gdcode WHERE masgrpdrg.gdcode =" & GDCODE & " ; "
        Dim dt As DataTable
        dt = connect.GetTable(sql)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim sql1 As String
            sql1 = "SELECT drugitem.drgcode, drugitem.prdcode , genericname, tradename, prdorderdt.hn,prdorderdt.orderid,CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name ,vn,an,d_order,unitname_th,qty FROM  (SELECT prdcode,orderid,hn,qty,unitid FROM  prdorderdt  WHERE prdcat = 1 AND f_cancel = 0 AND chkin_date >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND chkin_date  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "'  AND prdcode = " & dt.Rows(i)("prdcode").ToString & " ) AS prdorderdt  JOIN prdorder ON prdorder.orderid = prdorderdt.orderid JOIN (SELECT drgcode, prdcode , genericname, tradename FROM drugitem  )  AS drugitem  ON drugitem.prdcode = prdorderdt.prdcode JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON person.hn = prdorderdt.hn  JOIN masprename ON masprename.prename = person.prename JOIN masunit  ON masunit.unitid = prdorderdt.unitid ;"
            connect.GetTable(sql1, dtset.Tables("DrugOrder"))
        Next

    End Sub
End Class
