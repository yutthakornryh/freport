﻿Imports DevExpress.XtraReports.UI

Public Class REPORTCLASS
    Dim dataset As DataSet = New FDataSet
    Dim dview As DataView
    Dim dTable As DataTable
    Dim report As XtraReport
    Dim condb As ConnecDBRYH
    Public Sub New(ByRef report As XtraReport, ByVal sqlList As List(Of String), ByVal tname As String, ByVal sortBy() As String)
        Me.report = report
        dTable = New DataTable(tname)
        For Each sql As String In sqlList
            addData(sql)
        Next
        If dataset IsNot Nothing Then
            dataset.Tables.Remove(tname)  ' Remove the table from its dataset tables collection
        End If

        sortData(sortBy)
        dataset.Tables.Add(dTable)

        setdataSource()
    End Sub

    Private Sub addData(ByVal sql As String)
        condb = ConnecDBRYH.NewConnection

        'dataset.Tables(tablename)
        condb.GetTable(sql, dTable)
        condb.Dispose()
    End Sub
    Private Sub sortData(ByVal sortBy() As String)
        Dim sortColumn As String = ""

        For Each column As String In sortBy
            sortColumn += column & ", "
        Next

        sortColumn = sortColumn.Remove(sortColumn.Length - 2, 2)
        dview = dTable.DefaultView
        dview.Sort = sortColumn & " ASC"
        dTable = dview.ToTable
    End Sub
    Private Sub setdataSource()
        report.DataSource = dataset
        report.CreateDocument()
    End Sub

End Class
