﻿Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraPivotGrid
Imports DevExpress.XtraGrid
Imports DevExpress.XtraPrinting

Module ExportClass


    Public Sub ExportReport(ByVal report As XtraReport, ByVal fileName As String, ByVal fileType As String)
        report.ExportOptions.PrintPreview.ActionAfterExport = DevExpress.XtraPrinting.ActionAfterExport.Open
        If fileType = "xls" Then
            Dim xlsOptions As XlsExportOptions = report.ExportOptions.Xls
            xlsOptions.ShowGridLines = True
            xlsOptions.TextExportMode = TextExportMode.Value
            report.ExportToXlsx(fileName)
            StartProcess(fileName)

        End If
        If fileType = "pdf" Then
            report.ExportToPdf(fileName)
        End If
        If fileType = "rtf" Then
            report.ExportToRtf(fileName)
        End If
        If fileType = "csv" Then
            report.ExportToCsv(fileName)
        End If
        If fileType = "jpg" Then
            report.ExportToImage(fileName)
        End If
    End Sub
    Public Sub StartProcess(ByVal path As String)
        Dim process As New Process()
        Try
            process.StartInfo.FileName = path
            process.Start()
            process.WaitForInputIdle()
        Catch
        End Try
    End Sub
    Public Sub ExportReport(ByVal report As PivotGridControl, ByVal fileName As String, ByVal fileType As String)

        If fileType = "xls" Then
            report.ExportToXls(fileName)
        End If
        If fileType = "pdf" Then
            report.ExportToPdf(fileName)
        End If
        If fileType = "rtf" Then
            report.ExportToRtf(fileName)
        End If
        If fileType = "csv" Then
            report.ExportToCsv(fileName)
        End If
        If fileType = "jpg" Then
            report.ExportToImage(fileName)
        End If
    End Sub
End Module
