﻿Public Class SQLClass
    Public Function getBj8QuerySQL(ByVal startedDate As String, ByVal finishedDate As String, ByVal drgCategory As Integer) As List(Of String)
        Dim sqlList As List(Of String) = New List(Of String)
        Dim sql As String = ""
        'สั่งจ่ายยา
        sql += "SELECT DATE_FORMAT(porder.CHKIN_DATE, '%Y-%m-%d') as 'ISSUEDDATE', "
        sql += "prd.PRDNAME as 'PRDNAME', "
        sql += "lotcon.LOTNO as 'PRDLOTNO', "
        sql += "agent.COMPANY_NAME as 'PRDAGENT', "
        sql += "CONCAT(p.name, ' ', p.lname) as 'PERSON_NAME', "
        sql += "CAST( DATEDIFF(NOW(), p.BIRTH)/360.25 as SIGNED) as 'PERSON_AGE', "
        sql += "p.ADDRESDEC1 as 'PERSON_ADDR', "
        sql += "porder.QTY as 'PRDQL_PAY', "
        sql += "unit.UNITNAME_TH as 'UNIT' "
        sql += "FROM "
        sql += "( "
        sql += "SELECT id, PRDCODE, CHKIN_DATE, QTY, LOTID, ORDERID, UNITID "
        sql += "FROM PRDORDERDT "
        sql += ") as porder "
        sql += "JOIN masproduct as prd ON porder.PRDCODE = prd.PRDCODE "
        sql += "LEFT JOIN ( "
        sql += "SELECT PRDCODE, GDCODE "
        sql += "FROM drugitem ) as drug "
        sql += "ON prd.PRDCODE = drug.PRDCODE "
        sql += "LEFT JOIN masunit as unit "
        sql += "ON porder.UNITID = unit.UNITID "
        sql += "LEFT JOIN PRDLOTCON as lotcon ON porder.LOTID = lotcon.LOTID "
        sql += "LEFT JOIN ( "
        sql += "SELECT LOTID, DOCNO "
        sql += "FROM prdlandinglist) as landinglist "
        sql += "ON porder.LOTID = landinglist.LOTID "
        sql += "LEFT JOIN ( "
        sql += "SELECT * "
        sql += "FROM prdinvland as invland "
        sql += "	LEFT JOIN ( "
        sql += "		SELECT INVID as 'inv', AGENTCODE "
        sql += "		FROM prdinvoice) as invoice "
        sql += "	ON invland.INVID = invoice.inv "
        sql += "	WHERE invland.INVID = invoice.inv "
        sql += "	) as inv "
        sql += "	ON landinglist.DOCNO = inv.DOCNO "
        sql += "LEFT JOIN agent as agent "
        sql += "	ON inv.AGENTCODE = agent.AGENTCODE "
        sql += "LEFT JOIN ( "
        sql += "	SELECT ORDERID, HN "
        sql += "	FROM prdorder "
        sql += "	) as prdorder ON porder.ORDERID = prdorder.ORDERID "
        sql += "LEFT JOIN ("
        sql += "	SELECT person.name, "
        sql += "        person.lname,"
        sql += "        person.BIRTH,"
        sql += "        person.HN,"
        sql += "        addr.ADDRESDEC1 "
        sql += "        from "
        sql += "        person "
        sql += "	LEFT JOIN address as addr"
        sql += "	ON person.hn = addr.hn AND person.CONTACT_ADDRESS = addr.ADDRESSTYPE"
        sql += ") as p"
        sql += "	ON prdorder.HN = p.HN "
        sql += "WHERE drug.GDCODE =" & drgCategory & " AND porder.CHKIN_DATE BETWEEN '" & startedDate & "' AND " & "'" & finishedDate & "' "
        sql += "GROUP BY porder.id "
        sql += "ORDER BY porder.CHKIN_DATE ASC; "
        sqlList.Add(sql)

        'รับยา
        sql = "SELECT "
        sql += "		DATE_FORMAT(landing.DATEDOC, '%Y-%m-%d') as 'ISSUEDDATE', "
        sql += "		lot.LOTNO as 'PRDLOTNO', "
        sql += "		prgdt.PRDNAME as 'PRDNAME', "
        sql += "		agentdt.AGENT as 'PRDAGENT', "
        sql += "		landinglist.QUANTITY as 'PRDQL_RECIEVE', "
        sql += "        unit.UNITNAME_TH as 'UNIT' "
        sql += "FROM "
        sql += "	PRDLANDINGLIST as landinglist "
        sql += "	LEFT JOIN "
        sql += "	( "
        sql += "	SELECT * FROM masproduct as prd "
        sql += "	JOIN ( "
        sql += "		SELECT PRDCODE as 'drugcode', GDCODE "
        sql += "		FROM drugitem "
        sql += "	) as drug "
        sql += "	ON prd.PRDCODE = drug.drugcode "
        sql += "	) as prgdt "
        sql += "	ON landinglist.PRDCODE = prgdt.PRDCODE "

        sql += "   LEFT JOIN masunit as unit"
        sql += "    ON landinglist.unitcode = unit.UNITID "

        sql += "	LEFT JOIN( "
        sql += "		SELECT PRDCODE as 'lotprd', LOTNO, LOTID "
        sql += "		FROM prdlotcon "
        sql += "	) as lot "
        sql += "	ON landinglist.PRDCODE = lot.lotprd AND landinglist.LOTID = lot.LOTID "

        sql += "	JOIN ( "
        sql += "		SELECT DATEDOC , DOCNO as 'ldoc' "
        sql += "		FROM prdlanding "
        sql += "	) as landing "
        sql += "	ON landing.ldoc = landinglist.DOCNO "

        sql += "	LEFT JOIN ( "
        sql += "		SELECT prdinv.DOCNO, agent.COMPANY_NAME as 'AGENT' "
        sql += "		FROM "
        sql += "			(SELECT invland.DOCNO as 'DOCNO', inv.AGENTCODE as 'AGENTCODE' "
        sql += "			FROM "
        sql += "			prdinvland as invland "
        sql += "		JOIN "
        sql += "			PRDINVOICE as inv "
        sql += "		ON invland.INVID = inv.INVID "
        sql += "		) as prdinv "
        sql += "		JOIN "
        sql += "			agent as agent "
        sql += "		ON prdinv.AGENTCODE = agent.AGENTCODE "
        sql += "	) as agentdt "
        sql += "	ON agentdt.DOCNO = landinglist.DOCNO "
        sql += "WHERE prgdt.GDCODE =" & drgCategory & " AND landing.DATEDOC BETWEEN '" & startedDate & "' AND '" & finishedDate & "' "
        sql += "; "
        sqlList.Add(sql)

        Return sqlList
    End Function

    Public Function get_regsclinic_sql() As String
        Dim sql As String
        sql = "SELECT fservice.vn as 'VN', "
        sql += "fservice.hn as 'HN', "
        sql += "DATE_FORMAT(fservice.date_serv, '%d-%m-%Y') as 'SERVICE_DATE', "
        sql += "fclinic.clinic_name as 'CLINIC', "
        sql += "fclinic.rectime, "
        sql += "fclinic.sentime, "
        sql += "fclinic.fintime, "
        sql += "TIME_FORMAT(TIMEDIFF(fclinic.rectime, fclinic.sentime), '%H %i') as 'WAITING_TIME', "
        sql += "TIME_FORMAT(TIMEDIFF( fclinic.fintime, fclinic.rectime), '%H %i') as 'WORKING_TIME', "
        sql += "fclinic.doctor_name as 'DOCTOR', "
        sql += "CONCAT(p.pname, ' ', p.plname) as 'PATIENT' "
        sql += "from (  "
        sql += "		SELECT  "
        sql += "		f.vn, "
        sql += "		m.clinicname as 'CLINIC_NAME', "
        sql += "		f.clinic, "
        sql += "		f.rectime, "
        sql += "		f.sentime, "
        sql += "		f.fintime, "
        sql += "		f.clinicid, "
        sql += "		CONCAT(d.name, ' ', d.lname) as 'DOCTOR_NAME' "
        sql += "		FROM frnclinic as f "
        sql += "		JOIN masclinic as m ON f.clinic = m.clinic "
        sql += "		LEFT JOIN hospemp as d ON f.doctor = d.empid "
        sql += "        WHERE m.status = 1"
        sql += "	)as fclinic "
        sql += "LEFT JOIN frnservice as fservice "
        sql += "ON fservice.vn = fclinic.vn "
        sql += "LEFT JOIN ( "
        sql += "		SELECT name as 'pname', lname as 'plname',HN as 'pHN', CONTACT_ADDRESS  "
        sql += "		FROM person WHERE status=1 "
        sql += "		) as p "
        sql += "ON fservice.HN = p.pHN  "
        sql += "WHERE fservice.date_serv BETWEEN '2015-06-25' AND '2015-06-27'; "
        Return sql
    End Function

    Public Function getClinicServiceTime(ByVal startedDate As DateTime, ByVal finishedDate As DateTime) As String
        Dim stDate As String = startedDate.Year & startedDate.ToString("-%M-%d")
        Dim finDate As String = finishedDate.Year & finishedDate.ToString("-%M-%d")

        Dim sql As String
        sql = "SELECT "
        sql += "COUNT(DISTINCT fservice.vn) as 'VN_COUNT', "
        sql += "fservice.date_serv as 'SERVICE_DATE', "
        sql += "fclinic.clinic_name as 'CLINIC', "
        sql += "CAST(SUM(fclinic.SWAITING_TIME)/COUNT(DISTINCT fservice.vn) AS SIGNED) as 'waiting', "
        sql += "CAST(SUM(fclinic.SWORKING_TIME)/COUNT(DISTINCT fservice.vn) AS SIGNED) as 'working' "
        sql += "from ( "
        sql += "		SELECT "
        sql += "		f.vn, "
        sql += "		m.clinicname as 'CLINIC_NAME', "
        sql += "		f.clinic, "
        sql += "		TIMESTAMPDIFF(MINUTE , f.sentime,f.rectime ) as 'SWAITING_TIME', "
        sql += "		TIMESTAMPDIFF(MINUTE , f.rectime,f.fintime ) as 'SWORKING_TIME', "
        sql += "		f.clinicid, "
        sql += "		CONCAT(d.name, ' ', d.lname) as 'DOCTOR_NAME' "
        sql += "		FROM frnclinic as f "
        sql += "		JOIN masclinic as m ON f.clinic = m.clinic "
        sql += "		LEFT JOIN hospemp as d ON f.doctor = d.empid "
        sql += "        WHERE m.status = 1"
        sql += "	)as fclinic "
        sql += "JOIN ("
        sql += "	SELECT "
        sql += "		DATE_FORMAT(frnservice.date_serv, '%Y-%m-%d') as 'date_serv', "
        sql += "        frnservice.vn "
        sql += "	FROM "
        sql += "		frnservice as frnservice "
        sql += "    WHERE (frnservice.date_serv BETWEEN '" & stDate & "' AND '" & finDate & "') "
        sql += "	)as fservice "
        sql += "ON fservice.vn = fclinic.vn "
        sql += "GROUP BY fclinic.clinic, fservice.date_serv "
        sql += "ORDER BY fservice.date_serv ASC , fclinic.clinic ASC;"
        Return sql
    End Function

    Public Function testLargeSQL() As String
        Dim sql As String
        sql = "select order_name as 'LAB', count(order_id) as 'AMOUNT', order_date as 'DATE' "
        sql += "FROM worklist_order "
        sql += "WHERE isnull(order_date) = FALSE "
        sql += "GROUP BY order_code "
        sql += "ORDER BY amount ASC "
        sql += "LIMIT 100000; "
        Return sql
    End Function
End Class
