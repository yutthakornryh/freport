﻿Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraGrid.Views.Grid
Imports DevComponents.DotNetBar

Public Class DeptBillCheck
    Dim dtset As New AccountSet
    Dim dtset1 As New AccountSet
    Public Property report As XtraReport
    Dim debtClass As DebtClass
    Dim InvoiceCheck As Integer
    Dim strDebtStr As New DebtClass.fncDebt(0)
    Dim providerpolicies As New PROVIDERCLASS
    Dim checkLoginUser As Boolean = False

    Private _Cache As New MyCache("RCP")
    Private _Cache1 As New MyCache("RCP")
    Private _Cache2 As New MyCache("RCP")
    Private _Cache3 As New MyCache("RCP")

    Public Class MyCache

        Private ReadOnly _KeyFieldName As String
        Private valuesCache As New Dictionary(Of Object, Object)()

        Public Sub New(ByVal keyFieldName As String)
            _KeyFieldName = keyFieldName
        End Sub

        Public Function GetKeyByRow(ByVal row As Object) As Object
            Return (TryCast(row, DataRowView))(_KeyFieldName)
        End Function

        Public Function GetValue(ByVal row As Object) As Object
            Return GetValueByKey(GetKeyByRow(row))
        End Function

        Public Function GetValueByKey(ByVal key As Object) As Object
            Dim result As Object = Nothing
            valuesCache.TryGetValue(key, result)
            Return result
        End Function

        Public Sub SetValue(ByVal row As Object, ByVal value As Object)
            SetValueByKey(GetKeyByRow(row), value)
        End Sub

        Public Sub SetValueByKey(ByVal key As Object, ByVal value As Object)
            valuesCache(key) = value
        End Sub
    End Class
    Private Sub DeptBillCheck_Load(sender As Object, e As EventArgs) Handles Me.Load


        debtClass = New DebtClass(dtset)

        debtClass.getAgent()
        debtClass.getBank()
        debtClass.getAccept()
        GridControl1.DataSource = dtset1



        txtType.Properties.DataSource = dtset.Tables("acceptm")
        txtType.Properties.DisplayMember = "accm_name"
        txtType.Properties.ValueMember = "accmid"
        Dim coll2 As LookUpColumnInfoCollection = txtType.Properties.Columns
        coll2.Add(New LookUpColumnInfo("accmid", 0))
        coll2.Add(New LookUpColumnInfo("accm_name", 0))
        coll2.Add(New LookUpColumnInfo("accm_name_en", 0))
        txtType.Properties.BestFitMode = BestFitMode.BestFitResizePopup
        txtType.Properties.SearchMode = SearchMode.AutoComplete
        txtType.Properties.AutoSearchColumnIndex = 1






        txtBank.Properties.DataSource = dtset.Tables("BANK")
        txtBank.Properties.DisplayMember = "bank_name"
        txtBank.Properties.ValueMember = "bankid"

        Dim coll As LookUpColumnInfoCollection = txtBank.Properties.Columns
        coll.Add(New LookUpColumnInfo("bankid", 0))
        coll.Add(New LookUpColumnInfo("code", 0))
        coll.Add(New LookUpColumnInfo("bank_name", 0))
        txtBank.Properties.BestFitMode = BestFitMode.BestFitResizePopup
        txtBank.Properties.SearchMode = SearchMode.AutoComplete
        txtBank.Properties.AutoSearchColumnIndex = 1




        txtCompany.Properties.DataSource = dtset.Tables("Agent")
        txtCompany.Properties.DisplayMember = "company_name"
        txtCompany.Properties.ValueMember = "agentcode"
        Dim col3 As LookUpColumnInfoCollection = txtCompany.Properties.Columns
        col3.Add(New LookUpColumnInfo("agentcode", 0))
        col3.Add(New LookUpColumnInfo("company_name", 0))

        txtCompany.Properties.BestFitMode = BestFitMode.BestFitResizePopup
        txtCompany.Properties.SearchMode = SearchMode.AutoComplete
        txtCompany.Properties.AutoSearchColumnIndex = 1

        debtClass.GetDebType()
        txtDebtType.Properties.DataSource = dtset.Tables("DEBTCODE")
        txtDebtType.Properties.DisplayMember = "debtname"
        txtDebtType.Properties.ValueMember = "debtype"

        Dim col4 As LookUpColumnInfoCollection = txtDebtType.Properties.Columns
        col4.Add(New LookUpColumnInfo("debtype", 0))
        col4.Add(New LookUpColumnInfo("debtcode", 0))
        col4.Add(New LookUpColumnInfo("debtname", 0))

        txtDebtType.Properties.BestFitMode = BestFitMode.BestFitResizePopup
        txtDebtType.Properties.SearchMode = SearchMode.AutoComplete
        txtDebtType.Properties.AutoSearchColumnIndex = 1

    End Sub

    Private Sub txtCompany_EditValueChanged(sender As Object, e As EventArgs) Handles txtCompany.EditValueChanged
        getInvoice()
    End Sub
    Public Sub getInvoice()
        dtset1.Tables("DOCINV").Clear()
        debtClass.GetInvoice(txtCompany.EditValue)
        GridControl2.DataSource = dtset
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If providerpolicies.PADD_ = True Then
        Else
            TOASTCLASS.gloadToastMSG(Me, providerpolicies.PADDMSG_)

            '  txtUserRequest.Focus = True


            Dim amount As Decimal
            Dim discount As Decimal
            Dim total As Decimal
            Dim tax As Decimal

            For i As Integer = 0 To dtset1.Tables("DOCINV").Rows.Count - 1
                strDebtStr.invdocid = dtset1.Tables("DOCINV")(i)("invdocid")
                strDebtStr.mtrgtid = dtset1.Tables("DOCINV")(i)("mtrgtid")
                strDebtStr.strgtid = If(IsDBNull(dtset1.Tables("DOCINV")(i)("strgtid")), "null", dtset1.Tables("DOCINV")(i)("strgtid"))
                strDebtStr.sbrgtid = If(IsDBNull(dtset1.Tables("DOCINV")(i)("sbrgtid")), "null", dtset1.Tables("DOCINV")(i)("sbrgtid"))
                strDebtStr.ptrgtid = If(IsDBNull(dtset1.Tables("DOCINV")(i)("ptrgtid")), "null", dtset1.Tables("DOCINV")(i)("ptrgtid"))
                strDebtStr.pconid = If(IsDBNull(dtset1.Tables("DOCINV")(i)("pconid")), "null", dtset1.Tables("DOCINV")(i)("pconid"))
                If txtDebtType.EditValue Is Nothing Then
                    MsgBox("กรุณาเลือกประเภทการชำระ")
                    Exit Sub
                Else
                    strDebtStr.debtype = txtDebtType.EditValue
                End If

                If txtType.EditValue Is Nothing Then
                    MsgBox("กรุณาเลือกประเภทการชำระ")
                    Exit Sub
                Else
                    strDebtStr.ACCMID = txtType.EditValue
                End If

                If txtBank.EditValue Is Nothing Then
                    strDebtStr.BANKID = "null"
                Else
                    strDebtStr.BANKID = txtBank.EditValue
                End If
                If txtSN.Text.Trim = "" Then
                    strDebtStr.SN = "null"
                Else
                    strDebtStr.SN = txtSN.Text
                End If
                If Convert.ToDouble(dtset1.Tables("DOCINV")(i)("bill")) > 0 Then
                    strDebtStr.amount = Convert.ToDouble(dtset1.Tables("DOCINV")(i)("bill"))
                Else
                    MsgBox("กรุณา กรอกจำนวนเงินมากกว่า 0")
                    Exit Sub
                End If
                If IsNumeric(dtset1.Tables("DOCINV")(i)("discountT")) Then
                    strDebtStr.discount = dtset1.Tables("DOCINV")(i)("discountT")
                Else
                    MsgBox("ช่องส่วนลด ไมได้กรอกตัวเลข")
                    Exit Sub
                End If
                If IsNumeric(dtset1.Tables("DOCINV")(i)("tax")) Then
                    strDebtStr.vat = dtset1.Tables("DOCINV")(i)("tax")
                Else
                    MsgBox("ช่องส่วนลด ไมได้กรอกตัวเลข")
                    Exit Sub
                End If
                strDebtStr.usmk = Convert.ToString(txtUserRequest.Tag)


                strDebtStr.nameAgent = "ได้รับเงินจาก " + txtCompany.Text
                strDebtStr.TypeBill = txtType.Text
                Dim mne As New MoneyExt


                If txtBank.Text = "เลือกธนาคาร" Then
                    strDebtStr.nameBank = ""
                Else

                    strDebtStr.nameBank = txtType.Text + "  " + txtBank.Text + " เลขที่ " + txtSN.Text

                End If
                strDebtStr.nameUser = txtUsername.Text
                If Convert.ToDouble(dtset1.Tables("DOCINV")(i)("amount")) >= Convert.ToDouble(dtset1.Tables("DOCINV")(i)("Bill")) + Convert.ToDouble(dtset1.Tables("DOCINV")(i)("discountT")) + Convert.ToDouble(dtset1.Tables("DOCINV")(i)("Tax")) Then



                    total += dtset1.Tables("DOCINV")(i)("Bill")
                    tax += dtset1.Tables("DOCINV")(i)("Tax")
                    amount += dtset1.Tables("DOCINV")(i)("amount")
                    discount += dtset1.Tables("DOCINV")(i)("discountT")

                    strDebtStr.fonthai = mne.NumberToThaiWord(total)

                    debtClass.setReceipt = strDebtStr
                    debtClass.SetDebt()
                    debtClass.setFrnDebt()
                    debtClass.UpdateInvoice()
                    debtClass.InsertReceipt()
                Else
                    MsgBox("ยอดชำระเกิน")

                End If





            Next

            Dim nextform As DebtBillPrintReport = New DebtBillPrintReport(debtClass, dtset1)
            nextform.ShowDialog()
            getInvoice()
            ClearForm()

        End If


    End Sub


    Private Sub btnChoose_Click(sender As Object, e As EventArgs) Handles btnChoose.Click
        Dim sql As String

        Dim row As DataRow
        row = GridView2.GetDataRow(GridView2.FocusedRowHandle)
        Dim Check As Boolean = False
        For i As Integer = 0 To dtset1.Tables("DOCINV").Rows.Count - 1

            If dtset1.Tables("DOCINV").Rows(i)("invdocid") = row("invdocid") Then
                Check = True
                Exit For
            Else
                Check = False
            End If

        Next
        If Check = False Then
            dtset1.Tables("DOCINV").ImportRow(row)

        End If

      

    End Sub




    Private Sub txtUserReq_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtUserRequest.KeyPress
        If e.KeyChar = Convert.ToChar(13) Then
            providerpolicies.GETHOSEM(CStr(txtUserRequest.Text.Trim), Convert.ToString(Me.Tag))
            If providerpolicies._CHECKUSRFAIL = False Then
                SETUIUSERREQ()
                checkLoginUser = True
            Else
                checkLoginUser = False
                SETUIUSERREQ()
                txtUserRequest.Text = ""
                MsgBox(providerpolicies._CHECKUSRFAILMSG)
            End If

        End If
    End Sub
    'SET UI แสดงผล
    Public Sub SETUIUSERREQ()
        txtUsername.Text = providerpolicies.txtusername_
        txtUsername.Tag = providerpolicies.txtusername_tag_
        ' btnLogout.Visible = PROVIDERPOLICIES.btnlogout_

    End Sub
    Public Sub ClearForm()


        ' txtInvDocid.Text = Nothing
        ' txtName.Text = Nothing
        'txtamount.Text = Nothing
        'txtTotal.Text = Nothing
        'txtDateDoc.Text = Nothing
        'txtBill.Text = Nothing
        'txtDiscount.Text = Nothing
        'txtTax.Text = Nothing
        txtSN.Text = Nothing
        txtDebtType.EditValue = Nothing
        txtType.EditValue = Nothing
        txtBank.EditValue = Nothing
        dtset1.Tables("DOCINV").Clear()


    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        txtBank.EditValue = Nothing
    End Sub

    Private Sub GridControl1_ProcessGridKey(sender As Object, e As KeyEventArgs) Handles GridControl1.ProcessGridKey
        If e.KeyCode = Keys.Enter Then
            TryCast(GridControl1.FocusedView, ColumnView).FocusedRowHandle += 1
            e.Handled = True
        ElseIf e.KeyCode = Keys.Down Then
            TryCast(GridControl1.FocusedView, ColumnView).FocusedRowHandle += 1
            e.Handled = True

        ElseIf e.KeyCode = Keys.Up Then
            TryCast(GridControl1.FocusedView, ColumnView).FocusedRowHandle -= 1
            e.Handled = True

        End If


    End Sub

    Public Class MoneyExt

        Public Function NumberToThaiWord(ByVal InputNumber As Double) As String
            If InputNumber = 0 Then
                NumberToThaiWord = "ศูนย์บาทถ้วน"
                Return NumberToThaiWord
            End If

            Dim NewInputNumber As String
            NewInputNumber = InputNumber.ToString("###0.00")

            If CDbl(NewInputNumber) >= 10000000000000 Then
                NumberToThaiWord = ""
                Return NumberToThaiWord
            End If

            Dim tmpNumber(2) As String
            Dim FirstNumber As String
            Dim LastNumber As String

            tmpNumber = NewInputNumber.Split(CChar("."))
            FirstNumber = tmpNumber(0)
            LastNumber = tmpNumber(1)

            Dim nLength As Integer = 0
            nLength = CInt(FirstNumber.Length)

            Dim i As Integer
            Dim CNumber As Integer = 0
            Dim CNumberBak As Integer = 0
            Dim strNumber As String = ""
            Dim strPosition As String = ""
            Dim FinalWord As String = ""
            Dim CountPos As Integer = 0

            For i = nLength To 1 Step -1
                CNumberBak = CNumber
                CNumber = CInt(FirstNumber.Substring(CountPos, 1))

                If CNumber = 0 AndAlso i = 7 Then
                    strPosition = "ล้าน"
                ElseIf CNumber = 0 Then
                    strPosition = ""
                Else
                    strPosition = PositionToText(i)
                End If

                If CNumber = 2 AndAlso strPosition = "สิบ" Then
                    strNumber = "ยี่"
                ElseIf CNumber = 1 AndAlso strPosition = "สิบ" Then
                    strNumber = ""
                ElseIf CNumber = 1 AndAlso strPosition = "ล้าน" AndAlso nLength >= 8 Then
                    If CNumberBak = 0 Then
                        strNumber = "หนึ่ง"
                    Else
                        strNumber = "เอ็ด"
                    End If
                ElseIf CNumber = 1 AndAlso strPosition = "" AndAlso nLength > 1 Then
                    strNumber = "เอ็ด"
                Else
                    strNumber = NumberToText(CNumber)
                End If

                CountPos = CountPos + 1

                FinalWord = FinalWord & strNumber & strPosition
            Next

            CountPos = 0
            CNumberBak = 0
            nLength = CInt(LastNumber.Length)

            Dim Stang As String = ""
            Dim FinalStang As String = ""

            If CDbl(LastNumber) > 0.0 Then
                For i = nLength To 1 Step -1
                    CNumberBak = CNumber
                    CNumber = CInt(LastNumber.Substring(CountPos, 1))

                    If CNumber = 1 AndAlso i = 2 Then
                        strPosition = "สิบ"
                    ElseIf CNumber = 0 Then
                        strPosition = ""
                    Else
                        strPosition = PositionToText(i)
                    End If

                    If CNumber = 2 AndAlso strPosition = "สิบ" Then
                        Stang = "ยี่"
                    ElseIf CNumber = 1 AndAlso i = 2 Then
                        Stang = ""
                    ElseIf CNumber = 1 AndAlso strPosition = "" AndAlso nLength > 1 Then
                        If CNumberBak = 0 Then
                            Stang = "หนึ่ง"
                        Else
                            Stang = "เอ็ด"
                        End If
                    Else
                        Stang = NumberToText(CNumber)
                    End If

                    CountPos = CountPos + 1

                    FinalStang = FinalStang & Stang & strPosition
                Next

                FinalStang = FinalStang & "สตางค์"
            Else
                FinalStang = ""
            End If

            Dim SubUnit As String
            If FinalStang = "" Then
                SubUnit = "บาทถ้วน"
            Else
                If CDbl(FirstNumber) <> 0 Then
                    SubUnit = "บาท"
                Else
                    SubUnit = ""
                End If
            End If

            NumberToThaiWord = FinalWord & SubUnit & FinalStang
        End Function

        Private Function NumberToText(ByVal CurrentNum As Integer) As String
            Dim _nText As String = ""

            Select Case CurrentNum
                Case 0
                    _nText = ""
                Case 1
                    _nText = "หนึ่ง"
                Case 2
                    _nText = "สอง"
                Case 3
                    _nText = "สาม"
                Case 4
                    _nText = "สี่"
                Case 5
                    _nText = "ห้า"
                Case 6
                    _nText = "หก"
                Case 7
                    _nText = "เจ็ด"
                Case 8
                    _nText = "แปด"
                Case 9
                    _nText = "เก้า"
            End Select

            NumberToText = _nText
        End Function

        Private Function PositionToText(ByVal CurrentPos As Integer) As String
            Dim _nPos As String = ""

            Select Case CurrentPos
                Case 0
                    _nPos = ""
                Case 1
                    _nPos = ""
                Case 2
                    _nPos = "สิบ"
                Case 3
                    _nPos = "ร้อย"
                Case 4
                    _nPos = "พัน"
                Case 5
                    _nPos = "หมื่น"
                Case 6
                    _nPos = "แสน"
                Case 7
                    _nPos = "ล้าน"
                Case 8
                    _nPos = "สิบ"
                Case 9
                    _nPos = "ร้อย"
                Case 10
                    _nPos = "พัน"
                Case 11
                    _nPos = "หมื่น"
                Case 12
                    _nPos = "แสน"
                Case 13
                    _nPos = "ล้าน"
            End Select

            PositionToText = _nPos
        End Function
    End Class

    Private Sub BtnDelete_Click(sender As Object, e As EventArgs) Handles BtnDelete.Click
        If GridView1.GetFocusedRowCellValue("invdocid") Is Nothing Then

        Else
            GridView1.DeleteRow(GridView1.FocusedRowHandle)
            GridView1.RefreshData()
        End If
        dtset1.AcceptChanges()
    End Sub
End Class

Public Class TOASTCLASS

    Public Shared Sub gloadToastMSG(ByVal form As Form, ByVal msg As String)
        Dim glow As eToastGlowColor = eToastGlowColor.Blue
        Dim pos As eToastPosition = eToastPosition.TopCenter
        ToastNotification.Show(form, msg + "   ", My.Resources.version_32x32, 5 * 1000, glow, pos)
    End Sub
    Public Shared Sub gloadToastMSG(ByVal form As UserControl, ByVal msg As String)
        Dim glow As eToastGlowColor = eToastGlowColor.Blue
        Dim pos As eToastPosition = eToastPosition.TopCenter
        ToastNotification.Show(form, msg + "   ", My.Resources.version_32x32, 5 * 1000, glow, pos)
    End Sub
    Public Shared Sub SuccesToastMSG_TOP(ByVal form As Form, ByVal msg As String)
        Dim glow As eToastGlowColor = eToastGlowColor.Blue
        Dim pos As eToastPosition = eToastPosition.TopCenter
        ToastNotification.Show(form, msg + "   ", My.Resources.version_32x32, 5 * 1000, glow, pos)
    End Sub

    Public Shared Sub SuccesToastMSG_DOWN(ByVal form As Form, ByVal msg As String)
        Dim glow As eToastGlowColor = eToastGlowColor.Blue
        Dim pos As eToastPosition = eToastPosition.BottomCenter
        ToastNotification.Show(form, msg + "   ", My.Resources.version_32x32, 5 * 1000, glow, pos)
    End Sub

    Public Shared Sub WarningToastMSG_DOWN(ByVal form As Form, ByVal msg As String)
        Dim glow As eToastGlowColor = eToastGlowColor.Blue
        Dim pos As eToastPosition = eToastPosition.BottomCenter
        ToastNotification.Show(form, msg + "   ", My.Resources.version_32x32, 3 * 1000, glow, pos)
    End Sub

    ''' <summary>
    ''' แสดงกล่องข้อความแจ้งเตือนความผิดพลาด
    ''' </summary>
    ''' <remarks>
    ''' Input :  form  =  หน้าต่างที่กำลังใช้งาน
    '''	 msg  =  ข้อความที่จะแสดง
    '''	Location  =   ตำแหน่งที่ต้องการแสดง
    ''' TopLeft,TopCenter,TopRight,
    ''' MiddleLeft,MiddleCenter,MiddleRight,
    ''' BottomLeft,BottomCenter,BottomRight
    ''' Output :  แสดงกล่องข้อความแจ้งเตือนข้อความที่ต้องการ
    ''' Example :  TOASTCLASS.WarningToastMSG(Me, “Database Error”, eToastPosition.BottomCenter)
    ''' </remarks>
    Public Shared Sub WarningToastMSG(ByVal form As Form, ByVal msg As String, ByVal Location As eToastPosition)
        Dim glow As eToastGlowColor = eToastGlowColor.Blue
        ToastNotification.Show(form, msg + "   ", My.Resources.version_32x32, 5 * 1000, glow, Location)
    End Sub
    Public Shared Sub WarningToastMSG(ByVal form As UserControl, ByVal msg As String, ByVal Location As eToastPosition)
        Dim glow As eToastGlowColor = eToastGlowColor.Blue
        ToastNotification.Show(form, msg + "   ", My.Resources.version_32x32, 5 * 1000, glow, Location)
    End Sub
    ''' <summary>
    ''' แสดงกล่องข้อความแจ้งเตือนข้อความที่สำเร็จ
    ''' </summary>
    ''' <remarks>
    ''' Input :  form  =  หน้าต่างที่กำลังใช้งาน
    '''	 msg  =  ข้อความที่จะแสดง
    '''	Location  =   ตำแหน่งที่ต้องการแสดง
    ''' TopLeft,TopCenter,TopRight,
    ''' MiddleLeft,MiddleCenter,MiddleRight,
    ''' BottomLeft,BottomCenter,BottomRight
    ''' Output :  แสดงกล่องข้อความที่ต้องการ
    ''' Example :  TOASTCLASS. SuccesToastMSG(Me, “Save Success”, eToastPosition.BottomCenter)
    ''' </remarks>
    Public Shared Sub SuccesToastMSG(ByVal form As Form, ByVal msg As String, ByVal Location As eToastPosition)
        Dim glow As eToastGlowColor = eToastGlowColor.Blue
        ToastNotification.Show(form, msg + "   ", My.Resources.version_32x32, 5 * 1000, glow, Location)
    End Sub
End Class

