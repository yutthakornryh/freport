﻿Imports DevExpress.XtraReports.UI

Public Class DebtBillPrintReport
    Dim dtset As New AccountSet
    Dim debtClass As DebtClass
    Dim rpt As XtraReport = New DebtBillReport
    Public Sub New(ByRef fnclass As DebtClass, ByRef dtset1 As AccountSet)

        ' This call is required by the designer.
        InitializeComponent()
        debtClass = fnclass
        dtset = dtset1
        ' Add any initialization after the InitializeComponent() call.

    End Sub


    Private Sub DebtBillPrintReport_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        rpt.DataSource = dtset
        rpt.DataMember = "DOCINV"
        rpt.Parameters("Contact").Value = debtClass.setReceipt.nameAgent
        rpt.Parameters("DescriptType").Value = debtClass.setReceipt.nameBank

        rpt.Parameters("fontThai").Value = debtClass.setReceipt.fonthai
        rpt.Parameters("USMK").Value = debtClass.setReceipt.nameUser

        rpt.CreateDocument()
        Me.DocumentViewer1.DocumentSource = rpt


    End Sub
End Class