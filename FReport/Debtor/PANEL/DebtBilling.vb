﻿Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraEditors.Controls

Public Class DebtBilling
    Dim dtset As AccountSet
    Public Property report As XtraReport
    Dim acclass As AccountClass
    Dim InvoiceCheck As Integer
    Private Sub btnApplyFilter_Click(sender As Object, e As EventArgs) Handles btnApplyFilter.Click
        acclass.InvoiceDebt(dateStart.EditValue, dateTo.EditValue, txtAgent.EditValue)
        report = New DebtDailyReport
        report.DataSource = dtset
        report.Parameters("contactName").Value = txtAgent.Text
        report.Parameters("DateDescription").Value = "เอกสารการรักษาพยาบาล ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text

        report.CreateDocument()
        DocumentViewer1.DocumentSource = report

    End Sub

    Private Sub DebtBilling_Load(sender As Object, e As EventArgs) Handles Me.Load
        dateStart.EditValue = Now
        dateTo.EditValue = Now
        dtset = New AccountSet
        acclass = New AccountClass(dtset)

        acclass.getAgent()

        txtAgent.Properties.DataSource = dtset.Tables("Agent")
        txtAgent.Properties.DisplayMember = "company_name"
        txtAgent.Properties.ValueMember = "agentcode"

        Dim col3 As LookUpColumnInfoCollection = txtAgent.Properties.Columns
        col3.Add(New LookUpColumnInfo("agentcode", 0))
        col3.Add(New LookUpColumnInfo("company_name", 0))

        txtAgent.Properties.BestFitMode = BestFitMode.BestFitResizePopup
        txtAgent.Properties.SearchMode = SearchMode.AutoComplete
        txtAgent.Properties.AutoSearchColumnIndex = 1
    End Sub
End Class
