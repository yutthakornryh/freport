﻿Imports DevExpress.XtraReports.UI

Public Class DebtForm
    Dim report As New XtraReport
    Dim DebtBill As DebtBilling
    Dim DebtBillCheck As DeptBillCheck
    Dim DebtPrintForm As DebtPrintForm
    Dim localPath As String = "F://FProject//"
    Dim reportname As String
    Private Sub menuFrnDrugPay_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles menuFrnDrugPay.LinkClicked
        DebtBill = New DebtBilling
        AddHandler DebtBill.btnApplyFilter.Click, AddressOf InvoicePanel
        DebtBill.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(DebtBill)
        reportname = "Invoice"
    End Sub


    Private Sub DebtForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None

    End Sub

    Private Sub menuMonthlyDrgPay_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles menuMonthlyDrgPay.LinkClicked
        DebtBillCheck = New DeptBillCheck
        '   AddHandler DebtBillCheck.btnApplyFilter.Click, AddressOf InvoicePanel
        DebtBillCheck.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(DebtBillCheck)
        '   reportname = "Invoice"
    End Sub

    Private Sub NavBarItem1_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem1.LinkClicked


        DebtPrintForm = New DebtPrintForm
        AddHandler DebtPrintForm.btnApplyFilter.Click, AddressOf InvoicePanel
        DebtPrintForm.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(DebtPrintForm)
        reportname = "DebtPrintForm"
    End Sub

    Private Sub InvoicePanel(sender As Object, e As EventArgs)
        If reportname = "DebtPrintForm" Then
            Me.report = DebtPrintForm.report
        ElseIf reportname = "Invoice" Then

            Me.report = DebtBill.report
        End If
        'RemoveHandler bj8Panel.btnApplyFilter.Click, AddressOf bj8PanelShowing
    End Sub

    Private Sub BarButtonItem6_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        If report IsNot Nothing Then
            FolderBrowserDialog1.Description = "Pick Folder to store PDF files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                ExportReport(report, FolderBrowserDialog1.SelectedPath & "\" & reportname & ".pdf", "pdf")
            End If
        End If


    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        If report IsNot Nothing Then
            FolderBrowserDialog1.Description = "Pick Folder to store Excecl files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                ExportReport(report, FolderBrowserDialog1.SelectedPath & "\" & reportname & ".xls", "xls")

            End If
        End If
    End Sub

    Private Sub BarButtonItem4_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        If report IsNot Nothing Then
            Dim printTool As New ReportPrintTool(report)
            printTool.Print()
            printTool.PrintingSystem.ShowMarginsWarning = False
        End If
    End Sub

    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        If report IsNot Nothing Then
            Dim printTool As New ReportPrintTool(report)
            printTool.PrintDialog()
        End If
    End Sub

    Private Sub NavBarControl1_Click(sender As Object, e As EventArgs) Handles NavBarControl1.Click

    End Sub
End Class