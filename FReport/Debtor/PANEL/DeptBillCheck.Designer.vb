﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DeptBillCheck
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.AccountSet3 = New FReport.AccountSet()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colinvdocid1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldatedoc1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmtrgtid1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colstrgtid1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colsbrgtid1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colptrgtid1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpconid1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colamount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colamt1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbill1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldiscount1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltax1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTotal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.BtnDelete = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.txtUsername = New DevExpress.XtraEditors.LabelControl()
        Me.txtUserRequest = New DevExpress.XtraEditors.TextEdit()
        Me.txtBank = New DevExpress.XtraEditors.LookUpEdit()
        Me.txtSN = New System.Windows.Forms.TextBox()
        Me.txtType = New DevExpress.XtraEditors.LookUpEdit()
        Me.txtDebtType = New DevExpress.XtraEditors.LookUpEdit()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.AccountSet2 = New FReport.AccountSet()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colinvdocid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldatedoc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colamt = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colname = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmtrgtid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpconid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colptrgtid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colstrgtid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colsbrgtid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnChoose = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.colamount1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbill = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldiscount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coltax = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.txtCompany = New DevExpress.XtraEditors.LookUpEdit()
        Me.a = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.AccountSet1 = New FReport.AccountSet()
        Me.ประวัติ = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AccountSet3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BtnDelete, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUserRequest.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBank.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDebtType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AccountSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnChoose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCompany.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.a, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AccountSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ประวัติ, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.txtUsername)
        Me.LayoutControl1.Controls.Add(Me.txtUserRequest)
        Me.LayoutControl1.Controls.Add(Me.txtBank)
        Me.LayoutControl1.Controls.Add(Me.txtSN)
        Me.LayoutControl1.Controls.Add(Me.txtType)
        Me.LayoutControl1.Controls.Add(Me.txtDebtType)
        Me.LayoutControl1.Controls.Add(Me.SearchControl1)
        Me.LayoutControl1.Controls.Add(Me.btnSave)
        Me.LayoutControl1.Controls.Add(Me.txtCompany)
        Me.LayoutControl1.Controls.Add(Me.GridControl2)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.a
        Me.LayoutControl1.Size = New System.Drawing.Size(1044, 655)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'GridControl1
        '
        Me.GridControl1.Cursor = System.Windows.Forms.Cursors.Default
        Me.GridControl1.DataMember = "DOCINV"
        Me.GridControl1.DataSource = Me.AccountSet3
        Me.GridControl1.Location = New System.Drawing.Point(262, 49)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.BtnDelete})
        Me.GridControl1.Size = New System.Drawing.Size(770, 255)
        Me.GridControl1.TabIndex = 31
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'AccountSet3
        '
        Me.AccountSet3.DataSetName = "AccountSet"
        Me.AccountSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Appearance.FooterPanel.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.GridView1.Appearance.FooterPanel.Options.UseFont = True
        Me.GridView1.Appearance.GroupFooter.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.GridView1.Appearance.GroupFooter.Options.UseFont = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colinvdocid1, Me.coldatedoc1, Me.colname1, Me.colmtrgtid1, Me.colstrgtid1, Me.colsbrgtid1, Me.colptrgtid1, Me.colpconid1, Me.colamount, Me.colamt1, Me.colbill1, Me.coldiscount1, Me.coltax1, Me.colTotal, Me.GridColumn2})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsCustomization.AllowFilter = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsCustomization.AllowSort = False
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colinvdocid1
        '
        Me.colinvdocid1.Caption = "เลขใบแจ้งหนี้"
        Me.colinvdocid1.FieldName = "invdocid"
        Me.colinvdocid1.Name = "colinvdocid1"
        Me.colinvdocid1.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.colinvdocid1.Visible = True
        Me.colinvdocid1.VisibleIndex = 0
        Me.colinvdocid1.Width = 103
        '
        'coldatedoc1
        '
        Me.coldatedoc1.Caption = "วันที่คนไข้ชำระเงิน"
        Me.coldatedoc1.FieldName = "datedoc"
        Me.coldatedoc1.Name = "coldatedoc1"
        Me.coldatedoc1.Visible = True
        Me.coldatedoc1.VisibleIndex = 1
        Me.coldatedoc1.Width = 103
        '
        'colname1
        '
        Me.colname1.Caption = "ชื่อ - นามสกุล"
        Me.colname1.FieldName = "name"
        Me.colname1.Name = "colname1"
        Me.colname1.Visible = True
        Me.colname1.VisibleIndex = 2
        Me.colname1.Width = 103
        '
        'colmtrgtid1
        '
        Me.colmtrgtid1.FieldName = "mtrgtid"
        Me.colmtrgtid1.Name = "colmtrgtid1"
        '
        'colstrgtid1
        '
        Me.colstrgtid1.FieldName = "strgtid"
        Me.colstrgtid1.Name = "colstrgtid1"
        '
        'colsbrgtid1
        '
        Me.colsbrgtid1.FieldName = "sbrgtid"
        Me.colsbrgtid1.Name = "colsbrgtid1"
        '
        'colptrgtid1
        '
        Me.colptrgtid1.FieldName = "ptrgtid"
        Me.colptrgtid1.Name = "colptrgtid1"
        '
        'colpconid1
        '
        Me.colpconid1.FieldName = "pconid"
        Me.colpconid1.Name = "colpconid1"
        '
        'colamount
        '
        Me.colamount.Caption = "ยอดเงิน"
        Me.colamount.DisplayFormat.FormatString = "n2"
        Me.colamount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colamount.FieldName = "amount"
        Me.colamount.Name = "colamount"
        Me.colamount.Visible = True
        Me.colamount.VisibleIndex = 3
        Me.colamount.Width = 103
        '
        'colamt1
        '
        Me.colamt1.Caption = "ยอดคงเหลือ"
        Me.colamt1.DisplayFormat.FormatString = "n2"
        Me.colamt1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colamt1.FieldName = "amt"
        Me.colamt1.Name = "colamt1"
        Me.colamt1.Visible = True
        Me.colamt1.VisibleIndex = 4
        Me.colamt1.Width = 103
        '
        'colbill1
        '
        Me.colbill1.Caption = "จำนวนเงินที่ชำระ"
        Me.colbill1.DisplayFormat.FormatString = "n2"
        Me.colbill1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colbill1.FieldName = "bill"
        Me.colbill1.Name = "colbill1"
        Me.colbill1.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "bill", "{0:n2}")})
        Me.colbill1.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.colbill1.Visible = True
        Me.colbill1.VisibleIndex = 5
        Me.colbill1.Width = 103
        '
        'coldiscount1
        '
        Me.coldiscount1.Caption = "ส่วนลด"
        Me.coldiscount1.DisplayFormat.FormatString = "n2"
        Me.coldiscount1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.coldiscount1.FieldName = "discountT"
        Me.coldiscount1.Name = "coldiscount1"
        Me.coldiscount1.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "discountT", "{0:n2}")})
        Me.coldiscount1.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.coldiscount1.Visible = True
        Me.coldiscount1.VisibleIndex = 6
        Me.coldiscount1.Width = 103
        '
        'coltax1
        '
        Me.coltax1.Caption = "ภาษี"
        Me.coltax1.DisplayFormat.FormatString = "n2"
        Me.coltax1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.coltax1.FieldName = "tax"
        Me.coltax1.Name = "coltax1"
        Me.coltax1.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "tax", "{0:n2}")})
        Me.coltax1.Visible = True
        Me.coltax1.VisibleIndex = 7
        Me.coltax1.Width = 103
        '
        'colTotal
        '
        Me.colTotal.Caption = "คงเหลือ"
        Me.colTotal.DisplayFormat.FormatString = "n2"
        Me.colTotal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colTotal.FieldName = "TotalResultT"
        Me.colTotal.Name = "colTotal"
        Me.colTotal.OptionsColumn.AllowEdit = False
        Me.colTotal.OptionsColumn.AllowFocus = False
        Me.colTotal.OptionsColumn.ReadOnly = True
        Me.colTotal.UnboundExpression = "[amt] - [bill] - [discountT] - [tax]"
        Me.colTotal.UnboundType = DevExpress.Data.UnboundColumnType.[Decimal]
        Me.colTotal.Visible = True
        Me.colTotal.VisibleIndex = 8
        Me.colTotal.Width = 144
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "ยกเลิก"
        Me.GridColumn2.ColumnEdit = Me.BtnDelete
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 9
        Me.GridColumn2.Width = 66
        '
        'BtnDelete
        '
        Me.BtnDelete.AutoHeight = False
        Me.BtnDelete.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Close)})
        Me.BtnDelete.Name = "BtnDelete"
        Me.BtnDelete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(1008, 332)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(24, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 30
        Me.SimpleButton1.Text = "x"
        '
        'txtUsername
        '
        Me.txtUsername.Location = New System.Drawing.Point(841, 403)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(87, 13)
        Me.txtUsername.StyleController = Me.LayoutControl1
        Me.txtUsername.TabIndex = 29
        '
        'txtUserRequest
        '
        Me.txtUserRequest.Location = New System.Drawing.Point(729, 403)
        Me.txtUserRequest.Name = "txtUserRequest"
        Me.txtUserRequest.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtUserRequest.Size = New System.Drawing.Size(108, 20)
        Me.txtUserRequest.StyleController = Me.LayoutControl1
        Me.txtUserRequest.TabIndex = 28
        Me.txtUserRequest.Tag = "1"
        '
        'txtBank
        '
        Me.txtBank.Location = New System.Drawing.Point(729, 332)
        Me.txtBank.Name = "txtBank"
        Me.txtBank.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtBank.Properties.NullText = "เลือกธนาคาร"
        Me.txtBank.Size = New System.Drawing.Size(275, 20)
        Me.txtBank.StyleController = Me.LayoutControl1
        Me.txtBank.TabIndex = 27
        '
        'txtSN
        '
        Me.txtSN.Location = New System.Drawing.Point(729, 358)
        Me.txtSN.Name = "txtSN"
        Me.txtSN.Size = New System.Drawing.Size(303, 20)
        Me.txtSN.TabIndex = 24
        '
        'txtType
        '
        Me.txtType.Location = New System.Drawing.Point(729, 308)
        Me.txtType.Name = "txtType"
        Me.txtType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtType.Properties.NullText = "เลือกประเภท"
        Me.txtType.Size = New System.Drawing.Size(303, 20)
        Me.txtType.StyleController = Me.LayoutControl1
        Me.txtType.TabIndex = 23
        '
        'txtDebtType
        '
        Me.txtDebtType.Location = New System.Drawing.Point(340, 308)
        Me.txtDebtType.Name = "txtDebtType"
        Me.txtDebtType.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.txtDebtType.Properties.Appearance.Options.UseFont = True
        Me.txtDebtType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtDebtType.Properties.NullText = "ประเภทการชำระ"
        Me.txtDebtType.Size = New System.Drawing.Size(307, 22)
        Me.txtDebtType.StyleController = Me.LayoutControl1
        Me.txtDebtType.TabIndex = 19
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.GridControl2
        Me.SearchControl1.Location = New System.Drawing.Point(96, 61)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton()})
        Me.SearchControl1.Properties.Client = Me.GridControl2
        Me.SearchControl1.Size = New System.Drawing.Size(156, 20)
        Me.SearchControl1.StyleController = Me.LayoutControl1
        Me.SearchControl1.TabIndex = 18
        '
        'GridControl2
        '
        Me.GridControl2.Cursor = System.Windows.Forms.Cursors.Default
        Me.GridControl2.DataMember = "DOCINV"
        Me.GridControl2.DataSource = Me.AccountSet2
        Me.GridControl2.Location = New System.Drawing.Point(18, 85)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.btnChoose})
        Me.GridControl2.Size = New System.Drawing.Size(234, 552)
        Me.GridControl2.TabIndex = 13
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'AccountSet2
        '
        Me.AccountSet2.DataSetName = "AccountSet"
        Me.AccountSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colinvdocid, Me.coldatedoc, Me.colamt, Me.colname, Me.colmtrgtid, Me.colpconid, Me.colptrgtid, Me.colstrgtid, Me.colsbrgtid, Me.GridColumn1, Me.colamount1, Me.colbill, Me.coldiscount, Me.coltax})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colinvdocid
        '
        Me.colinvdocid.Caption = "เลข"
        Me.colinvdocid.FieldName = "invdocid"
        Me.colinvdocid.Name = "colinvdocid"
        Me.colinvdocid.Visible = True
        Me.colinvdocid.VisibleIndex = 0
        '
        'coldatedoc
        '
        Me.coldatedoc.Caption = "วันที่ชำระ"
        Me.coldatedoc.FieldName = "datedoc"
        Me.coldatedoc.Name = "coldatedoc"
        Me.coldatedoc.Visible = True
        Me.coldatedoc.VisibleIndex = 1
        '
        'colamt
        '
        Me.colamt.Caption = "คงเหลือ"
        Me.colamt.FieldName = "amt"
        Me.colamt.Name = "colamt"
        Me.colamt.Visible = True
        Me.colamt.VisibleIndex = 2
        '
        'colname
        '
        Me.colname.Caption = "ชื่อ-นามสกุล"
        Me.colname.FieldName = "name"
        Me.colname.Name = "colname"
        Me.colname.Visible = True
        Me.colname.VisibleIndex = 3
        '
        'colmtrgtid
        '
        Me.colmtrgtid.FieldName = "mtrgtid"
        Me.colmtrgtid.Name = "colmtrgtid"
        '
        'colpconid
        '
        Me.colpconid.FieldName = "pconid"
        Me.colpconid.Name = "colpconid"
        '
        'colptrgtid
        '
        Me.colptrgtid.FieldName = "ptrgtid"
        Me.colptrgtid.Name = "colptrgtid"
        '
        'colstrgtid
        '
        Me.colstrgtid.FieldName = "strgtid"
        Me.colstrgtid.Name = "colstrgtid"
        '
        'colsbrgtid
        '
        Me.colsbrgtid.FieldName = "sbrgtid"
        Me.colsbrgtid.Name = "colsbrgtid"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "GridColumn1"
        Me.GridColumn1.ColumnEdit = Me.btnChoose
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 4
        '
        'btnChoose
        '
        Me.btnChoose.AutoHeight = False
        Me.btnChoose.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.OK)})
        Me.btnChoose.Name = "btnChoose"
        Me.btnChoose.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'colamount1
        '
        Me.colamount1.FieldName = "amount"
        Me.colamount1.Name = "colamount1"
        '
        'colbill
        '
        Me.colbill.FieldName = "bill"
        Me.colbill.Name = "colbill"
        '
        'coldiscount
        '
        Me.coldiscount.FieldName = "discount"
        Me.coldiscount.Name = "coldiscount"
        '
        'coltax
        '
        Me.coltax.FieldName = "tax"
        Me.coltax.Name = "coltax"
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(932, 403)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(100, 22)
        Me.btnSave.StyleController = Me.LayoutControl1
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "ออกใบเสร็จ"
        '
        'txtCompany
        '
        Me.txtCompany.Location = New System.Drawing.Point(18, 37)
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtCompany.Properties.NullText = "เลือกบริษัท"
        Me.txtCompany.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.txtCompany.Size = New System.Drawing.Size(234, 20)
        Me.txtCompany.StyleController = Me.LayoutControl1
        Me.txtCompany.TabIndex = 14
        '
        'a
        '
        Me.a.CustomizationFormText = "a"
        Me.a.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.a.GroupBordersVisible = False
        Me.a.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup1, Me.EmptySpaceItem3, Me.LayoutControlItem6, Me.LayoutControlItem13, Me.LayoutControlItem15, Me.LayoutControlItem7, Me.LayoutControlItem16, Me.EmptySpaceItem5, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.EmptySpaceItem4, Me.EmptySpaceItem1})
        Me.a.Location = New System.Drawing.Point(0, 0)
        Me.a.Name = "a"
        Me.a.Size = New System.Drawing.Size(1044, 655)
        Me.a.Text = "a"
        Me.a.TextVisible = False
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "บริษัท"
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem14})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3)
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(250, 635)
        Me.LayoutControlGroup1.Text = "บริษัท"
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.GridControl2
        Me.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10"
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(238, 556)
        Me.LayoutControlItem10.Text = "LayoutControlItem10"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextToControlDistance = 0
        Me.LayoutControlItem10.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.txtCompany
        Me.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11"
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(238, 24)
        Me.LayoutControlItem11.Text = "LayoutControlItem11"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextToControlDistance = 0
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.SearchControl1
        Me.LayoutControlItem14.CustomizationFormText = "ค้นหา"
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(238, 24)
        Me.LayoutControlItem14.Text = "ค้นหา"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(75, 13)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(250, 417)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(774, 218)
        Me.EmptySpaceItem3.Text = "EmptySpaceItem3"
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.btnSave
        Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
        Me.LayoutControlItem6.Location = New System.Drawing.Point(920, 391)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(104, 26)
        Me.LayoutControlItem6.Text = "LayoutControlItem6"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextToControlDistance = 0
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.txtType
        Me.LayoutControlItem13.CustomizationFormText = "ชำระผ่าน"
        Me.LayoutControlItem13.Location = New System.Drawing.Point(639, 296)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(385, 24)
        Me.LayoutControlItem13.Text = "ชำระผ่าน"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(75, 13)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.txtSN
        Me.LayoutControlItem15.CustomizationFormText = "หมายเลข"
        Me.LayoutControlItem15.Location = New System.Drawing.Point(639, 346)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(385, 24)
        Me.LayoutControlItem15.Text = "หมายเลข"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(75, 13)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.txtDebtType
        Me.LayoutControlItem7.CustomizationFormText = "ประเภทการชำระ"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(250, 296)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(389, 26)
        Me.LayoutControlItem7.Text = "ประเภทการชำระ"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(75, 13)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.txtBank
        Me.LayoutControlItem16.CustomizationFormText = "ธนาคาร"
        Me.LayoutControlItem16.Location = New System.Drawing.Point(639, 320)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(357, 26)
        Me.LayoutControlItem16.Text = "ธนาคาร"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(75, 13)
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.CustomizationFormText = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(639, 370)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(385, 21)
        Me.EmptySpaceItem5.Text = "EmptySpaceItem5"
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.txtUserRequest
        Me.LayoutControlItem17.CustomizationFormText = "User Req"
        Me.LayoutControlItem17.Location = New System.Drawing.Point(639, 391)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(190, 26)
        Me.LayoutControlItem17.Text = "User Req"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(75, 13)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.txtUsername
        Me.LayoutControlItem18.CustomizationFormText = "LayoutControlItem18"
        Me.LayoutControlItem18.Location = New System.Drawing.Point(829, 391)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(91, 26)
        Me.LayoutControlItem18.Text = "LayoutControlItem18"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextToControlDistance = 0
        Me.LayoutControlItem18.TextVisible = False
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.SimpleButton1
        Me.LayoutControlItem19.CustomizationFormText = "LayoutControlItem19"
        Me.LayoutControlItem19.Location = New System.Drawing.Point(996, 320)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(28, 26)
        Me.LayoutControlItem19.Text = "LayoutControlItem19"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextToControlDistance = 0
        Me.LayoutControlItem19.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.GridControl1
        Me.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(250, 37)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(774, 259)
        Me.LayoutControlItem20.Text = "LayoutControlItem20"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextToControlDistance = 0
        Me.LayoutControlItem20.TextVisible = False
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(250, 0)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(774, 37)
        Me.EmptySpaceItem4.Text = "EmptySpaceItem4"
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(250, 322)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(389, 95)
        Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'AccountSet1
        '
        Me.AccountSet1.DataSetName = "AccountSet"
        Me.AccountSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ประวัติ
        '
        Me.ประวัติ.BestFitWeight = 80
        Me.ประวัติ.CaptionImagePadding = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.ประวัติ.CustomizationFormText = "ประวัติ"
        Me.ประวัติ.Location = New System.Drawing.Point(114, 0)
        Me.ประวัติ.Name = "ประวัติ"
        Me.ประวัติ.Size = New System.Drawing.Size(381, 158)
        Me.ประวัติ.Text = "ประวัติ"
        '
        'Bar1
        '
        Me.Bar1.BarName = "Custom 2"
        Me.Bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Custom 2"
        '
        'DeptBillCheck
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "DeptBillCheck"
        Me.Size = New System.Drawing.Size(1044, 655)
        Me.Tag = "DEBT000"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AccountSet3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BtnDelete, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUserRequest.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBank.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDebtType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AccountSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnChoose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCompany.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.a, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AccountSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ประวัติ, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents a As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents txtCompany As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ประวัติ As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents AccountSet1 As FReport.AccountSet
    Friend WithEvents btnChoose As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents txtDebtType As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents AccountSet2 As FReport.AccountSet
    Friend WithEvents colinvdocid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldatedoc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colamt As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmtrgtid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpconid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colptrgtid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colstrgtid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colsbrgtid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents txtSN As System.Windows.Forms.TextBox
    Friend WithEvents txtType As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtBank As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents txtUserRequest As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtUsername As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colamount1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbill As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldiscount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltax As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colinvdocid1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldatedoc1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colamt1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colname1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmtrgtid1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colstrgtid1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colsbrgtid1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colptrgtid1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpconid1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colamount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbill1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldiscount1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coltax1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AccountSet3 As FReport.AccountSet
    Friend WithEvents colTotal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BtnDelete As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit

End Class
