﻿Imports DevExpress.XtraReports.UI

Public Class DebtPrintForm
    Dim dtset As New AccountSet
    Public Property report As XtraReport

    Private Sub btnApplyFilter_Click(sender As Object, e As EventArgs) Handles btnApplyFilter.Click
        Dim debtClass As DebtClass

        debtClass = New DebtClass(dtset)

        report = New ReceiptDailyReport
        debtClass.getDebt(dateStart.EditValue, dateTo.EditValue)

        report.DataSource = dtset
        report.Parameters("Header").Value = "รายงานทางการเงิน ( ใบนำส่งเงิน ) "
        report.Parameters("DateDescription").Value = "ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text


        report.CreateDocument()
        DocumentViewer1.DocumentSource = report
    End Sub
End Class
