﻿Public Class DebtClass
    Dim dtset As AccountSet
    Dim connect As ConnecDBRYH
    Structure fncDebt
        Sub New(j As Integer)
            f_cancel = 0
            f_complete = 0
            f_print = 0
            SN = "null"
            hremark = " "
            fremark = " "
        End Sub
        Dim debdocid As String
        Dim datedoc As Date
        Dim debtype As String
        Dim mtrgtid As String
        Dim strgtid As String
        Dim sbrgtid As String
        Dim ptrgtid As String
        Dim pconid As String
        Dim amount As String
        Dim discount As String
        Dim vat As String
        Dim total As String
        Dim f_cancel As String
        Dim f_print As String
        Dim f_complete As String
        Dim hremark As String
        Dim fremark As String
        Dim usmk As String
        Dim invdocid As String
        Dim ACCMID As String
        Dim BANKID As String
        Dim SN As String
        Dim COMID As String

        Dim nameAgent As String
        Dim TypeBill As String
        Dim nameBank As String
        Dim fonthai As String
        Dim nameUser As String
    End Structure
    Dim strFncDebt As fncDebt
    Public Property setReceipt As fncDebt
        Get
            Return strFncDebt
        End Get
        Set(value As fncDebt)
            strFncDebt = value
        End Set
    End Property
    Public Sub New(ByRef dset As AccountSet)
        dtset = dset
        connect = ConnecDBRYH.NewConnection
        strFncDebt = New fncDebt(0)

    End Sub
    Public Sub getAgent()
        Dim sql As String
        sql = "SELECT agentcode,company_name FROM  agent WHERE agentype = 4;"
        dtset.Tables("Agent").Clear()
        connect.GetTable(sql, dtset.Tables("Agent"))
    End Sub
    Public Sub GetInvoice(ByRef AgentCode As String)
        Dim sql As String

        sql = "SELECT  invoicehd.invdocid  ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,invoicehd.amount,discount,total ,datedoc,(amount - paid - tax) AS 'amt',invoicehd.mtrgtid,invoicehd.strgtid,invoicehd.sbrgtid,invoicehd.ptrgtid,invoicehd.pconid FROM  (SELECT * FROM invoicehd WHERE  amount > (paid + tax)   AND f_cancel = 0  )  AS invoicehd JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON invoicehd.hn	 = person.hn JOIN masprename ON masprename.PRENAME = person.prename JOIN masptypergt ON (coalesce(invoicehd.mtrgtid, '') = coalesce(masptypergt.mtrgtid, '')) AND (coalesce(invoicehd.strgtid, '') = coalesce(masptypergt.strgtid, ''))AND (coalesce(invoicehd.sbrgtid, '') = coalesce(masptypergt.sbrgtid, '')) AND  agentcode = '" & AgentCode & "';"
        dtset.Tables("DOCINV").Clear()
        connect.GetTable(sql, dtset.Tables("DOCINV"))
    End Sub
    Public Sub GetDebType()
        Dim sql As String
        sql = "SELECT debtype , debtcode , debtname FROM masdebtype WHERE status = 1 "
        dtset.Tables("DEBTCODE").Clear()
        connect.GetTable(sql, dtset.Tables("DEBTCODE"))

    End Sub

    Public Sub getBank()
        Dim sql As String = "SELECT bankid,`code`,bank_name,bank_name_en FROM  masbank WHERE `status` = 1; "
        connect.GetTable(sql, dtset.Tables("BANK"))
    End Sub

    Public Sub getAccept()
        Dim sql As String = "SELECT accmid ,accm_name , accm_name_en FROM masacceptm WHERE status = 1; "
        connect.GetTable(sql, dtset.Tables("acceptm"))
    End Sub
    Public Sub InsertReceipt()
        Dim sql As String
        Dim sql1 As String

        sql = "EMPID,"
        sql1 = "" & strFncDebt.usmk & ","
        sql += "DEBDOCID,"
        sql1 += "" & strFncDebt.debdocid & ","
      

        sql += "COMID,"
        sql1 += "" & ENVIRONMENTCLASS.MACHINE.IDCOM & ","

        sql += "ACCMID,"
        sql1 += "" & strFncDebt.ACCMID & ","

        sql += "AMOUNT,"
        sql1 += "" & strFncDebt.amount & ","

        sql += "BANKID,"
        sql1 += "" & strFncDebt.BANKID & ","

        sql += "SN,"
        sql1 += If(strFncDebt.SN = "null", "null,", "'" & strFncDebt.SN & "',")

        sql += "ACC_DATE "
        sql1 += "current_timestamp() "
        Dim sql3 As String
        sql3 = "INSERT INTO frnreceipt ( " & sql & " ) VALUES ( " & sql1 & " ); "
        connect.ExecuteNonQuery(sql3)

    End Sub

    Public Sub getDebt(Start As Date, Todate As Date)
        Dim sql As String


        sql = "SELECT  debt.debdocid AS billdocid ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,debt.amount,debt.discount,debt.total,debt.vat,conname AS CONTACT , usmktime AS dateserv , frnreceipt.accmid,frnreceipt.amount AS 'frnamount' , accm_name FROM  (SELECT * FROM debt WHERE  usmktime >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND usmktime < '" & Start.Year & Todate.ToString("-MM-dd") & " 23:59:59" & "'  )  AS debt JOIN  (SELECT conname,pconid,hn FROM  contact ) AS contact   ON  debt.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON contact.hn = person.hn JOIN masprename ON masprename.prename = person.prename  JOIN  (SELECT * FROM frnreceipt WHERE acc_date >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND acc_date < '" & Start.Year & Todate.ToString("-MM-dd") & " 23:59:59" & "'   ) AS frnreceipt ON frnreceipt.debdocid = debt.debdocid JOIN masacceptm ON masacceptm.accmid = frnreceipt.accmid"
        dtset.Tables("ReceiptDaily").Clear()
        connect.GetTable(sql, dtset.Tables("ReceiptDaily"))


    End Sub

    Public Sub getDebtAgent(Start As Date, Todate As Date)
        Dim sql As String


        sql = "SELECT  debt.debdocid AS billdocid ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,debt.amount,debt.discount,debt.total,debt.vat,conname AS CONTACT , usmktime AS dateserv , frnreceipt.accmid,frnreceipt.amount AS 'frnamount' , accm_name FROM  (SELECT * FROM debt WHERE  usmktime >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND usmktime < '" & Start.Year & Todate.ToString("-MM-dd") & " 23:59:59" & "'  )  AS debt JOIN  (SELECT conname,pconid,hn FROM  contact ) AS contact   ON  debt.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON contact.hn = person.hn JOIN masprename ON masprename.prename = person.prename  JOIN  (SELECT * FROM frnreceipt WHERE acc_date >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND acc_date < '" & Start.Year & Todate.ToString("-MM-dd") & " 23:59:59" & "'   ) AS frnreceipt ON frnreceipt.debdocid = debt.debdocid JOIN masacceptm ON masacceptm.accmid = frnreceipt.accmid"
        dtset.Tables("ReceiptDaily").Clear()
        connect.GetTable(sql, dtset.Tables("ReceiptDaily"))


    End Sub



    Public Sub setFrnDebt()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String

        sql += "DEBDOCID ,"
        sql1 += " " & strFncDebt.debdocid & " ,"

        sql += "invdocid , "
        sql1 += " '" & strFncDebt.invdocid & "' , "

        sql += " amount , "
        sql1 += " '" & strFncDebt.amount & "', "

        sql += "discount , "
        sql1 += " '" & strFncDebt.discount & "', "

        sql += "vat , "
        sql1 += " '" & strFncDebt.vat & "' , "

        sql += "total , "
        sql1 += " '" & Convert.ToDouble(strFncDebt.discount) + Convert.ToDouble(strFncDebt.amount) + Convert.ToDouble(strFncDebt.vat) & "' , "

        sql += "status  "
        sql1 += " 1  "


        sql2 = "INSERT INTO  frndebt ( " & sql & ") VALUES ( " & sql1 & " ); "
        connect.ExecuteNonQuery(sql2)



    End Sub


    Public Sub SetDebt()
        Dim sql As String
        Dim sql1 As String
        Dim sql2 As String


        sql = "debdocid , "
        strFncDebt.debdocid = generateIDYearAndRuning("debt", "debdocid")
        sql1 = strFncDebt.debdocid + " , "

        sql += "datedoc , "
        sql1 += "curtime() , "

        sql += "pconid , "
        sql1 += " " & strFncDebt.pconid & " ,"


        sql += "debtype , "
        sql1 += " " & strFncDebt.debtype & " ,"

        sql += "mtrgtid , "
        sql1 += " " & strFncDebt.mtrgtid & " , "

        sql += "strgtid ,"
        sql1 += " " & strFncDebt.strgtid & " , "

        sql += "ptrgtid , "
        sql1 += " " & strFncDebt.ptrgtid & " , "

        sql += "amount , "
        sql1 += " '" & strFncDebt.amount & "' , "

        sql += "discount , "
        sql1 += " '" & strFncDebt.discount & "' , "

        sql += "vat , "
        sql1 += " '" & strFncDebt.vat & "' , "

        sql += "total , "
        sql1 += " '" & Convert.ToDouble(strFncDebt.discount) + Convert.ToDouble(strFncDebt.amount) + Convert.ToDouble(strFncDebt.vat) & "' , "

        sql += "f_cancel , "
        sql1 += " " & strFncDebt.f_cancel & " , "

        sql += "f_print , "
        sql1 += " " & strFncDebt.f_print & " , "


        sql += "f_complete , "
        sql1 += " " & strFncDebt.f_complete & " , "

        sql += "hremark , "
        sql1 += " '" & strFncDebt.hremark & "', "

        sql += "fremark , "
        sql1 += " '" & strFncDebt.fremark & "',"

        sql += "usmk , "
        sql1 += " '" & strFncDebt.usmk & "' ,"

        sql += "usmktime  "
        sql1 += "curtime()  "


        sql2 = "INSERT INTO debt ( " & sql & " ) VALUES ( " & sql1 & " ); "

        connect.ExecuteNonQuery(sql2)
    End Sub
    Public Sub UpdateInvoice()
        Dim sql As String
        sql = "UPDATE invoicehd SET  paid = paid + " & Convert.ToDouble(strFncDebt.discount) + Convert.ToDouble(strFncDebt.amount) & " , tax  =  tax + " & strFncDebt.vat & " WHERE  invdocid = " & strFncDebt.invdocid & ""
        connect.ExecuteNonQuery(sql)
    End Sub
    Public Shared Function generateIDYearAndRuning(tableName As String, pk As String) As String
        Dim db = ConnecDBRYH.NewConnection
        Dim dt As New DataTable
        Dim curdate As String = ""
        Dim dbHead As String = ""
        Dim dbRuning As String = ""
        Dim headLeight As Integer = 2
        Dim runLeight As Integer = 6

        db.BeginTrans()
        Dim id As String = ""
        Dim sql As String = "SELECT " & pk & ",SUBSTRING(" & pk & "," & headLeight + 1 & "," & runLeight & ") AS run,SUBSTRING(" & pk & ",1," & headLeight & ") AS head FROM  " & tableName & " ORDER BY " & pk & " DESC LIMIT 1;"
        dt = db.GetTable(sql)
        If dt.Rows.Count > 0 Then
            dbHead = dt.Rows(0)("head").ToString
            dbRuning = dt.Rows(0)("run").ToString
        End If
        sql = "SELECT DATE_FORMAT(CURRENT_TIMESTAMP(),'%y') cur"
        curdate = db.GetTable(sql).Rows(0)("cur").ToString
        If curdate = dbHead Then
            Dim numFormat As String = ""
            For i As Integer = 0 To runLeight - 1
                numFormat &= "0"
            Next i
            id = dbHead & (Convert.ToInt32(dbRuning) + 1).ToString(numFormat)
        Else
            Dim numFormat As String = ""
            For i As Integer = 0 To runLeight - 1
                If i = runLeight - 1 Then
                    numFormat &= "1"
                Else
                    numFormat &= "0"
                End If
            Next i
            id = curdate & numFormat
        End If
        db.CommitTrans()
        db.Dispose()
        Return id
    End Function

End Class
