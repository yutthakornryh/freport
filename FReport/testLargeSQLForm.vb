﻿Public Class testLargeSQLForm
    Dim sqlclass As New SQLClass
    Dim regDataset As New regDataset
    Private Sub testLargeSQLForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '  initUI()
        addtestData()
        PivotGridControl1.DataSource = regDataset
        PivotGridControl1.ForceInitialize()
    End Sub

    Private Sub initUI()
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub addtestData()
        Try
            Dim condb As ConnecDBRYH = ConnecDBRYH.NewConnection
            Dim sql As String = sqlclass.testLargeSQL()
            condb.GetTable(sql, regDataset.Tables("TestLargeData"))
            condb.Dispose()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub
End Class