﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class MadicalCertReport
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Code128Generator1 As DevExpress.XtraPrinting.BarCode.Code128Generator = New DevExpress.XtraPrinting.BarCode.Code128Generator()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MadicalCertReport))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrBarCode1 = New DevExpress.XtraReports.UI.XRBarCode()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrLine14 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine15 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine13 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine12 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine11 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine10 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine9 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine8 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine7 = New DevExpress.XtraReports.UI.XRLine()
        Me.docNameF = New DevExpress.XtraReports.UI.XRLabel()
        Me.dateNowField = New DevExpress.XtraReports.UI.XRLabel()
        Me.finYearField = New DevExpress.XtraReports.UI.XRLabel()
        Me.finMonthField = New DevExpress.XtraReports.UI.XRLabel()
        Me.finDateField = New DevExpress.XtraReports.UI.XRLabel()
        Me.stYearField = New DevExpress.XtraReports.UI.XRLabel()
        Me.startMonthField = New DevExpress.XtraReports.UI.XRLabel()
        Me.stDateField = New DevExpress.XtraReports.UI.XRLabel()
        Me.DayoffField = New DevExpress.XtraReports.UI.XRLabel()
        Me.AdviceField = New DevExpress.XtraReports.UI.XRLabel()
        Me.AddressField = New DevExpress.XtraReports.UI.XRLabel()
        Me.HNField = New DevExpress.XtraReports.UI.XRLabel()
        Me.PatientAgeField = New DevExpress.XtraReports.UI.XRLabel()
        Me.patientNameField = New DevExpress.XtraReports.UI.XRLabel()
        Me.doctorNameField = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine24 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine23 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine22 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine21 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine20 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine19 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine18 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine17 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine6 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine5 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine4 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine3 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel24 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel23 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel22 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel21 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel20 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel19 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel18 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel17 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel16 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel14 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel12 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel11 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel10 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel9 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.DxNoteField = New DevExpress.XtraReports.UI.XRLabel()
        Me.doctorCertField = New DevExpress.XtraReports.UI.XRLabel()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.PageHeader = New DevExpress.XtraReports.UI.PageHeaderBand()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrBarCode1, Me.XrPageInfo2, Me.XrLine14, Me.XrLine15, Me.XrLine13, Me.XrLine12, Me.XrLine11, Me.XrLine10, Me.XrLine9, Me.XrLine8, Me.XrLine7, Me.docNameF, Me.dateNowField, Me.finYearField, Me.finMonthField, Me.finDateField, Me.stYearField, Me.startMonthField, Me.stDateField, Me.DayoffField, Me.AdviceField, Me.AddressField, Me.HNField, Me.PatientAgeField, Me.patientNameField, Me.doctorNameField, Me.XrLine24, Me.XrLine23, Me.XrLine22, Me.XrLine21, Me.XrLine20, Me.XrLine19, Me.XrLine18, Me.XrLine17, Me.XrLine6, Me.XrLine5, Me.XrLine4, Me.XrLine3, Me.XrLine2, Me.XrLine1, Me.XrLabel24, Me.XrLabel23, Me.XrLabel22, Me.XrLabel21, Me.XrLabel20, Me.XrLabel19, Me.XrLabel18, Me.XrLabel17, Me.XrLabel16, Me.XrLabel15, Me.XrLabel14, Me.XrLabel13, Me.XrLabel12, Me.XrLabel11, Me.XrLabel10, Me.XrLabel9, Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.DxNoteField, Me.doctorCertField})
        Me.Detail.Dpi = 254.0!
        Me.Detail.Font = New System.Drawing.Font("CordiaUPC", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Detail.HeightF = 2007.031!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.Detail.StylePriority.UseFont = False
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrBarCode1
        '
        Me.XrBarCode1.Dpi = 254.0!
        Me.XrBarCode1.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrBarCode1.LocationFloat = New DevExpress.Utils.PointFloat(1008.729!, 1898.339!)
        Me.XrBarCode1.Module = 5.08!
        Me.XrBarCode1.Name = "XrBarCode1"
        Me.XrBarCode1.Padding = New DevExpress.XtraPrinting.PaddingInfo(25, 25, 0, 0, 254.0!)
        Me.XrBarCode1.ShowText = False
        Me.XrBarCode1.SizeF = New System.Drawing.SizeF(631.271!, 108.691!)
        Me.XrBarCode1.StylePriority.UseFont = False
        Me.XrBarCode1.StylePriority.UseTextAlignment = False
        Me.XrBarCode1.Symbology = Code128Generator1
        Me.XrBarCode1.Text = "1003439"
        Me.XrBarCode1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.Dpi = 254.0!
        Me.XrPageInfo2.Font = New System.Drawing.Font("Cordia New", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrPageInfo2.Format = "{0:พิมพ์วันที่ dd/MM/yyyy HH:mm:ss}"
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 1948.61!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(560.3596!, 58.42!)
        Me.XrPageInfo2.StylePriority.UseFont = False
        '
        'XrLine14
        '
        Me.XrLine14.Dpi = 254.0!
        Me.XrLine14.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine14.LineWidth = 3
        Me.XrLine14.LocationFloat = New DevExpress.Utils.PointFloat(10.58333!, 800.2916!)
        Me.XrLine14.Name = "XrLine14"
        Me.XrLine14.SizeF = New System.Drawing.SizeF(1597.291!, 10.58337!)
        '
        'XrLine15
        '
        Me.XrLine15.Dpi = 254.0!
        Me.XrLine15.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine15.LineWidth = 3
        Me.XrLine15.LocationFloat = New DevExpress.Utils.PointFloat(10.58333!, 877.0208!)
        Me.XrLine15.Name = "XrLine15"
        Me.XrLine15.SizeF = New System.Drawing.SizeF(1597.291!, 10.58331!)
        '
        'XrLine13
        '
        Me.XrLine13.Dpi = 254.0!
        Me.XrLine13.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine13.LineWidth = 3
        Me.XrLine13.LocationFloat = New DevExpress.Utils.PointFloat(10.58333!, 718.4374!)
        Me.XrLine13.Name = "XrLine13"
        Me.XrLine13.SizeF = New System.Drawing.SizeF(1597.291!, 10.58331!)
        '
        'XrLine12
        '
        Me.XrLine12.Dpi = 254.0!
        Me.XrLine12.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine12.LineWidth = 3
        Me.XrLine12.LocationFloat = New DevExpress.Utils.PointFloat(10.58333!, 644.3541!)
        Me.XrLine12.Name = "XrLine12"
        Me.XrLine12.SizeF = New System.Drawing.SizeF(1597.291!, 10.58337!)
        '
        'XrLine11
        '
        Me.XrLine11.Dpi = 254.0!
        Me.XrLine11.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine11.LineWidth = 3
        Me.XrLine11.LocationFloat = New DevExpress.Utils.PointFloat(10.58333!, 567.6249!)
        Me.XrLine11.Name = "XrLine11"
        Me.XrLine11.SizeF = New System.Drawing.SizeF(1597.291!, 10.58325!)
        '
        'XrLine10
        '
        Me.XrLine10.Dpi = 254.0!
        Me.XrLine10.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine10.LineWidth = 3
        Me.XrLine10.LocationFloat = New DevExpress.Utils.PointFloat(10.58333!, 493.5416!)
        Me.XrLine10.Name = "XrLine10"
        Me.XrLine10.SizeF = New System.Drawing.SizeF(1597.291!, 10.58334!)
        '
        'XrLine9
        '
        Me.XrLine9.Dpi = 254.0!
        Me.XrLine9.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine9.LineWidth = 3
        Me.XrLine9.LocationFloat = New DevExpress.Utils.PointFloat(10.58333!, 416.8124!)
        Me.XrLine9.Name = "XrLine9"
        Me.XrLine9.SizeF = New System.Drawing.SizeF(1597.291!, 10.58334!)
        '
        'XrLine8
        '
        Me.XrLine8.Dpi = 254.0!
        Me.XrLine8.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine8.LineWidth = 3
        Me.XrLine8.LocationFloat = New DevExpress.Utils.PointFloat(10.58333!, 343.9583!)
        Me.XrLine8.Name = "XrLine8"
        Me.XrLine8.SizeF = New System.Drawing.SizeF(1597.291!, 10.58331!)
        '
        'XrLine7
        '
        Me.XrLine7.Dpi = 254.0!
        Me.XrLine7.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine7.LineWidth = 3
        Me.XrLine7.LocationFloat = New DevExpress.Utils.PointFloat(335.2292!, 275.1667!)
        Me.XrLine7.Name = "XrLine7"
        Me.XrLine7.SizeF = New System.Drawing.SizeF(1275.292!, 10.5834!)
        '
        'docNameF
        '
        Me.docNameF.Dpi = 254.0!
        Me.docNameF.LocationFloat = New DevExpress.Utils.PointFloat(1026.583!, 1441.821!)
        Me.docNameF.Name = "docNameF"
        Me.docNameF.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.docNameF.SizeF = New System.Drawing.SizeF(524.5417!, 52.91675!)
        Me.docNameF.StylePriority.UseTextAlignment = False
        Me.docNameF.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'dateNowField
        '
        Me.dateNowField.Dpi = 254.0!
        Me.dateNowField.LocationFloat = New DevExpress.Utils.PointFloat(1079.5!, 1502.184!)
        Me.dateNowField.Name = "dateNowField"
        Me.dateNowField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.dateNowField.SizeF = New System.Drawing.SizeF(508.6666!, 58.42004!)
        Me.dateNowField.StylePriority.UseTextAlignment = False
        Me.dateNowField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'finYearField
        '
        Me.finYearField.Dpi = 254.0!
        Me.finYearField.LocationFloat = New DevExpress.Utils.PointFloat(946.9487!, 1100.692!)
        Me.finYearField.Name = "finYearField"
        Me.finYearField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.finYearField.SizeF = New System.Drawing.SizeF(242.0939!, 58.42004!)
        Me.finYearField.StylePriority.UseTextAlignment = False
        Me.finYearField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'finMonthField
        '
        Me.finMonthField.Dpi = 254.0!
        Me.finMonthField.LocationFloat = New DevExpress.Utils.PointFloat(465.407!, 1100.692!)
        Me.finMonthField.Name = "finMonthField"
        Me.finMonthField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.finMonthField.SizeF = New System.Drawing.SizeF(357.1875!, 58.42004!)
        Me.finMonthField.StylePriority.UseTextAlignment = False
        Me.finMonthField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'finDateField
        '
        Me.finDateField.Dpi = 254.0!
        Me.finDateField.LocationFloat = New DevExpress.Utils.PointFloat(195.0004!, 1100.692!)
        Me.finDateField.Name = "finDateField"
        Me.finDateField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.finDateField.SizeF = New System.Drawing.SizeF(156.6358!, 58.42004!)
        Me.finDateField.StylePriority.UseTextAlignment = False
        Me.finDateField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'stYearField
        '
        Me.stYearField.Dpi = 254.0!
        Me.stYearField.LocationFloat = New DevExpress.Utils.PointFloat(1298.844!, 1042.272!)
        Me.stYearField.Name = "stYearField"
        Me.stYearField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.stYearField.SizeF = New System.Drawing.SizeF(288.6556!, 58.42004!)
        Me.stYearField.StylePriority.UseTextAlignment = False
        Me.stYearField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'startMonthField
        '
        Me.startMonthField.Dpi = 254.0!
        Me.startMonthField.LocationFloat = New DevExpress.Utils.PointFloat(933.7189!, 1042.272!)
        Me.startMonthField.Name = "startMonthField"
        Me.startMonthField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.startMonthField.SizeF = New System.Drawing.SizeF(254.0!, 58.42!)
        Me.startMonthField.StylePriority.UseTextAlignment = False
        Me.startMonthField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'stDateField
        '
        Me.stDateField.Dpi = 254.0!
        Me.stDateField.LocationFloat = New DevExpress.Utils.PointFloat(645.323!, 1042.272!)
        Me.stDateField.Name = "stDateField"
        Me.stDateField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.stDateField.SizeF = New System.Drawing.SizeF(174.625!, 58.42004!)
        Me.stDateField.StylePriority.UseTextAlignment = False
        Me.stDateField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'DayoffField
        '
        Me.DayoffField.Dpi = 254.0!
        Me.DayoffField.LocationFloat = New DevExpress.Utils.PointFloat(195.0004!, 1042.272!)
        Me.DayoffField.Name = "DayoffField"
        Me.DayoffField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DayoffField.SizeF = New System.Drawing.SizeF(153.9893!, 58.42004!)
        Me.DayoffField.StylePriority.UseTextAlignment = False
        Me.DayoffField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'AdviceField
        '
        Me.AdviceField.Dpi = 254.0!
        Me.AdviceField.LocationFloat = New DevExpress.Utils.PointFloat(195.0004!, 983.8521!)
        Me.AdviceField.Name = "AdviceField"
        Me.AdviceField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.AdviceField.SizeF = New System.Drawing.SizeF(1391.708!, 58.41998!)
        Me.AdviceField.StylePriority.UseTextAlignment = False
        Me.AdviceField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        '
        'AddressField
        '
        Me.AddressField.Dpi = 254.0!
        Me.AddressField.LocationFloat = New DevExpress.Utils.PointFloat(484.1869!, 140.7325!)
        Me.AddressField.Name = "AddressField"
        Me.AddressField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.AddressField.SizeF = New System.Drawing.SizeF(1123.688!, 63.71167!)
        Me.AddressField.StylePriority.UseTextAlignment = False
        Me.AddressField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'HNField
        '
        Me.HNField.Dpi = 254.0!
        Me.HNField.LocationFloat = New DevExpress.Utils.PointFloat(116.4167!, 140.4409!)
        Me.HNField.Name = "HNField"
        Me.HNField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.HNField.SizeF = New System.Drawing.SizeF(254.0!, 58.42!)
        Me.HNField.StylePriority.UseTextAlignment = False
        Me.HNField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'PatientAgeField
        '
        Me.PatientAgeField.Dpi = 254.0!
        Me.PatientAgeField.LocationFloat = New DevExpress.Utils.PointFloat(1473.729!, 82.02084!)
        Me.PatientAgeField.Name = "PatientAgeField"
        Me.PatientAgeField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.PatientAgeField.SizeF = New System.Drawing.SizeF(76.72925!, 58.41998!)
        Me.PatientAgeField.StylePriority.UseTextAlignment = False
        Me.PatientAgeField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        Me.PatientAgeField.WordWrap = False
        '
        'patientNameField
        '
        Me.patientNameField.Dpi = 254.0!
        Me.patientNameField.LocationFloat = New DevExpress.Utils.PointFloat(922.0732!, 82.02084!)
        Me.patientNameField.Name = "patientNameField"
        Me.patientNameField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.patientNameField.SizeF = New System.Drawing.SizeF(459.0518!, 58.42001!)
        Me.patientNameField.StylePriority.UseTextAlignment = False
        Me.patientNameField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'doctorNameField
        '
        Me.doctorNameField.Dpi = 254.0!
        Me.doctorNameField.LocationFloat = New DevExpress.Utils.PointFloat(407.4583!, 23.60083!)
        Me.doctorNameField.Name = "doctorNameField"
        Me.doctorNameField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.doctorNameField.SizeF = New System.Drawing.SizeF(769.9375!, 58.42!)
        Me.doctorNameField.StylePriority.UseTextAlignment = False
        Me.doctorNameField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'XrLine24
        '
        Me.XrLine24.Dpi = 254.0!
        Me.XrLine24.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine24.LineWidth = 3
        Me.XrLine24.LocationFloat = New DevExpress.Utils.PointFloat(946.4171!, 1151.466!)
        Me.XrLine24.Name = "XrLine24"
        Me.XrLine24.SizeF = New System.Drawing.SizeF(243.4167!, 7.9375!)
        '
        'XrLine23
        '
        Me.XrLine23.Dpi = 254.0!
        Me.XrLine23.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine23.LineWidth = 3
        Me.XrLine23.LocationFloat = New DevExpress.Utils.PointFloat(464.8754!, 1154.112!)
        Me.XrLine23.Name = "XrLine23"
        Me.XrLine23.SizeF = New System.Drawing.SizeF(359.8333!, 5.291626!)
        '
        'XrLine22
        '
        Me.XrLine22.Dpi = 254.0!
        Me.XrLine22.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine22.LineWidth = 3
        Me.XrLine22.LocationFloat = New DevExpress.Utils.PointFloat(195.0004!, 1154.404!)
        Me.XrLine22.Name = "XrLine22"
        Me.XrLine22.SizeF = New System.Drawing.SizeF(161.3958!, 5.0!)
        '
        'XrLine21
        '
        Me.XrLine21.Dpi = 254.0!
        Me.XrLine21.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine21.LineWidth = 3
        Me.XrLine21.LocationFloat = New DevExpress.Utils.PointFloat(1298.312!, 1095.692!)
        Me.XrLine21.Name = "XrLine21"
        Me.XrLine21.SizeF = New System.Drawing.SizeF(298.9792!, 5.000122!)
        '
        'XrLine20
        '
        Me.XrLine20.Dpi = 254.0!
        Me.XrLine20.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine20.LineWidth = 3
        Me.XrLine20.LocationFloat = New DevExpress.Utils.PointFloat(933.1873!, 1095.692!)
        Me.XrLine20.Name = "XrLine20"
        Me.XrLine20.SizeF = New System.Drawing.SizeF(259.2917!, 5.291748!)
        '
        'XrLine19
        '
        Me.XrLine19.Dpi = 254.0!
        Me.XrLine19.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine19.LineWidth = 3
        Me.XrLine19.LocationFloat = New DevExpress.Utils.PointFloat(644.7914!, 1095.692!)
        Me.XrLine19.Name = "XrLine19"
        Me.XrLine19.SizeF = New System.Drawing.SizeF(174.625!, 5.291626!)
        '
        'XrLine18
        '
        Me.XrLine18.Dpi = 254.0!
        Me.XrLine18.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine18.LineWidth = 3
        Me.XrLine18.LocationFloat = New DevExpress.Utils.PointFloat(194.4688!, 1095.4!)
        Me.XrLine18.Name = "XrLine18"
        Me.XrLine18.SizeF = New System.Drawing.SizeF(153.9893!, 5.583496!)
        '
        'XrLine17
        '
        Me.XrLine17.Dpi = 254.0!
        Me.XrLine17.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine17.LineWidth = 3
        Me.XrLine17.LocationFloat = New DevExpress.Utils.PointFloat(195.0004!, 1042.272!)
        Me.XrLine17.Name = "XrLine17"
        Me.XrLine17.SizeF = New System.Drawing.SizeF(1397.0!, 5.0!)
        '
        'XrLine6
        '
        Me.XrLine6.Dpi = 254.0!
        Me.XrLine6.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine6.LineWidth = 3
        Me.XrLine6.LocationFloat = New DevExpress.Utils.PointFloat(489.4792!, 193.5692!)
        Me.XrLine6.Name = "XrLine6"
        Me.XrLine6.SizeF = New System.Drawing.SizeF(1119.188!, 10.58333!)
        '
        'XrLine5
        '
        Me.XrLine5.Dpi = 254.0!
        Me.XrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine5.LineWidth = 3
        Me.XrLine5.LocationFloat = New DevExpress.Utils.PointFloat(111.125!, 193.5692!)
        Me.XrLine5.Name = "XrLine5"
        Me.XrLine5.SizeF = New System.Drawing.SizeF(284.4271!, 5.291687!)
        '
        'XrLine4
        '
        Me.XrLine4.Dpi = 254.0!
        Me.XrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine4.LineWidth = 3
        Me.XrLine4.LocationFloat = New DevExpress.Utils.PointFloat(1476.375!, 135.4409!)
        Me.XrLine4.Name = "XrLine4"
        Me.XrLine4.SizeF = New System.Drawing.SizeF(74.08337!, 5.291672!)
        '
        'XrLine3
        '
        Me.XrLine3.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash
        Me.XrLine3.Dpi = 254.0!
        Me.XrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine3.LineWidth = 3
        Me.XrLine3.LocationFloat = New DevExpress.Utils.PointFloat(920.75!, 135.4409!)
        Me.XrLine3.Name = "XrLine3"
        Me.XrLine3.SizeF = New System.Drawing.SizeF(470.9584!, 5.0!)
        Me.XrLine3.StylePriority.UseBorderDashStyle = False
        '
        'XrLine2
        '
        Me.XrLine2.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash
        Me.XrLine2.Dpi = 254.0!
        Me.XrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine2.LineWidth = 3
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(365.125!, 135.4409!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(296.3333!, 5.0!)
        Me.XrLine2.StylePriority.UseBorderDashStyle = False
        '
        'XrLine1
        '
        Me.XrLine1.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Dash
        Me.XrLine1.Dpi = 254.0!
        Me.XrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot
        Me.XrLine1.LineWidth = 3
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(269.875!, 71.4375!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(986.8959!, 10.58334!)
        Me.XrLine1.StylePriority.UseBorderDashStyle = False
        '
        'XrLabel24
        '
        Me.XrLabel24.Dpi = 254.0!
        Me.XrLabel24.LocationFloat = New DevExpress.Utils.PointFloat(984.25!, 1502.184!)
        Me.XrLabel24.Name = "XrLabel24"
        Me.XrLabel24.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel24.SizeF = New System.Drawing.SizeF(95.25!, 58.42004!)
        Me.XrLabel24.Text = "วันที่"
        '
        'XrLabel23
        '
        Me.XrLabel23.Dpi = 254.0!
        Me.XrLabel23.LocationFloat = New DevExpress.Utils.PointFloat(1551.125!, 1436.318!)
        Me.XrLabel23.Name = "XrLabel23"
        Me.XrLabel23.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel23.SizeF = New System.Drawing.SizeF(50.27069!, 58.42017!)
        Me.XrLabel23.Text = ")"
        '
        'XrLabel22
        '
        Me.XrLabel22.Dpi = 254.0!
        Me.XrLabel22.LocationFloat = New DevExpress.Utils.PointFloat(989.542!, 1436.318!)
        Me.XrLabel22.Name = "XrLabel22"
        Me.XrLabel22.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel22.SizeF = New System.Drawing.SizeF(50.27069!, 58.42017!)
        Me.XrLabel22.Text = "("
        '
        'XrLabel21
        '
        Me.XrLabel21.Dpi = 254.0!
        Me.XrLabel21.LocationFloat = New DevExpress.Utils.PointFloat(920.75!, 1367.79!)
        Me.XrLabel21.Name = "XrLabel21"
        Me.XrLabel21.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel21.SizeF = New System.Drawing.SizeF(105.8333!, 58.42004!)
        Me.XrLabel21.Text = "ลงชื่อ"
        '
        'XrLabel20
        '
        Me.XrLabel20.Dpi = 254.0!
        Me.XrLabel20.LocationFloat = New DevExpress.Utils.PointFloat(827.3546!, 1100.984!)
        Me.XrLabel20.Name = "XrLabel20"
        Me.XrLabel20.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel20.SizeF = New System.Drawing.SizeF(105.8334!, 58.42004!)
        Me.XrLabel20.Text = "พ.ศ."
        '
        'XrLabel19
        '
        Me.XrLabel19.Dpi = 254.0!
        Me.XrLabel19.LocationFloat = New DevExpress.Utils.PointFloat(356.3962!, 1100.984!)
        Me.XrLabel19.Name = "XrLabel19"
        Me.XrLabel19.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel19.SizeF = New System.Drawing.SizeF(105.8334!, 58.42004!)
        Me.XrLabel19.Text = "เดือน"
        '
        'XrLabel18
        '
        Me.XrLabel18.Dpi = 254.0!
        Me.XrLabel18.LocationFloat = New DevExpress.Utils.PointFloat(5.291667!, 1100.984!)
        Me.XrLabel18.Name = "XrLabel18"
        Me.XrLabel18.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel18.SizeF = New System.Drawing.SizeF(177.2708!, 58.42004!)
        Me.XrLabel18.Text = "จนถึงวันที่"
        '
        'XrLabel17
        '
        Me.XrLabel17.Dpi = 254.0!
        Me.XrLabel17.LocationFloat = New DevExpress.Utils.PointFloat(1189.833!, 1042.564!)
        Me.XrLabel17.Name = "XrLabel17"
        Me.XrLabel17.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel17.SizeF = New System.Drawing.SizeF(103.1875!, 58.42004!)
        Me.XrLabel17.Text = "พ.ศ."
        '
        'XrLabel16
        '
        Me.XrLabel16.Dpi = 254.0!
        Me.XrLabel16.LocationFloat = New DevExpress.Utils.PointFloat(824.7081!, 1042.564!)
        Me.XrLabel16.Name = "XrLabel16"
        Me.XrLabel16.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel16.SizeF = New System.Drawing.SizeF(105.8334!, 58.42004!)
        Me.XrLabel16.Text = "เดือน"
        '
        'XrLabel15
        '
        Me.XrLabel15.Dpi = 254.0!
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(454.2914!, 1042.564!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(182.5625!, 58.42004!)
        Me.XrLabel15.Text = "ตั้งแต่วันที่"
        '
        'XrLabel14
        '
        Me.XrLabel14.Dpi = 254.0!
        Me.XrLabel14.LocationFloat = New DevExpress.Utils.PointFloat(353.7497!, 1042.564!)
        Me.XrLabel14.Name = "XrLabel14"
        Me.XrLabel14.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel14.SizeF = New System.Drawing.SizeF(63.50006!, 58.42004!)
        Me.XrLabel14.Text = "วัน"
        '
        'XrLabel13
        '
        Me.XrLabel13.Dpi = 254.0!
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(5.291667!, 1042.564!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(153.4583!, 58.42004!)
        Me.XrLabel13.Text = "มีกำหนด"
        '
        'XrLabel12
        '
        Me.XrLabel12.Dpi = 254.0!
        Me.XrLabel12.LocationFloat = New DevExpress.Utils.PointFloat(5.291667!, 984.1439!)
        Me.XrLabel12.Name = "XrLabel12"
        Me.XrLabel12.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel12.SizeF = New System.Drawing.SizeF(185.2083!, 58.42004!)
        Me.XrLabel12.Text = "เห็นสมควร"
        '
        'XrLabel11
        '
        Me.XrLabel11.Dpi = 254.0!
        Me.XrLabel11.LocationFloat = New DevExpress.Utils.PointFloat(134.1459!, 214.2066!)
        Me.XrLabel11.Name = "XrLabel11"
        Me.XrLabel11.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel11.SizeF = New System.Drawing.SizeF(195.7917!, 58.41995!)
        Me.XrLabel11.Text = "ขอรับรองว่า"
        '
        'XrLabel10
        '
        Me.XrLabel10.Dpi = 254.0!
        Me.XrLabel10.LocationFloat = New DevExpress.Utils.PointFloat(395.5521!, 140.4409!)
        Me.XrLabel10.Name = "XrLabel10"
        Me.XrLabel10.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel10.SizeF = New System.Drawing.SizeF(87.31247!, 58.41998!)
        Me.XrLabel10.Text = "ที่อยู่"
        '
        'XrLabel9
        '
        Me.XrLabel9.Dpi = 254.0!
        Me.XrLabel9.LocationFloat = New DevExpress.Utils.PointFloat(5.291667!, 140.4409!)
        Me.XrLabel9.Name = "XrLabel9"
        Me.XrLabel9.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel9.SizeF = New System.Drawing.SizeF(95.25!, 58.41998!)
        Me.XrLabel9.Text = "H.N."
        '
        'XrLabel8
        '
        Me.XrLabel8.Dpi = 254.0!
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(1544.375!, 82.02084!)
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(47.625!, 58.42001!)
        Me.XrLabel8.Text = "ปี"
        '
        'XrLabel7
        '
        Me.XrLabel7.Dpi = 254.0!
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(1389.063!, 82.02084!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(84.66663!, 58.42001!)
        Me.XrLabel7.Text = "อายุ"
        '
        'XrLabel6
        '
        Me.XrLabel6.Dpi = 254.0!
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(666.75!, 82.02084!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(254.0!, 58.42!)
        Me.XrLabel6.Text = "ได้ตรวจร่างกาย"
        '
        'XrLabel5
        '
        Me.XrLabel5.Dpi = 254.0!
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(5.291667!, 82.02084!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(365.125!, 58.42001!)
        Me.XrLabel5.Text = "สาขาเวชกรรม เลขที่"
        '
        'XrLabel4
        '
        Me.XrLabel4.Dpi = 254.0!
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(1260.083!, 23.60083!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(306.9167!, 58.42!)
        Me.XrLabel4.Text = "แพทย์แผนปัจจุบัน"
        '
        'XrLabel3
        '
        Me.XrLabel3.Dpi = 254.0!
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(129.6458!, 23.60083!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(140.2292!, 58.42!)
        Me.XrLabel3.Text = "ข้าพเจ้า"
        '
        'DxNoteField
        '
        Me.DxNoteField.Dpi = 254.0!
        Me.DxNoteField.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 214.2066!)
        Me.DxNoteField.Multiline = True
        Me.DxNoteField.Name = "DxNoteField"
        Me.DxNoteField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.DxNoteField.SizeF = New System.Drawing.SizeF(1597.291!, 750.1266!)
        '
        'doctorCertField
        '
        Me.doctorCertField.Dpi = 254.0!
        Me.doctorCertField.LocationFloat = New DevExpress.Utils.PointFloat(365.125!, 82.31248!)
        Me.doctorCertField.Name = "doctorCertField"
        Me.doctorCertField.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.doctorCertField.SizeF = New System.Drawing.SizeF(288.3958!, 58.42001!)
        Me.doctorCertField.StylePriority.UseTextAlignment = False
        Me.doctorCertField.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'TopMargin
        '
        Me.TopMargin.Dpi = 254.0!
        Me.TopMargin.HeightF = 257.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 254.0!
        Me.BottomMargin.HeightF = 76.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel1
        '
        Me.XrLabel1.Dpi = 254.0!
        Me.XrLabel1.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(412.75!, 203.5175!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(759.3545!, 98.10753!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.Text = "โรงพยาบาลราษฎร์ยินดี"
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel1, Me.XrPictureBox1})
        Me.PageHeader.Dpi = 254.0!
        Me.PageHeader.HeightF = 378.3542!
        Me.PageHeader.Name = "PageHeader"
        '
        'XrLabel2
        '
        Me.XrLabel2.Dpi = 254.0!
        Me.XrLabel2.Font = New System.Drawing.Font("Cordia New", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(194.4688!, 301.625!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(1195.917!, 58.42!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.Text = "119 ถ.ราษฎร์ยินดี อ.หาดใหญ่ จ.สงขลา 90110 โทร. 074-200-200 FAX 074-200-292"
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Dpi = 254.0!
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(695.8542!, 0.0!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(193.1459!, 185.42!)
        '
        'MadicalCertReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageHeader})
        Me.Dpi = 254.0!
        Me.Margins = New System.Drawing.Printing.Margins(216, 244, 257, 76)
        Me.PageHeight = 2970
        Me.PageWidth = 2100
        Me.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.Version = "14.1"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents docNameF As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents dateNowField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents finYearField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents finMonthField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents finDateField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents stYearField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents startMonthField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents stDateField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DayoffField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents DxNoteField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents AdviceField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents AddressField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents HNField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PatientAgeField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents patientNameField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents doctorCertField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents doctorNameField As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine24 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine23 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine22 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine21 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine20 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine19 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine18 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine17 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine6 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine5 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine4 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine3 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel24 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel23 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel22 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel21 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel20 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel19 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel18 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel17 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel16 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel14 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel12 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel11 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel10 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel9 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents PageHeader As DevExpress.XtraReports.UI.PageHeaderBand
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine14 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine15 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine13 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine12 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine11 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine10 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine9 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine8 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine7 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrBarCode1 As DevExpress.XtraReports.UI.XRBarCode
End Class
