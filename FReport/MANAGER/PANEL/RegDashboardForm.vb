﻿Imports System.IO
Imports Microsoft.Office.Interop.Excel
Imports DevExpress.XtraCharts
Imports DevExpress.XtraPivotGrid
Imports DevExpress.XtraEditors
Imports DevExpress.Data.PivotGrid
Imports System.Windows.Forms
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class RegDashboardForm
    Dim regDataset As New regDataset
    Dim sqlclass As New SQLClass
    'Dim filename As String = System.Windows.Forms.Application.StartupPath & "\\report\\"
    Dim filename As String = "D:\\fProject\\test"
    Dim printSystem As New PrintingSystem



    Private Sub RegDashboardForm_Load(sender As Object, e As EventArgs) Handles Me.Load
        initUI()
        addRegfData()
        addCustomsFieldtoPivot()
    End Sub

    Private Sub initUI()
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.WindowState = FormWindowState.Maximized

        dfStartedDate.EditValue = Date.Now.AddDays(-1)
        dfEndDate.EditValue = Date.Now

    End Sub


    Private Sub addCustomsFieldtoPivot()

        ' Get a reference to the CategoryName field.
        Dim waiting_field As PivotGridField = PivotGridControl2.Fields("CLINIC")
        Dim working_field As PivotGridField = PivotGridControl2.Fields("SERVICE_DATE")
        PivotGridControl2.BeginUpdate()
        Try
            ' Clear the custom total collection.
            waiting_field.CustomTotals.Clear()
            working_field.CustomTotals.Clear()
            ' Add four items to the custom total collection to calculate the Average, 
            ' Sum, Max and Min summaries.
            waiting_field.CustomTotals.Add(PivotSummaryType.Average)
            waiting_field.CustomTotals.Add(PivotSummaryType.Max)
            waiting_field.CustomTotals.Add(PivotSummaryType.Min)

            working_field.CustomTotals.Add(PivotSummaryType.Average)
            working_field.CustomTotals.Add(PivotSummaryType.Max)
            working_field.CustomTotals.Add(PivotSummaryType.Min)
            ' Make the custom totals visible for this field.
            waiting_field.TotalsVisibility = PivotTotalsVisibility.CustomTotals
            working_field.TotalsVisibility = PivotTotalsVisibility.CustomTotals
        Finally
            PivotGridControl2.EndUpdate()
        End Try

        add_Constant_line()
    End Sub

    Private Sub addRegfData()
        Try
            Dim condb As ConnecDBRYH = ConnecDBRYH.NewConnection
            Dim sql As String = sqlclass.get_regsclinic_sql()
            condb.GetTable(sql, regDataset.Tables("REG_SCLINIC"))
            condb.Dispose()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub

    Private Sub addServiceTimeData()
        Dim condb As ConnecDBRYH = ConnecDBRYH.NewConnection
        Dim sql As String = sqlclass.getClinicServiceTime(dfStartedDate.EditValue, dfEndDate.EditValue)
        condb.GetTable(sql, regDataset.Tables("CLINIC_SERVICETIME"))
    End Sub
    Private Sub exportToExcel_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles exportToExcel.ItemClick
        Try
            ExportData()
            printSystem.ExportToXlsx(filename & ".xlsx")
            System.Diagnostics.Process.Start(filename & ".xlsx")

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub BarButtonItem2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        Try
            ExportData()
            printSystem.ExportToPdf(filename & ".pdf")
            System.Diagnostics.Process.Start(filename & ".pdf")

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub

    Private Sub add_Constant_line()
        ' Cast the chart's diagram to the XYDiagram type, to access its axes.
        Dim diagram As XYDiagram = CType(ChartControl2.Diagram, XYDiagram)

        ' Create a constant line.
        Dim waitingtimeconstantLine As New ConstantLine("waiting threshold")
        set_constant_line(waitingtimeconstantLine, "Threshold: เวลารอตรวจ (นาที)", 20, Color.DarkSalmon)
        diagram.AxisY.ConstantLines.Add(waitingtimeconstantLine)

        Dim workingtimeconstantLine As New ConstantLine("working threshold")
        set_constant_line(workingtimeconstantLine, "Threshold: เวลาตรวจ (นาที)", 40, Color.SandyBrown)
        diagram.AxisY.ConstantLines.Add(workingtimeconstantLine)
    End Sub

    Private Sub set_constant_line(ByRef constantline As ConstantLine, ByVal nameConstant As String, ByVal value As Decimal, ByVal color As Color)

        ' Define its axis value.
        constantline.AxisValue = value

        ' Customize the behavior of the constant line.
        constantline.Visible = True
        constantline.ShowInLegend = False
        constantline.LegendText = ""
        constantline.ShowBehind = False

        ' Customize the constant line's title.
        constantline.Title.Visible = True
        constantline.Title.Text = nameConstant & " : " & value
        constantline.Title.TextColor = color
        constantline.Title.Antialiasing = False
        constantline.Title.ShowBelowLine = True
        constantline.Title.Alignment = ConstantLineTitleAlignment.Far

        ' Customize the appearance of the constant line.
        constantline.Color = color
        constantline.LineStyle.DashStyle = DashStyle.Dash
        constantline.LineStyle.Thickness = 2
    End Sub


    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        Dim diagram As XYDiagram = CType(ChartControl2.Diagram, XYDiagram)
        Dim constantLine As ConstantLine

        constantLine = diagram.AxisY.ConstantLines.GetConstantLineByName("waiting threshold")
        constantLine.AxisValue = thresholdWaitingTime.EditValue
        constantLine.Title.Text = "Threshold: เวลารอตรวจ (นาที)" & " : " & thresholdWaitingTime.EditValue

        constantLine = diagram.AxisY.ConstantLines.GetConstantLineByName("working threshold")
        constantLine.AxisValue = thresholdWorkingTime.EditValue
        constantLine.Title.Text = "Threshold: เวลาตรวจ (นาที)" & " : " & thresholdWorkingTime.EditValue

    End Sub

    Private Sub ExportData()

        ' Create objects and define event handlers.
        Dim composLink As CompositeLink = New CompositeLink(printSystem)
        'AddHandler composLink.CreateMarginalHeaderArea, AddressOf composLink_CreateMarginalHeaderArea
        Dim pcLinkPivot As New PrintableComponentLink
        Dim pcLinkChart1 As New PrintableComponentLink
        Dim pcLinkChart2 As New PrintableComponentLink

        Dim linkMainReport As New Link
        AddHandler linkMainReport.CreateDetailArea, AddressOf linkMainReport_CreateDetailArea
        Dim linkPivotGrid As New Link
        ' AddHandler linkPivotGrid.CreateDetailArea, AddressOf linkPivotGrid_CreateDetailArea
        Dim linkChart1 As New Link
        ' AddHandler linkChart1.CreateDetailArea, AddressOf linkChart1_CreateDetailArea
        Dim linkChart2 As New Link
        ' AddHandler linkChart2.CreateDetailArea, AddressOf linkChart2_CreateDetailArea
        Dim linkPageBreak As New Link
        AddHandler linkPageBreak.CreateDetailArea, AddressOf pageBreakLink_CreateDetailArea

        ' Assign the controls to the printing links.
        pcLinkPivot.Component = Me.PivotGridControl2
        pcLinkChart1.Component = Me.ChartControl1
        pcLinkChart2.Component = Me.ChartControl2

        ' Populate the collection of links in the composite link.
        ' The order of operations corresponds to the document structure.
        'composLink.Links.Add(linkPivotGrid)
        composLink.Links.Add(pcLinkPivot)
        composLink.Links.Add(linkPageBreak)
        'composLink.Links.Add(linkMainReport)
        'composLink.Links.Add(linkChart1)
        composLink.Links.Add(pcLinkChart1)
        composLink.Links.Add(linkPageBreak)
        ' composLink.Links.Add(linkChart2)
        composLink.Links.Add(pcLinkChart2)
        composLink.CreateDocument()
        setPagetoPrint()

    End Sub

    Private Sub setPagetoPrint()
        printSystem.PageSettings.Landscape = True
        printSystem.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4
        'printSystem.Document.AutoFitToPagesWidth = 1
    End Sub
    Private Sub PivotGridControl2_CustomDrawFieldValue(sender As Object, e As PivotCustomDrawFieldValueEventArgs) Handles PivotGridControl2.CustomDrawFieldValue
        If (e.ValueType = PivotGridValueType.GrandTotal) AndAlso (e.Field Is Nothing) AndAlso e.Info.IsTopMost Then
            e.Info.Caption = " "
        End If
    End Sub

    Private Sub pageBreakLink_CreateDetailArea(sender As Object, e As CreateAreaEventArgs)
        e.Graph.PrintingSystem.InsertPageBreak(0)
    End Sub

    Private Sub linkMainReport_CreateDetailArea(sender As Object, e As CreateAreaEventArgs)
        Try
            Dim gr As BrickGraphics = printSystem.Graph
            Dim bsf As BrickStringFormat = New BrickStringFormat(StringAlignment.Near, StringAlignment.Center)
            gr.StringFormat = bsf

            Dim textbrick As TextBrick
            Dim headerText As String = "ตารางแสดงเวลา OPD Time (นาทีเฉลี่ย/ผู้ป่วย)"
            gr.Modifier = BrickModifier.Detail
            gr.BeginUnionRect()

            'gr.Font = New System.Drawing.Font("Tahoma", 10, FontStyle.Bold)
            textbrick = gr.DrawString(headerText, Color.Black, New RectangleF(New PointF(0, 160), New SizeF(gr.ClientPageSize.Width, 40)), BorderSide.None)
            ' e.Graph.DrawBrick(textbrick)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
       

    End Sub

    Private Sub linkPivotGrid_CreateDetailArea(sender As Object, e As CreateAreaEventArgs)
        Throw New NotImplementedException
        'Dim tb As TextBrick = New TextBrick()
        'tb.Text = "Northwind Traders"
        'tb.Font = New Font("Arial", 15)
        'tb.Rect = New RectangleF(0, 0, 300, 25)
        'tb.BorderWidth = 0
        'tb.BackColor = Color.Transparent
        'tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        'e.Graph.DrawBrick(tb)
    End Sub

    Private Sub linkChart1_CreateDetailArea(sender As Object, e As CreateAreaEventArgs)
        Throw New NotImplementedException
    End Sub

    Private Sub linkChart2_CreateDetailArea(sender As Object, e As CreateAreaEventArgs)
        Throw New NotImplementedException
    End Sub

    Private Sub composLink_CreateMarginalHeaderArea(sender As Object, e As CreateAreaEventArgs)
        Throw New NotImplementedException
    End Sub

    Private Sub PivotGridControl2_Click(sender As Object, e As EventArgs) Handles PivotGridControl2.Click

    End Sub

    Private Sub btnQueryServiceDate_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnQueryServiceDate.ItemClick
        addServiceTimeData()
        PivotGridControl2.DataSource = regDataset
        PivotGridControl2.ForceInitialize()
    End Sub
End Class