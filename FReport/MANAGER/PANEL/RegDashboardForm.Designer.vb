﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RegDashboardForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim PivotGridCustomTotal1 As DevExpress.XtraPivotGrid.PivotGridCustomTotal = New DevExpress.XtraPivotGrid.PivotGridCustomTotal()
        Dim PivotGridCustomTotal2 As DevExpress.XtraPivotGrid.PivotGridCustomTotal = New DevExpress.XtraPivotGrid.PivotGridCustomTotal()
        Dim SimpleDiagram1 As DevExpress.XtraCharts.SimpleDiagram = New DevExpress.XtraCharts.SimpleDiagram()
        Dim PieSeriesLabel1 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim PieSeriesView1 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        Dim SeriesTitle1 As DevExpress.XtraCharts.SeriesTitle = New DevExpress.XtraCharts.SeriesTitle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RegDashboardForm))
        Dim XyDiagram1 As DevExpress.XtraCharts.XYDiagram = New DevExpress.XtraCharts.XYDiagram()
        Dim Series1 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Dim Series2 As DevExpress.XtraCharts.Series = New DevExpress.XtraCharts.Series()
        Me.PivotGridControl2 = New DevExpress.XtraPivotGrid.PivotGridControl()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.RegDataset2 = New FReport.regDataset()
        Me.clinicNameField = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.workingField = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.waitingField = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.serviceDateField = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.RegDataset1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ChartControl1 = New DevExpress.XtraCharts.ChartControl()
        Me.RibbonControl1 = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.exportToExcel = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.thresholdWaitingTime = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemSpinEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
        Me.thresholdWorkingTime = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemSpinEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.dfStartedDate = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.dfEndDate = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.btnQueryServiceDate = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPage2 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.dataAnalyse = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage1 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.Export = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.ChartControl2 = New DevExpress.XtraCharts.ChartControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.CLINICSERVICETIMEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.PivotGridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RegDataset2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RegDataset1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChartControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(SimpleDiagram1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(PieSeriesView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSpinEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ChartControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(XyDiagram1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Series2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.CLINICSERVICETIMEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PivotGridControl2
        '
        Me.PivotGridControl2.Appearance.Cell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PivotGridControl2.Appearance.Cell.Options.UseFont = True
        Me.PivotGridControl2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BindingSource1, "SERVICE_DATE", True))
        Me.PivotGridControl2.DataMember = "CLINIC_SERVICETIME"
        Me.PivotGridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PivotGridControl2.Fields.AddRange(New DevExpress.XtraPivotGrid.PivotGridField() {Me.clinicNameField, Me.workingField, Me.waitingField, Me.serviceDateField})
        Me.PivotGridControl2.Location = New System.Drawing.Point(2, 2)
        Me.PivotGridControl2.Name = "PivotGridControl2"
        Me.PivotGridControl2.OptionsChartDataSource.ProvideRowTotals = True
        Me.PivotGridControl2.Prefilter.CriteriaString = "[serviceDateField] Between(?, ?)"
        Me.PivotGridControl2.Prefilter.Enabled = False
        Me.PivotGridControl2.Size = New System.Drawing.Size(1228, 306)
        Me.PivotGridControl2.TabIndex = 1
        '
        'BindingSource1
        '
        Me.BindingSource1.DataMember = "CLINIC_SERVICETIME"
        Me.BindingSource1.DataSource = Me.RegDataset2
        Me.BindingSource1.Sort = ""
        '
        'RegDataset2
        '
        Me.RegDataset2.DataSetName = "regDataset"
        '
        'clinicNameField
        '
        Me.clinicNameField.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.clinicNameField.AreaIndex = 1
        Me.clinicNameField.Caption = "ชื่อคลินิค"
        Me.clinicNameField.CustomTotals.AddRange(New DevExpress.XtraPivotGrid.PivotGridCustomTotal() {PivotGridCustomTotal1})
        Me.clinicNameField.FieldName = "CLINIC"
        Me.clinicNameField.Name = "clinicNameField"
        '
        'workingField
        '
        Me.workingField.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.workingField.AreaIndex = 1
        Me.workingField.Caption = "ตรวจเฉลี่ย (นาที)"
        Me.workingField.FieldName = "working"
        Me.workingField.Name = "workingField"
        Me.workingField.Width = 97
        '
        'waitingField
        '
        Me.waitingField.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.waitingField.AreaIndex = 0
        Me.waitingField.Caption = "รอตรวจเฉลี่ย (นาที)"
        Me.waitingField.FieldName = "waiting"
        Me.waitingField.Name = "waitingField"
        Me.waitingField.Width = 106
        '
        'serviceDateField
        '
        Me.serviceDateField.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.serviceDateField.AreaIndex = 0
        Me.serviceDateField.Caption = "วันที่"
        PivotGridCustomTotal2.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        PivotGridCustomTotal2.Format.FormatType = DevExpress.Utils.FormatType.Numeric
        PivotGridCustomTotal2.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        Me.serviceDateField.CustomTotals.AddRange(New DevExpress.XtraPivotGrid.PivotGridCustomTotal() {PivotGridCustomTotal2})
        Me.serviceDateField.FieldName = "SERVICE_DATE"
        Me.serviceDateField.Name = "serviceDateField"
        '
        'ChartControl1
        '
        Me.ChartControl1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ChartControl1.DataSource = Me.PivotGridControl2
        SimpleDiagram1.Dimension = 1
        SimpleDiagram1.EqualPieSize = False
        Me.ChartControl1.Diagram = SimpleDiagram1
        Me.ChartControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartControl1.EmptyChartText.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ChartControl1.Legend.Antialiasing = True
        Me.ChartControl1.Legend.Font = New System.Drawing.Font("Tahoma", 6.0!)
        Me.ChartControl1.Legend.MaxHorizontalPercentage = 30.0R
        Me.ChartControl1.Location = New System.Drawing.Point(2, 2)
        Me.ChartControl1.Name = "ChartControl1"
        Me.ChartControl1.SeriesDataMember = "Series"
        Me.ChartControl1.SeriesSerializable = New DevExpress.XtraCharts.Series(-1) {}
        Me.ChartControl1.SeriesTemplate.ArgumentDataMember = "Arguments"
        Me.ChartControl1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        PieSeriesLabel1.Antialiasing = True
        PieSeriesLabel1.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns
        PieSeriesLabel1.TextColor = System.Drawing.Color.DimGray
        PieSeriesLabel1.TextPattern = "{VP:0.00%}"
        Me.ChartControl1.SeriesTemplate.Label = PieSeriesLabel1
        Me.ChartControl1.SeriesTemplate.LegendTextPattern = "{A}"
        Me.ChartControl1.SeriesTemplate.ValueDataMembersSerializable = "Values"
        PieSeriesView1.RuntimeExploding = False
        PieSeriesView1.SweepDirection = DevExpress.XtraCharts.PieSweepDirection.Counterclockwise
        PieSeriesView1.Titles.AddRange(New DevExpress.XtraCharts.SeriesTitle() {SeriesTitle1})
        Me.ChartControl1.SeriesTemplate.View = PieSeriesView1
        Me.ChartControl1.Size = New System.Drawing.Size(590, 102)
        Me.ChartControl1.SmallChartText.Font = New System.Drawing.Font("Tahoma", 6.0!)
        Me.ChartControl1.SmallChartText.TextColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ChartControl1.TabIndex = 2
        '
        'RibbonControl1
        '
        Me.RibbonControl1.ExpandCollapseItem.Id = 0
        Me.RibbonControl1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl1.ExpandCollapseItem, Me.exportToExcel, Me.BarButtonItem2, Me.BarButtonItem3, Me.BarButtonItem4, Me.thresholdWaitingTime, Me.thresholdWorkingTime, Me.BarButtonItem5, Me.dfStartedDate, Me.dfEndDate, Me.btnQueryServiceDate})
        Me.RibbonControl1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl1.MaxItemId = 13
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.RibbonPage2, Me.RibbonPage1})
        Me.RibbonControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemSpinEdit1, Me.RepositoryItemSpinEdit2, Me.RepositoryItemDateEdit1, Me.RepositoryItemDateEdit2})
        Me.RibbonControl1.Size = New System.Drawing.Size(1232, 140)
        '
        'exportToExcel
        '
        Me.exportToExcel.Caption = "Export to Excel"
        Me.exportToExcel.CategoryGuid = New System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537")
        Me.exportToExcel.Glyph = Global.FReport.My.Resources.Resources.sendxls_16x16
        Me.exportToExcel.Id = 1
        Me.exportToExcel.LargeGlyph = Global.FReport.My.Resources.Resources.sendxls_32x32
        Me.exportToExcel.Name = "exportToExcel"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Export to PDF"
        Me.BarButtonItem2.Glyph = Global.FReport.My.Resources.Resources.sendpdf_16x16
        Me.BarButtonItem2.Id = 4
        Me.BarButtonItem2.LargeGlyph = Global.FReport.My.Resources.Resources.sendpdf_32x32
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Quick Print"
        Me.BarButtonItem3.Glyph = Global.FReport.My.Resources.Resources.print_16x16
        Me.BarButtonItem3.Id = 5
        Me.BarButtonItem3.LargeGlyph = Global.FReport.My.Resources.Resources.print_32x32
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Print Dialog"
        Me.BarButtonItem4.Glyph = Global.FReport.My.Resources.Resources.printdialog_16x16
        Me.BarButtonItem4.Id = 6
        Me.BarButtonItem4.LargeGlyph = Global.FReport.My.Resources.Resources.printdialog_32x32
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'thresholdWaitingTime
        '
        Me.thresholdWaitingTime.Caption = "Threshold : เวลารอตรวจ (นาที)"
        Me.thresholdWaitingTime.Edit = Me.RepositoryItemSpinEdit1
        Me.thresholdWaitingTime.EditValue = 20
        Me.thresholdWaitingTime.Id = 7
        Me.thresholdWaitingTime.Name = "thresholdWaitingTime"
        '
        'RepositoryItemSpinEdit1
        '
        Me.RepositoryItemSpinEdit1.AutoHeight = False
        Me.RepositoryItemSpinEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemSpinEdit1.Name = "RepositoryItemSpinEdit1"
        '
        'thresholdWorkingTime
        '
        Me.thresholdWorkingTime.Caption = "Threshold : เวลาตรวจ (นาที)    "
        Me.thresholdWorkingTime.Edit = Me.RepositoryItemSpinEdit2
        Me.thresholdWorkingTime.EditValue = 40
        Me.thresholdWorkingTime.Id = 8
        Me.thresholdWorkingTime.Name = "thresholdWorkingTime"
        '
        'RepositoryItemSpinEdit2
        '
        Me.RepositoryItemSpinEdit2.AutoHeight = False
        Me.RepositoryItemSpinEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemSpinEdit2.Name = "RepositoryItemSpinEdit2"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "ตั้งค่า"
        Me.BarButtonItem5.Glyph = Global.FReport.My.Resources.Resources.version_32x32
        Me.BarButtonItem5.Id = 9
        Me.BarButtonItem5.Name = "BarButtonItem5"
        Me.BarButtonItem5.RibbonStyle = CType((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'dfStartedDate
        '
        Me.dfStartedDate.Caption = "วันที่เริ่มต้น"
        Me.dfStartedDate.Edit = Me.RepositoryItemDateEdit1
        Me.dfStartedDate.Id = 10
        Me.dfStartedDate.Name = "dfStartedDate"
        Me.dfStartedDate.Width = 100
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'dfEndDate
        '
        Me.dfEndDate.Caption = "วันที่สิ้นสุด "
        Me.dfEndDate.Edit = Me.RepositoryItemDateEdit2
        Me.dfEndDate.Id = 11
        Me.dfEndDate.Name = "dfEndDate"
        Me.dfEndDate.Width = 100
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'btnQueryServiceDate
        '
        Me.btnQueryServiceDate.Caption = "แสดงรายงาน"
        Me.btnQueryServiceDate.Glyph = CType(resources.GetObject("btnQueryServiceDate.Glyph"), System.Drawing.Image)
        Me.btnQueryServiceDate.Id = 12
        Me.btnQueryServiceDate.LargeGlyph = CType(resources.GetObject("btnQueryServiceDate.LargeGlyph"), System.Drawing.Image)
        Me.btnQueryServiceDate.Name = "btnQueryServiceDate"
        '
        'RibbonPage2
        '
        Me.RibbonPage2.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.dataAnalyse, Me.RibbonPageGroup2})
        Me.RibbonPage2.Name = "RibbonPage2"
        Me.RibbonPage2.Text = "Data Analysis"
        '
        'dataAnalyse
        '
        Me.dataAnalyse.ItemLinks.Add(Me.thresholdWaitingTime)
        Me.dataAnalyse.ItemLinks.Add(Me.thresholdWorkingTime)
        Me.dataAnalyse.ItemLinks.Add(Me.BarButtonItem5)
        Me.dataAnalyse.Name = "dataAnalyse"
        Me.dataAnalyse.Text = "วิเคราะห์ข้อมูล"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.dfStartedDate)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.dfEndDate)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.btnQueryServiceDate)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "เลือกกรองช่วงวันที่"
        '
        'RibbonPage1
        '
        Me.RibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.Export, Me.RibbonPageGroup1})
        Me.RibbonPage1.Name = "RibbonPage1"
        Me.RibbonPage1.Text = "Export"
        '
        'Export
        '
        Me.Export.ItemLinks.Add(Me.exportToExcel)
        Me.Export.ItemLinks.Add(Me.BarButtonItem2)
        Me.Export.Name = "Export"
        Me.Export.Text = "Export"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BarButtonItem3)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BarButtonItem4)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "Print"
        '
        'ChartControl2
        '
        Me.ChartControl2.DataSource = Me.PivotGridControl2
        XyDiagram1.AxisX.Title.Text = "วันที่ ชื่อคลินิค"
        XyDiagram1.AxisX.VisibleInPanesSerializable = "-1"
        XyDiagram1.AxisY.Title.Text = "รอตรวจเฉลี่ย (นาที) ตรวจเฉลี่ย (นาที)"
        XyDiagram1.AxisY.VisibleInPanesSerializable = "-1"
        Me.ChartControl2.Diagram = XyDiagram1
        Me.ChartControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ChartControl2.Legend.MaxHorizontalPercentage = 30.0R
        Me.ChartControl2.Location = New System.Drawing.Point(2, 2)
        Me.ChartControl2.Name = "ChartControl2"
        Me.ChartControl2.SeriesDataMember = "Series"
        Series1.Name = "Series 1"
        Series1.Visible = False
        Series2.Name = "Series 2"
        Series2.Visible = False
        Me.ChartControl2.SeriesSerializable = New DevExpress.XtraCharts.Series() {Series1, Series2}
        Me.ChartControl2.SeriesTemplate.ArgumentDataMember = "Arguments"
        Me.ChartControl2.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        Me.ChartControl2.SeriesTemplate.ValueDataMembersSerializable = "Values"
        Me.ChartControl2.Size = New System.Drawing.Size(634, 102)
        Me.ChartControl2.TabIndex = 3
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.PivotGridControl2)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 140)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1232, 310)
        Me.PanelControl1.TabIndex = 5
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.ChartControl1)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl2.Location = New System.Drawing.Point(0, 450)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(594, 106)
        Me.PanelControl2.TabIndex = 6
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.ChartControl2)
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl3.Location = New System.Drawing.Point(594, 450)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(638, 106)
        Me.PanelControl3.TabIndex = 7
        '
        'CLINICSERVICETIMEBindingSource
        '
        Me.CLINICSERVICETIMEBindingSource.DataMember = "CLINIC_SERVICETIME"
        Me.CLINICSERVICETIMEBindingSource.DataSource = Me.RegDataset2
        '
        'RegDashboardForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1232, 556)
        Me.Controls.Add(Me.PanelControl3)
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.RibbonControl1)
        Me.Name = "RegDashboardForm"
        Me.Text = "RegDashboardForm"
        CType(Me.PivotGridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RegDataset2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RegDataset1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(SimpleDiagram1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(PieSeriesView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSpinEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(XyDiagram1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Series2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        CType(Me.CLINICSERVICETIMEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RegDataset1 As FReport.regDataset
    Friend WithEvents RegDataset1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PivotGridControl2 As DevExpress.XtraPivotGrid.PivotGridControl
    Friend WithEvents clinicNameField As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents workingField As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents waitingField As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents serviceDateField As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents RegDataset2 As FReport.regDataset
    Friend WithEvents ChartControl1 As DevExpress.XtraCharts.ChartControl
    Friend WithEvents RibbonControl1 As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents exportToExcel As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage1 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents Export As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents ChartControl2 As DevExpress.XtraCharts.ChartControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents thresholdWaitingTime As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemSpinEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents thresholdWorkingTime As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemSpinEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents RibbonPage2 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents dataAnalyse As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents CLINICSERVICETIMEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents dfStartedDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents dfEndDate As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents btnQueryServiceDate As DevExpress.XtraBars.BarButtonItem
End Class
