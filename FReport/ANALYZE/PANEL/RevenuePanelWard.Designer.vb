﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RevenuePanelWard
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim PivotGridGroup1 As DevExpress.XtraPivotGrid.PivotGridGroup = New DevExpress.XtraPivotGrid.PivotGridGroup()
        Dim PivotGridGroup2 As DevExpress.XtraPivotGrid.PivotGridGroup = New DevExpress.XtraPivotGrid.PivotGridGroup()
        Dim PivotGridGroup3 As DevExpress.XtraPivotGrid.PivotGridGroup = New DevExpress.XtraPivotGrid.PivotGridGroup()
        Me.fieldMTRGTID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldMTRGTNAME1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldSTRGTID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldSTRGTNAME1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldsbrgtid1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldSUBRGTNAME1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.PivotGridControl1 = New DevExpress.XtraPivotGrid.PivotGridControl()
        Me.AccountSet1 = New FReport.AccountSet()
        Me.fieldDOCID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldAN1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldVN1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldPCONID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldDATESERV1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldIPD1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldOPD1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldamount1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fielddiscount1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldtotal1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.DateStart = New DevExpress.XtraEditors.DateEdit()
        Me.DateTo = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.PersonSet2 = New FReport.PersonSet()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AccountSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.DateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PersonSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'fieldMTRGTID1
        '
        Me.fieldMTRGTID1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldMTRGTID1.AreaIndex = 0
        Me.fieldMTRGTID1.Caption = "ID"
        Me.fieldMTRGTID1.FieldName = "MTRGTID"
        Me.fieldMTRGTID1.Name = "fieldMTRGTID1"
        Me.fieldMTRGTID1.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending
        Me.fieldMTRGTID1.Width = 40
        '
        'fieldMTRGTNAME1
        '
        Me.fieldMTRGTNAME1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldMTRGTNAME1.AreaIndex = 1
        Me.fieldMTRGTNAME1.Caption = "สิทธิ"
        Me.fieldMTRGTNAME1.FieldName = "MTRGTNAME"
        Me.fieldMTRGTNAME1.Name = "fieldMTRGTNAME1"
        Me.fieldMTRGTNAME1.Width = 80
        '
        'fieldSTRGTID1
        '
        Me.fieldSTRGTID1.AreaIndex = 6
        Me.fieldSTRGTID1.Caption = "ID"
        Me.fieldSTRGTID1.FieldName = "STRGTID"
        Me.fieldSTRGTID1.Name = "fieldSTRGTID1"
        Me.fieldSTRGTID1.Width = 40
        '
        'fieldSTRGTNAME1
        '
        Me.fieldSTRGTNAME1.AreaIndex = 7
        Me.fieldSTRGTNAME1.Caption = "ประเภท"
        Me.fieldSTRGTNAME1.FieldName = "STRGTNAME"
        Me.fieldSTRGTNAME1.Name = "fieldSTRGTNAME1"
        '
        'fieldsbrgtid1
        '
        Me.fieldsbrgtid1.AreaIndex = 8
        Me.fieldsbrgtid1.Caption = "ID"
        Me.fieldsbrgtid1.FieldName = "sbrgtid"
        Me.fieldsbrgtid1.Name = "fieldsbrgtid1"
        Me.fieldsbrgtid1.Width = 40
        '
        'fieldSUBRGTNAME1
        '
        Me.fieldSUBRGTNAME1.AreaIndex = 9
        Me.fieldSUBRGTNAME1.Caption = "คู่สัญญา"
        Me.fieldSUBRGTNAME1.FieldName = "SUBRGTNAME"
        Me.fieldSUBRGTNAME1.Name = "fieldSUBRGTNAME1"
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.PivotGridControl1
        Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 49)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(1121, 630)
        Me.LayoutControlItem1.Text = "LayoutControlItem1"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextToControlDistance = 0
        Me.LayoutControlItem1.TextVisible = False
        '
        'PivotGridControl1
        '
        Me.PivotGridControl1.DataMember = "profitmonthly"
        Me.PivotGridControl1.DataSource = Me.AccountSet1
        Me.PivotGridControl1.Fields.AddRange(New DevExpress.XtraPivotGrid.PivotGridField() {Me.fieldDOCID1, Me.fieldAN1, Me.fieldVN1, Me.fieldPCONID1, Me.fieldDATESERV1, Me.fieldIPD1, Me.fieldOPD1, Me.fieldamount1, Me.fielddiscount1, Me.fieldtotal1, Me.fieldMTRGTID1, Me.fieldSTRGTID1, Me.fieldsbrgtid1, Me.fieldMTRGTNAME1, Me.fieldSTRGTNAME1, Me.fieldSUBRGTNAME1})
        PivotGridGroup1.Caption = "สิทธิ์"
        PivotGridGroup1.Fields.Add(Me.fieldMTRGTID1)
        PivotGridGroup1.Fields.Add(Me.fieldMTRGTNAME1)
        PivotGridGroup1.Hierarchy = Nothing
        PivotGridGroup1.ShowNewValues = True
        PivotGridGroup2.Caption = "ประเภท"
        PivotGridGroup2.Fields.Add(Me.fieldSTRGTID1)
        PivotGridGroup2.Fields.Add(Me.fieldSTRGTNAME1)
        PivotGridGroup2.Hierarchy = Nothing
        PivotGridGroup2.ShowNewValues = True
        PivotGridGroup3.Caption = "คู่สัญญา"
        PivotGridGroup3.Fields.Add(Me.fieldsbrgtid1)
        PivotGridGroup3.Fields.Add(Me.fieldSUBRGTNAME1)
        PivotGridGroup3.Hierarchy = Nothing
        PivotGridGroup3.ShowNewValues = True
        Me.PivotGridControl1.Groups.AddRange(New DevExpress.XtraPivotGrid.PivotGridGroup() {PivotGridGroup1, PivotGridGroup2, PivotGridGroup3})
        Me.PivotGridControl1.Location = New System.Drawing.Point(12, 61)
        Me.PivotGridControl1.Name = "PivotGridControl1"
        Me.PivotGridControl1.Size = New System.Drawing.Size(1117, 626)
        Me.PivotGridControl1.TabIndex = 7
        '
        'AccountSet1
        '
        Me.AccountSet1.DataSetName = "AccountSet"
        Me.AccountSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'fieldDOCID1
        '
        Me.fieldDOCID1.AreaIndex = 0
        Me.fieldDOCID1.Caption = "DOCID"
        Me.fieldDOCID1.FieldName = "DOCID"
        Me.fieldDOCID1.Name = "fieldDOCID1"
        '
        'fieldAN1
        '
        Me.fieldAN1.AreaIndex = 1
        Me.fieldAN1.Caption = "AN"
        Me.fieldAN1.FieldName = "AN"
        Me.fieldAN1.Name = "fieldAN1"
        '
        'fieldVN1
        '
        Me.fieldVN1.AreaIndex = 2
        Me.fieldVN1.Caption = "VN"
        Me.fieldVN1.FieldName = "VN"
        Me.fieldVN1.Name = "fieldVN1"
        '
        'fieldPCONID1
        '
        Me.fieldPCONID1.AreaIndex = 3
        Me.fieldPCONID1.Caption = "PCONID"
        Me.fieldPCONID1.FieldName = "PCONID"
        Me.fieldPCONID1.Name = "fieldPCONID1"
        '
        'fieldDATESERV1
        '
        Me.fieldDATESERV1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldDATESERV1.AreaIndex = 0
        Me.fieldDATESERV1.Caption = "DATESERV"
        Me.fieldDATESERV1.FieldName = "DATESERV"
        Me.fieldDATESERV1.Name = "fieldDATESERV1"
        '
        'fieldIPD1
        '
        Me.fieldIPD1.AreaIndex = 4
        Me.fieldIPD1.Caption = "IPD"
        Me.fieldIPD1.FieldName = "IPD"
        Me.fieldIPD1.Name = "fieldIPD1"
        '
        'fieldOPD1
        '
        Me.fieldOPD1.AreaIndex = 5
        Me.fieldOPD1.Caption = "OPD"
        Me.fieldOPD1.FieldName = "OPD"
        Me.fieldOPD1.Name = "fieldOPD1"
        '
        'fieldamount1
        '
        Me.fieldamount1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldamount1.AreaIndex = 0
        Me.fieldamount1.Caption = "amount"
        Me.fieldamount1.FieldName = "amount"
        Me.fieldamount1.Name = "fieldamount1"
        '
        'fielddiscount1
        '
        Me.fielddiscount1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fielddiscount1.AreaIndex = 1
        Me.fielddiscount1.Caption = "discount"
        Me.fielddiscount1.FieldName = "discount"
        Me.fielddiscount1.Name = "fielddiscount1"
        '
        'fieldtotal1
        '
        Me.fieldtotal1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldtotal1.AreaIndex = 2
        Me.fieldtotal1.Caption = "total"
        Me.fieldtotal1.FieldName = "total"
        Me.fieldtotal1.Name = "fieldtotal1"
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.SimpleButton1
        Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(892, 23)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(229, 26)
        Me.LayoutControlItem4.Text = "LayoutControlItem4"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextToControlDistance = 0
        Me.LayoutControlItem4.TextVisible = False
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(904, 35)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(225, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "Report"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.LabelControl1)
        Me.LayoutControl1.Controls.Add(Me.PivotGridControl1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.DateStart)
        Me.LayoutControl1.Controls.Add(Me.DateTo)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(814, 396, 250, 350)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1141, 699)
        Me.LayoutControl1.TabIndex = 4
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'DateStart
        '
        Me.DateStart.EditValue = Nothing
        Me.DateStart.Location = New System.Drawing.Point(489, 35)
        Me.DateStart.Name = "DateStart"
        Me.DateStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateStart.Size = New System.Drawing.Size(190, 20)
        Me.DateStart.StyleController = Me.LayoutControl1
        Me.DateStart.TabIndex = 5
        '
        'DateTo
        '
        Me.DateTo.EditValue = Nothing
        Me.DateTo.Location = New System.Drawing.Point(732, 35)
        Me.DateTo.Name = "DateTo"
        Me.DateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateTo.Size = New System.Drawing.Size(168, 20)
        Me.DateTo.StyleController = Me.LayoutControl1
        Me.DateTo.TabIndex = 4
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.EmptySpaceItem1, Me.LayoutControlItem1, Me.LayoutControlItem5})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1141, 699)
        Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.DateTo
        Me.LayoutControlItem2.CustomizationFormText = "ถึงวันที่"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(671, 23)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(221, 26)
        Me.LayoutControlItem2.Text = "ถึงวันที่"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(46, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.DateStart
        Me.LayoutControlItem3.CustomizationFormText = "ตั้งแต่วันที่"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(428, 23)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(243, 26)
        Me.LayoutControlItem3.Text = "ตั้งแต่วันที่"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(46, 13)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 23)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(428, 26)
        Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'PersonSet2
        '
        Me.PersonSet2.DataSetName = "PersonSet"
        Me.PersonSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(184, 19)
        Me.LabelControl1.StyleController = Me.LayoutControl1
        Me.LabelControl1.TabIndex = 8
        Me.LabelControl1.Text = "รายงานรายได้แยกตามWard"
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.LabelControl1
        Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(1121, 23)
        Me.LayoutControlItem5.Text = "LayoutControlItem5"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextToControlDistance = 0
        Me.LayoutControlItem5.TextVisible = False
        '
        'RevenuePanelWard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "RevenuePanelWard"
        Me.Size = New System.Drawing.Size(1141, 699)
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AccountSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.DateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PersonSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents PivotGridControl1 As DevExpress.XtraPivotGrid.PivotGridControl
    Friend WithEvents AccountSet1 As FReport.AccountSet
    Friend WithEvents fieldDOCID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldAN1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldVN1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldPCONID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldDATESERV1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldIPD1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldOPD1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldamount1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fielddiscount1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldtotal1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldMTRGTID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldSTRGTID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldsbrgtid1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldMTRGTNAME1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldSTRGTNAME1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldSUBRGTNAME1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents DateStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateTo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents PersonSet2 As FReport.PersonSet
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem

End Class
