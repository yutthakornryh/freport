﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RevenuePanelShare
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim PivotGridGroup1 As DevExpress.XtraPivotGrid.PivotGridGroup = New DevExpress.XtraPivotGrid.PivotGridGroup()
        Dim PivotGridGroup2 As DevExpress.XtraPivotGrid.PivotGridGroup = New DevExpress.XtraPivotGrid.PivotGridGroup()
        Dim PivotGridGroup3 As DevExpress.XtraPivotGrid.PivotGridGroup = New DevExpress.XtraPivotGrid.PivotGridGroup()
        Me.fieldMTRGTID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldMTRGTNAME1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldSTRGTID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldSTRGTNAME1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldSBRGTID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldSUBRGTNAME1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.PivotGridControl1 = New DevExpress.XtraPivotGrid.PivotGridControl()
        Me.AccountSet1 = New FReport.AccountSet()
        Me.fieldVN1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldPCONID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldAMIPD1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldAMOPD1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldDISIPD1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldDISOPD1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldTOTOPD1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldTOTIPD1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldDATESERV1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.DateStart = New DevExpress.XtraEditors.DateEdit()
        Me.DateTo = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AccountSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'fieldMTRGTID1
        '
        Me.fieldMTRGTID1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldMTRGTID1.AreaIndex = 0
        Me.fieldMTRGTID1.Caption = "MTRGTID"
        Me.fieldMTRGTID1.FieldName = "MTRGTID"
        Me.fieldMTRGTID1.Name = "fieldMTRGTID1"
        Me.fieldMTRGTID1.Width = 55
        '
        'fieldMTRGTNAME1
        '
        Me.fieldMTRGTNAME1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldMTRGTNAME1.AreaIndex = 1
        Me.fieldMTRGTNAME1.Caption = "สิทธิ"
        Me.fieldMTRGTNAME1.FieldName = "MTRGTNAME"
        Me.fieldMTRGTNAME1.Name = "fieldMTRGTNAME1"
        '
        'fieldSTRGTID1
        '
        Me.fieldSTRGTID1.AreaIndex = 2
        Me.fieldSTRGTID1.Caption = "STRGTID"
        Me.fieldSTRGTID1.FieldName = "STRGTID"
        Me.fieldSTRGTID1.Name = "fieldSTRGTID1"
        '
        'fieldSTRGTNAME1
        '
        Me.fieldSTRGTNAME1.AreaIndex = 3
        Me.fieldSTRGTNAME1.Caption = "ประเภท"
        Me.fieldSTRGTNAME1.FieldName = "STRGTNAME"
        Me.fieldSTRGTNAME1.Name = "fieldSTRGTNAME1"
        '
        'fieldSBRGTID1
        '
        Me.fieldSBRGTID1.AreaIndex = 4
        Me.fieldSBRGTID1.Caption = "SBRGTID"
        Me.fieldSBRGTID1.FieldName = "SBRGTID"
        Me.fieldSBRGTID1.Name = "fieldSBRGTID1"
        '
        'fieldSUBRGTNAME1
        '
        Me.fieldSUBRGTNAME1.AreaIndex = 5
        Me.fieldSUBRGTNAME1.Caption = "คู่สัญญา"
        Me.fieldSUBRGTNAME1.FieldName = "SUBRGTNAME"
        Me.fieldSUBRGTNAME1.Name = "fieldSUBRGTNAME1"
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 23)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(381, 26)
        Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.SimpleButton1
        Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(793, 23)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(204, 26)
        Me.LayoutControlItem4.Text = "LayoutControlItem4"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextToControlDistance = 0
        Me.LayoutControlItem4.TextVisible = False
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(805, 35)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(200, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "Report"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.LabelControl1)
        Me.LayoutControl1.Controls.Add(Me.PivotGridControl1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.DateStart)
        Me.LayoutControl1.Controls.Add(Me.DateTo)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(814, 396, 250, 350)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1017, 612)
        Me.LayoutControl1.TabIndex = 4
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'PivotGridControl1
        '
        Me.PivotGridControl1.DataMember = "ProfitShare"
        Me.PivotGridControl1.DataSource = Me.AccountSet1
        Me.PivotGridControl1.Fields.AddRange(New DevExpress.XtraPivotGrid.PivotGridField() {Me.fieldVN1, Me.fieldPCONID1, Me.fieldAMIPD1, Me.fieldAMOPD1, Me.fieldDISIPD1, Me.fieldDISOPD1, Me.fieldTOTOPD1, Me.fieldTOTIPD1, Me.fieldMTRGTID1, Me.fieldSTRGTID1, Me.fieldSBRGTID1, Me.fieldMTRGTNAME1, Me.fieldSTRGTNAME1, Me.fieldSUBRGTNAME1, Me.fieldDATESERV1})
        PivotGridGroup1.Caption = "สิทธิ์"
        PivotGridGroup1.Fields.Add(Me.fieldMTRGTID1)
        PivotGridGroup1.Fields.Add(Me.fieldMTRGTNAME1)
        PivotGridGroup1.Hierarchy = Nothing
        PivotGridGroup1.ShowNewValues = True
        PivotGridGroup2.Caption = "ประเภท"
        PivotGridGroup2.Fields.Add(Me.fieldSTRGTID1)
        PivotGridGroup2.Fields.Add(Me.fieldSTRGTNAME1)
        PivotGridGroup2.Hierarchy = Nothing
        PivotGridGroup2.ShowNewValues = True
        PivotGridGroup3.Caption = "คู่สัญญา"
        PivotGridGroup3.Fields.Add(Me.fieldSBRGTID1)
        PivotGridGroup3.Fields.Add(Me.fieldSUBRGTNAME1)
        PivotGridGroup3.Hierarchy = Nothing
        PivotGridGroup3.ShowNewValues = True
        Me.PivotGridControl1.Groups.AddRange(New DevExpress.XtraPivotGrid.PivotGridGroup() {PivotGridGroup1, PivotGridGroup2, PivotGridGroup3})
        Me.PivotGridControl1.Location = New System.Drawing.Point(12, 61)
        Me.PivotGridControl1.Name = "PivotGridControl1"
        Me.PivotGridControl1.Size = New System.Drawing.Size(993, 539)
        Me.PivotGridControl1.TabIndex = 7
        '
        'AccountSet1
        '
        Me.AccountSet1.DataSetName = "AccountSet"
        Me.AccountSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'fieldVN1
        '
        Me.fieldVN1.AreaIndex = 0
        Me.fieldVN1.Caption = "VN"
        Me.fieldVN1.FieldName = "VN"
        Me.fieldVN1.Name = "fieldVN1"
        '
        'fieldPCONID1
        '
        Me.fieldPCONID1.AreaIndex = 1
        Me.fieldPCONID1.Caption = "PCONID"
        Me.fieldPCONID1.FieldName = "PCONID"
        Me.fieldPCONID1.Name = "fieldPCONID1"
        '
        'fieldAMIPD1
        '
        Me.fieldAMIPD1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldAMIPD1.AreaIndex = 3
        Me.fieldAMIPD1.Caption = "ยอดชำระIPD"
        Me.fieldAMIPD1.CellFormat.FormatString = "n"
        Me.fieldAMIPD1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldAMIPD1.FieldName = "AMIPD"
        Me.fieldAMIPD1.Name = "fieldAMIPD1"
        Me.fieldAMIPD1.Width = 89
        '
        'fieldAMOPD1
        '
        Me.fieldAMOPD1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldAMOPD1.AreaIndex = 0
        Me.fieldAMOPD1.Caption = "ยอดชำระOPD"
        Me.fieldAMOPD1.CellFormat.FormatString = "n"
        Me.fieldAMOPD1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldAMOPD1.FieldName = "AMOPD"
        Me.fieldAMOPD1.Name = "fieldAMOPD1"
        Me.fieldAMOPD1.Width = 89
        '
        'fieldDISIPD1
        '
        Me.fieldDISIPD1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldDISIPD1.AreaIndex = 4
        Me.fieldDISIPD1.Caption = "ยอดส่วนลดIPD"
        Me.fieldDISIPD1.CellFormat.FormatString = "n"
        Me.fieldDISIPD1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldDISIPD1.FieldName = "DISIPD"
        Me.fieldDISIPD1.Name = "fieldDISIPD1"
        Me.fieldDISIPD1.Width = 91
        '
        'fieldDISOPD1
        '
        Me.fieldDISOPD1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldDISOPD1.AreaIndex = 1
        Me.fieldDISOPD1.Caption = "ยอดส่วนลดOPD"
        Me.fieldDISOPD1.CellFormat.FormatString = "n"
        Me.fieldDISOPD1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldDISOPD1.FieldName = "DISOPD"
        Me.fieldDISOPD1.Name = "fieldDISOPD1"
        Me.fieldDISOPD1.Width = 89
        '
        'fieldTOTOPD1
        '
        Me.fieldTOTOPD1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldTOTOPD1.AreaIndex = 2
        Me.fieldTOTOPD1.Caption = "สุทธิOPD"
        Me.fieldTOTOPD1.CellFormat.FormatString = "n"
        Me.fieldTOTOPD1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTOTOPD1.FieldName = "TOTOPD"
        Me.fieldTOTOPD1.Name = "fieldTOTOPD1"
        '
        'fieldTOTIPD1
        '
        Me.fieldTOTIPD1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldTOTIPD1.AreaIndex = 5
        Me.fieldTOTIPD1.Caption = "สุทธิIPD"
        Me.fieldTOTIPD1.CellFormat.FormatString = "n"
        Me.fieldTOTIPD1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTOTIPD1.FieldName = "TOTIPD"
        Me.fieldTOTIPD1.Name = "fieldTOTIPD1"
        '
        'fieldDATESERV1
        '
        Me.fieldDATESERV1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldDATESERV1.AreaIndex = 0
        Me.fieldDATESERV1.Caption = "DATESERV"
        Me.fieldDATESERV1.FieldName = "DATESERV"
        Me.fieldDATESERV1.Name = "fieldDATESERV1"
        '
        'DateStart
        '
        Me.DateStart.EditValue = Nothing
        Me.DateStart.Location = New System.Drawing.Point(442, 35)
        Me.DateStart.Name = "DateStart"
        Me.DateStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateStart.Size = New System.Drawing.Size(163, 20)
        Me.DateStart.StyleController = Me.LayoutControl1
        Me.DateStart.TabIndex = 5
        '
        'DateTo
        '
        Me.DateTo.EditValue = Nothing
        Me.DateTo.Location = New System.Drawing.Point(658, 35)
        Me.DateTo.Name = "DateTo"
        Me.DateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateTo.Size = New System.Drawing.Size(143, 20)
        Me.DateTo.StyleController = Me.LayoutControl1
        Me.DateTo.TabIndex = 4
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.EmptySpaceItem1, Me.LayoutControlItem1, Me.LayoutControlItem5})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1017, 612)
        Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.DateTo
        Me.LayoutControlItem2.CustomizationFormText = "ถึงวันที่"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(597, 23)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(196, 26)
        Me.LayoutControlItem2.Text = "ถึงวันที่"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(46, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.DateStart
        Me.LayoutControlItem3.CustomizationFormText = "ตั้งแต่วันที่"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(381, 23)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(216, 26)
        Me.LayoutControlItem3.Text = "ตั้งแต่วันที่"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(46, 13)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.PivotGridControl1
        Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 49)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(997, 543)
        Me.LayoutControlItem1.Text = "LayoutControlItem1"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextToControlDistance = 0
        Me.LayoutControlItem1.TextVisible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(212, 19)
        Me.LabelControl1.StyleController = Me.LayoutControl1
        Me.LabelControl1.TabIndex = 8
        Me.LabelControl1.Text = "รายงานรายได้รวมแยก OPD IPD"
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.LabelControl1
        Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(997, 23)
        Me.LayoutControlItem5.Text = "LayoutControlItem5"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextToControlDistance = 0
        Me.LayoutControlItem5.TextVisible = False
        '
        'RevenuePanelShare
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "RevenuePanelShare"
        Me.Size = New System.Drawing.Size(1017, 612)
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AccountSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents PivotGridControl1 As DevExpress.XtraPivotGrid.PivotGridControl
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents DateStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateTo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents AccountSet1 As FReport.AccountSet
    Friend WithEvents fieldVN1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldPCONID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldAMIPD1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldAMOPD1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldDISIPD1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldDISOPD1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldTOTOPD1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldTOTIPD1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldMTRGTID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldSTRGTID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldSBRGTID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldMTRGTNAME1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldSTRGTNAME1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldSUBRGTNAME1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldDATESERV1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem

End Class
