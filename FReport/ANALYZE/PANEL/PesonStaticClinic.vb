﻿Imports DevExpress.XtraEditors.Controls

Public Class PesonStaticClinic
    Dim dtset As New PersonSet

    Public Property report As DevExpress.XtraGrid.GridControl

    Private Sub PersonStatPanel_Load(sender As Object, e As EventArgs) Handles Me.Load
        LabelControl1.Text = "รายงานคนไข้ตาม Clinic"
        Dim analuze As New AnalyzeClass(dtset)
        analuze.getMasclinic()
        LookUpEdit1.Properties.DataSource = dtset.Tables("CLINIC")
        LookUpEdit1.Properties.DisplayMember = "CLINICCODE"
        LookUpEdit1.Properties.ValueMember = "CLINIC"

        Dim coll As LookUpColumnInfoCollection = LookUpEdit1.Properties.Columns
        coll.Add(New LookUpColumnInfo("CLINIC", 0))
        coll.Add(New LookUpColumnInfo("CLINICCODE", 0))
        coll.Add(New LookUpColumnInfo("CLINICNAME", 0))
        LookUpEdit1.Properties.BestFitMode = BestFitMode.BestFitResizePopup
        LookUpEdit1.Properties.SearchMode = SearchMode.AutoComplete
        LookUpEdit1.Properties.AutoSearchColumnIndex = 1
        DateStart.EditValue = Now
        DateTo.EditValue = Now
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Dim analyze As New AnalyzeClass(dtset)
        analyze.getPersonStaticClinic(DateStart.EditValue, DateTo.EditValue)
        GridControl1.DataSource = dtset
        report = GridControl1

    End Sub
End Class
