﻿Public Class PersonNewStatPanel
    Dim dtset As New PersonSet
    Public Property report As DevExpress.XtraPivotGrid.PivotGridControl

    Private Sub PersonStatPanel_Load(sender As Object, e As EventArgs) Handles Me.Load
        DateStart.EditValue = Now
        DateTo.EditValue = Now
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Dim analyze As New AnalyzeClass(dtset)

        analyze.getNewPatient(DateStart.EditValue, DateTo.EditValue)
        PivotGridControl1.DataSource = dtset
        report = PivotGridControl1

    End Sub
End Class
