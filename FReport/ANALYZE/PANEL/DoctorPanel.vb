﻿Public Class DoctorPanel
    Dim dtset As New PersonSet
    Dim personclass As New AnalyzeClass(dtset)
    Public Property report As DevExpress.XtraGrid.GridControl

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        '   personclass.getPersonDoctor()
        Dim analyze As New AnalyzeClass(dtset)

        analyze.getPersonDoctor(DateStart.EditValue, DateTo.EditValue)
        GridControl1.DataSource = dtset
        report = GridControl1
    End Sub

    Private Sub DoctorPanel_Load(sender As Object, e As EventArgs) Handles Me.Load
        DateStart.EditValue = Now
        DateTo.EditValue = Now
    End Sub
End Class
