﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AnalyzeForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AnalyzeForm))
        Me.NavBarItem2 = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItem5 = New DevExpress.XtraNavBar.NavBarItem()
        Me.check = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavBarItem6 = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarGroup2 = New DevExpress.XtraNavBar.NavBarGroup()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl()
        Me.BJGroup = New DevExpress.XtraNavBar.NavBarGroup()
        Me.menuFrnDrugPay = New DevExpress.XtraNavBar.NavBarItem()
        Me.menuMonthlyDrgPay = New DevExpress.XtraNavBar.NavBarItem()
        Me.SearchControl1 = New DevExpress.XtraEditors.SearchControl()
        Me.ReportPanel = New DevExpress.XtraEditors.PanelControl()
        Me.NavBarItem4 = New DevExpress.XtraNavBar.NavBarItem()
        Me.RibbonControl1 = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPage1 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReportPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NavBarItem2
        '
        Me.NavBarItem2.Caption = "บจ-๙ รายงานประจำเดือน"
        Me.NavBarItem2.Name = "NavBarItem2"
        Me.NavBarItem2.SmallImage = CType(resources.GetObject("NavBarItem2.SmallImage"), System.Drawing.Image)
        '
        'NavBarItem5
        '
        Me.NavBarItem5.Caption = "ยส-๗ รายงานประจำปีประเภท 2"
        Me.NavBarItem5.Name = "NavBarItem5"
        Me.NavBarItem5.SmallImage = CType(resources.GetObject("NavBarItem5.SmallImage"), System.Drawing.Image)
        '
        'check
        '
        Me.check.Caption = "เอกสารผู้ป่วย"
        Me.check.Expanded = True
        Me.check.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItem6)})
        Me.check.LargeImage = Global.FReport.My.Resources.Resources.new_32x32
        Me.check.Name = "check"
        '
        'NavBarItem6
        '
        Me.NavBarItem6.Caption = "ใบรับรองแพทย์"
        Me.NavBarItem6.Name = "NavBarItem6"
        Me.NavBarItem6.SmallImage = Global.FReport.My.Resources.Resources.textbox_32x32
        '
        'NavBarGroup2
        '
        Me.NavBarGroup2.Caption = "แบบยาเสพติดให้โทษ (ย.ส.)"
        Me.NavBarGroup2.Expanded = True
        Me.NavBarGroup2.Name = "NavBarGroup2"
        Me.NavBarGroup2.SmallImage = CType(resources.GetObject("NavBarGroup2.SmallImage"), System.Drawing.Image)
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.NavBarControl1)
        Me.PanelControl1.Controls.Add(Me.SearchControl1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PanelControl1.Location = New System.Drawing.Point(0, 140)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(249, 370)
        Me.PanelControl1.TabIndex = 10
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.BJGroup
        Me.NavBarControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.BJGroup})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.NavBarItem6, Me.menuFrnDrugPay, Me.menuMonthlyDrgPay})
        Me.NavBarControl1.Location = New System.Drawing.Point(2, 22)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.OptionsNavPane.AllowOptionsMenuItem = True
        Me.NavBarControl1.OptionsNavPane.CollapsedNavPaneContentControl = Me.PanelControl1
        Me.NavBarControl1.OptionsNavPane.ExpandedWidth = 245
        Me.NavBarControl1.Size = New System.Drawing.Size(245, 346)
        Me.NavBarControl1.TabIndex = 1
        Me.NavBarControl1.Text = "NavBarControl1"
        '
        'BJGroup
        '
        Me.BJGroup.Caption = "รายงานการจ่ายยาและเวชภัณฑ์"
        Me.BJGroup.Expanded = True
        Me.BJGroup.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.menuFrnDrugPay), New DevExpress.XtraNavBar.NavBarItemLink(Me.menuMonthlyDrgPay)})
        Me.BJGroup.Name = "BJGroup"
        Me.BJGroup.SmallImage = CType(resources.GetObject("BJGroup.SmallImage"), System.Drawing.Image)
        '
        'menuFrnDrugPay
        '
        Me.menuFrnDrugPay.Caption = "รายงานประจำวัน"
        Me.menuFrnDrugPay.Name = "menuFrnDrugPay"
        '
        'menuMonthlyDrgPay
        '
        Me.menuMonthlyDrgPay.Caption = "รายงานประจำเดือน"
        Me.menuMonthlyDrgPay.Name = "menuMonthlyDrgPay"
        '
        'SearchControl1
        '
        Me.SearchControl1.Client = Me.NavBarControl1
        Me.SearchControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SearchControl1.Location = New System.Drawing.Point(2, 2)
        Me.SearchControl1.Name = "SearchControl1"
        Me.SearchControl1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Repository.ClearButton(), New DevExpress.XtraEditors.Repository.SearchButton(), New DevExpress.XtraEditors.Repository.MRUButton()})
        Me.SearchControl1.Properties.Client = Me.NavBarControl1
        Me.SearchControl1.Properties.ShowMRUButton = True
        Me.SearchControl1.Size = New System.Drawing.Size(245, 20)
        Me.SearchControl1.TabIndex = 3
        '
        'ReportPanel
        '
        Me.ReportPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportPanel.Location = New System.Drawing.Point(0, 140)
        Me.ReportPanel.Name = "ReportPanel"
        Me.ReportPanel.Size = New System.Drawing.Size(1007, 370)
        Me.ReportPanel.TabIndex = 9
        '
        'NavBarItem4
        '
        Me.NavBarItem4.Caption = "ยส-๖ รายงานประจำเดือนประเภท 2"
        Me.NavBarItem4.Name = "NavBarItem4"
        Me.NavBarItem4.SmallImage = CType(resources.GetObject("NavBarItem4.SmallImage"), System.Drawing.Image)
        '
        'RibbonControl1
        '
        Me.RibbonControl1.ExpandCollapseItem.Id = 0
        Me.RibbonControl1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl1.ExpandCollapseItem, Me.BarButtonItem1, Me.BarButtonItem4, Me.BarButtonItem5, Me.BarButtonItem6})
        Me.RibbonControl1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl1.MaxItemId = 7
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.RibbonPage1})
        Me.RibbonControl1.Size = New System.Drawing.Size(1007, 140)
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Export to Excel"
        Me.BarButtonItem1.Id = 1
        Me.BarButtonItem1.LargeGlyph = Global.FReport.My.Resources.Resources.sendxls_32x32
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Quick Print"
        Me.BarButtonItem4.Id = 4
        Me.BarButtonItem4.LargeGlyph = Global.FReport.My.Resources.Resources.print_32x32
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "Print Dialog"
        Me.BarButtonItem5.Id = 5
        Me.BarButtonItem5.LargeGlyph = Global.FReport.My.Resources.Resources.printdialog_32x32
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "Export to PDF"
        Me.BarButtonItem6.Id = 6
        Me.BarButtonItem6.LargeGlyph = Global.FReport.My.Resources.Resources.sendpdf_32x32
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'RibbonPage1
        '
        Me.RibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1, Me.RibbonPageGroup2})
        Me.RibbonPage1.Name = "RibbonPage1"
        Me.RibbonPage1.Text = "Export"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BarButtonItem6)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "Export"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BarButtonItem4)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.BarButtonItem5)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "Print"
        '
        'AnalyzeForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1007, 510)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.ReportPanel)
        Me.Controls.Add(Me.RibbonControl1)
        Me.Name = "AnalyzeForm"
        Me.Text = "AnalyzeForm"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SearchControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReportPanel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NavBarItem2 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItem5 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents check As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarItem6 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarGroup2 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents BJGroup As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents menuFrnDrugPay As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents menuMonthlyDrgPay As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents SearchControl1 As DevExpress.XtraEditors.SearchControl
    Friend WithEvents ReportPanel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents NavBarItem4 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents RibbonControl1 As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPage1 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
End Class
