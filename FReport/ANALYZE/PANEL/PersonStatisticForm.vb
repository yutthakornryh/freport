﻿Imports DevExpress.XtraReports.UI

Public Class PersonStatisticForm
    Dim personStat As PersonNewStatPanel
    Dim personVisitStat As PersonVisitPanel
    Dim profitRevenue As RevenuePanel
    Dim profitRevenueShare As RevenuePanelShare
    Dim profitRevenueWard As RevenuePanelWard
    Dim PersonStaticClinic As PesonStaticClinic
    Dim DoctorPanel As DoctorPanel


    Dim report As DevExpress.XtraPivotGrid.PivotGridControl
    Dim reportname As String

    Private Sub PersonStatisticForm_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
    End Sub

    Private Sub menuMonthlyDrgPay_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles menuMonthlyDrgPay.LinkClicked
        personStat = New PersonNewStatPanel()
        reportname = "PersonStatics"

        AddHandler personStat.SimpleButton1.Click, AddressOf PanelGrid
        personStat.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(personStat)
    End Sub
    Private Sub PanelGrid(sender As Object, e As EventArgs)
        If reportname = "PersonStatics" Then
            Me.report = personStat.report
        ElseIf reportname = "PersonVisitStatics" Then

            Me.report = personVisitStat.report
        ElseIf reportname = "profitRevenue" Then
            Me.report = profitRevenue.report

        ElseIf reportname = "profitRevenueShare" Then
            Me.report = profitRevenueShare.report

        ElseIf reportname = "profitRevenueShareWard" Then

            Me.report = profitRevenueWard.report


        End If
        'RemoveHandler bj8Panel.btnApplyFilter.Click, AddressOf bj8PanelShowing
    End Sub
    Private Sub BarButtonItem6_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        If report IsNot Nothing Then
            FolderBrowserDialog1.Description = "Pick Folder to store PDF files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then

                ExportReport(report, FolderBrowserDialog1.SelectedPath & "\" & reportname & Now.ToString("yyyyMMdd") & ".pdf", "pdf")

            End If
        End If

    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        If report IsNot Nothing Then
            FolderBrowserDialog1.Description = "Pick Folder to store PDF files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then

                ExportReport(report, FolderBrowserDialog1.SelectedPath & "\" & reportname & Now.ToString("yyyyMMdd") & ".xls", "xls")

            End If
        End If
    End Sub

    Private Sub NavBarItem1_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem1.LinkClicked
        personVisitStat = New PersonVisitPanel()
        reportname = "PersonVisitStatics"

        AddHandler personVisitStat.SimpleButton1.Click, AddressOf PanelGrid
        personVisitStat.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(personVisitStat)
    End Sub

    Private Sub NavBarItem3_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem3.LinkClicked
        profitRevenue = New RevenuePanel()
        reportname = "profitRevenue"

        AddHandler profitRevenue.SimpleButton1.Click, AddressOf PanelGrid
        profitRevenue.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(profitRevenue)
    End Sub



    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        If report IsNot Nothing Then
            If reportname = "PersonStatics" Then
                report.ShowRibbonPrintPreview()
            End If
            If reportname = "PersonVisitStatics" Then
                report.ShowRibbonPrintPreview()

            End If
            If reportname = "profitRevenue" Then
                report.ShowRibbonPrintPreview()

            End If
            If reportname = "profitRevenueShare" Then
                report.ShowRibbonPrintPreview()

            End If

            If reportname = "profitRevenueShareWard" Then
                report.ShowRibbonPrintPreview()

            End If



            'Dim printTool As New ReportPrintTool(report)
            'printTool.Print()
            'printTool.PrintingSystem.ShowMarginsWarning = False
        End If
    End Sub

    Private Sub NavBarItem7_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem7.LinkClicked
        profitRevenueShare = New RevenuePanelShare()
        reportname = "RevenuePanelShare"

        AddHandler profitRevenueShare.SimpleButton1.Click, AddressOf PanelGrid
        profitRevenueShare.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(profitRevenueShare)
    End Sub


    Private Sub NavBarItem8_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem8.LinkClicked
        profitRevenueWard = New RevenuePanelWard
        reportname = "RevenuePanelWard"

        AddHandler profitRevenueWard.SimpleButton1.Click, AddressOf PanelGrid
        profitRevenueWard.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(profitRevenueWard)
    End Sub

    Private Sub NavBarControl1_Click(sender As Object, e As EventArgs) Handles NavBarControl1.Click

    End Sub

    Private Sub menuFrnDrugPay_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles menuFrnDrugPay.LinkClicked
        PersonStaticClinic = New PesonStaticClinic
        reportname = "PersonStaticForm"

        AddHandler PersonStaticClinic.SimpleButton1.Click, AddressOf PanelGrid
        PersonStaticClinic.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(PersonStaticClinic)
    End Sub

    Private Sub NavBarItem9_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem9.LinkClicked
        DoctorPanel = New DoctorPanel
        reportname = "DoctorPanel"

        ' AddHandler DoctorPanel.SimpleButton1.Click, AddressOf PanelGrid
        DoctorPanel.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(DoctorPanel)
    End Sub
End Class