﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RevenuePanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim PivotGridGroup1 As DevExpress.XtraPivotGrid.PivotGridGroup = New DevExpress.XtraPivotGrid.PivotGridGroup()
        Dim PivotGridGroup2 As DevExpress.XtraPivotGrid.PivotGridGroup = New DevExpress.XtraPivotGrid.PivotGridGroup()
        Dim PivotGridGroup3 As DevExpress.XtraPivotGrid.PivotGridGroup = New DevExpress.XtraPivotGrid.PivotGridGroup()
        Me.fieldMTRGTID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldMTRGTNAME1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldSTRGTID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldSTRGTNAME1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldsbrgtid1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldSUBRGTNAME1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.PersonSet2 = New FReport.PersonSet()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DateTo = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.PivotGridControl1 = New DevExpress.XtraPivotGrid.PivotGridControl()
        Me.AccountSet1 = New FReport.AccountSet()
        Me.fieldDOCID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldAN1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldVN1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldPCONID1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldDATESERV1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldIPD1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldOPD1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldamount1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fielddiscount1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldtotal1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateStart = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.PersonSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AccountSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'fieldMTRGTID1
        '
        Me.fieldMTRGTID1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldMTRGTID1.AreaIndex = 0
        Me.fieldMTRGTID1.Caption = "ID"
        Me.fieldMTRGTID1.FieldName = "MTRGTID"
        Me.fieldMTRGTID1.Name = "fieldMTRGTID1"
        Me.fieldMTRGTID1.SortOrder = DevExpress.XtraPivotGrid.PivotSortOrder.Descending
        Me.fieldMTRGTID1.Width = 40
        '
        'fieldMTRGTNAME1
        '
        Me.fieldMTRGTNAME1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldMTRGTNAME1.AreaIndex = 1
        Me.fieldMTRGTNAME1.Caption = "สิทธิ"
        Me.fieldMTRGTNAME1.FieldName = "MTRGTNAME"
        Me.fieldMTRGTNAME1.Name = "fieldMTRGTNAME1"
        Me.fieldMTRGTNAME1.Width = 80
        '
        'fieldSTRGTID1
        '
        Me.fieldSTRGTID1.AreaIndex = 6
        Me.fieldSTRGTID1.Caption = "ID"
        Me.fieldSTRGTID1.FieldName = "STRGTID"
        Me.fieldSTRGTID1.Name = "fieldSTRGTID1"
        Me.fieldSTRGTID1.Width = 40
        '
        'fieldSTRGTNAME1
        '
        Me.fieldSTRGTNAME1.AreaIndex = 7
        Me.fieldSTRGTNAME1.Caption = "ประเภท"
        Me.fieldSTRGTNAME1.FieldName = "STRGTNAME"
        Me.fieldSTRGTNAME1.Name = "fieldSTRGTNAME1"
        '
        'fieldsbrgtid1
        '
        Me.fieldsbrgtid1.AreaIndex = 8
        Me.fieldsbrgtid1.Caption = "ID"
        Me.fieldsbrgtid1.FieldName = "sbrgtid"
        Me.fieldsbrgtid1.Name = "fieldsbrgtid1"
        Me.fieldsbrgtid1.Width = 40
        '
        'fieldSUBRGTNAME1
        '
        Me.fieldSUBRGTNAME1.AreaIndex = 9
        Me.fieldSUBRGTNAME1.Caption = "คู่สัญญา"
        Me.fieldSUBRGTNAME1.FieldName = "SUBRGTNAME"
        Me.fieldSUBRGTNAME1.Name = "fieldSUBRGTNAME1"
        '
        'PersonSet2
        '
        Me.PersonSet2.DataSetName = "PersonSet"
        Me.PersonSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.DateTo
        Me.LayoutControlItem2.CustomizationFormText = "ถึงวันที่"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(688, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(226, 26)
        Me.LayoutControlItem2.Text = "ถึงวันที่"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(46, 13)
        '
        'DateTo
        '
        Me.DateTo.EditValue = Nothing
        Me.DateTo.Location = New System.Drawing.Point(749, 12)
        Me.DateTo.Name = "DateTo"
        Me.DateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateTo.Size = New System.Drawing.Size(173, 20)
        Me.DateTo.StyleController = Me.LayoutControl1
        Me.DateTo.TabIndex = 4
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.PivotGridControl1)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.DateStart)
        Me.LayoutControl1.Controls.Add(Me.DateTo)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(814, 396, 250, 350)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1169, 738)
        Me.LayoutControl1.TabIndex = 3
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'PivotGridControl1
        '
        Me.PivotGridControl1.DataMember = "profitmonthly"
        Me.PivotGridControl1.DataSource = Me.AccountSet1
        Me.PivotGridControl1.Fields.AddRange(New DevExpress.XtraPivotGrid.PivotGridField() {Me.fieldDOCID1, Me.fieldAN1, Me.fieldVN1, Me.fieldPCONID1, Me.fieldDATESERV1, Me.fieldIPD1, Me.fieldOPD1, Me.fieldamount1, Me.fielddiscount1, Me.fieldtotal1, Me.fieldMTRGTID1, Me.fieldSTRGTID1, Me.fieldsbrgtid1, Me.fieldMTRGTNAME1, Me.fieldSTRGTNAME1, Me.fieldSUBRGTNAME1})
        PivotGridGroup1.Caption = "สิทธิ์"
        PivotGridGroup1.Fields.Add(Me.fieldMTRGTID1)
        PivotGridGroup1.Fields.Add(Me.fieldMTRGTNAME1)
        PivotGridGroup1.Hierarchy = Nothing
        PivotGridGroup1.ShowNewValues = True
        PivotGridGroup2.Caption = "ประเภท"
        PivotGridGroup2.Fields.Add(Me.fieldSTRGTID1)
        PivotGridGroup2.Fields.Add(Me.fieldSTRGTNAME1)
        PivotGridGroup2.Hierarchy = Nothing
        PivotGridGroup2.ShowNewValues = True
        PivotGridGroup3.Caption = "คู่สัญญา"
        PivotGridGroup3.Fields.Add(Me.fieldsbrgtid1)
        PivotGridGroup3.Fields.Add(Me.fieldSUBRGTNAME1)
        PivotGridGroup3.Hierarchy = Nothing
        PivotGridGroup3.ShowNewValues = True
        Me.PivotGridControl1.Groups.AddRange(New DevExpress.XtraPivotGrid.PivotGridGroup() {PivotGridGroup1, PivotGridGroup2, PivotGridGroup3})
        Me.PivotGridControl1.Location = New System.Drawing.Point(12, 38)
        Me.PivotGridControl1.Name = "PivotGridControl1"
        Me.PivotGridControl1.Size = New System.Drawing.Size(1145, 688)
        Me.PivotGridControl1.TabIndex = 7
        '
        'AccountSet1
        '
        Me.AccountSet1.DataSetName = "AccountSet"
        Me.AccountSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'fieldDOCID1
        '
        Me.fieldDOCID1.AreaIndex = 0
        Me.fieldDOCID1.Caption = "DOCID"
        Me.fieldDOCID1.FieldName = "DOCID"
        Me.fieldDOCID1.Name = "fieldDOCID1"
        '
        'fieldAN1
        '
        Me.fieldAN1.AreaIndex = 1
        Me.fieldAN1.Caption = "AN"
        Me.fieldAN1.FieldName = "AN"
        Me.fieldAN1.Name = "fieldAN1"
        '
        'fieldVN1
        '
        Me.fieldVN1.AreaIndex = 2
        Me.fieldVN1.Caption = "VN"
        Me.fieldVN1.FieldName = "VN"
        Me.fieldVN1.Name = "fieldVN1"
        '
        'fieldPCONID1
        '
        Me.fieldPCONID1.AreaIndex = 3
        Me.fieldPCONID1.Caption = "PCONID"
        Me.fieldPCONID1.FieldName = "PCONID"
        Me.fieldPCONID1.Name = "fieldPCONID1"
        '
        'fieldDATESERV1
        '
        Me.fieldDATESERV1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldDATESERV1.AreaIndex = 0
        Me.fieldDATESERV1.Caption = "DATESERV"
        Me.fieldDATESERV1.FieldName = "DATESERV"
        Me.fieldDATESERV1.Name = "fieldDATESERV1"
        '
        'fieldIPD1
        '
        Me.fieldIPD1.AreaIndex = 4
        Me.fieldIPD1.Caption = "จำนวนคนไข้ใน"
        Me.fieldIPD1.CellFormat.FormatString = "N"
        Me.fieldIPD1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldIPD1.FieldName = "IPD"
        Me.fieldIPD1.Name = "fieldIPD1"
        '
        'fieldOPD1
        '
        Me.fieldOPD1.AreaIndex = 5
        Me.fieldOPD1.Caption = "จำนวนคนไข้นอก"
        Me.fieldOPD1.CellFormat.FormatString = "N"
        Me.fieldOPD1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldOPD1.FieldName = "OPD"
        Me.fieldOPD1.Name = "fieldOPD1"
        '
        'fieldamount1
        '
        Me.fieldamount1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldamount1.AreaIndex = 0
        Me.fieldamount1.Caption = "ยอดชำระ"
        Me.fieldamount1.CellFormat.FormatString = "n"
        Me.fieldamount1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldamount1.FieldName = "amount"
        Me.fieldamount1.Name = "fieldamount1"
        '
        'fielddiscount1
        '
        Me.fielddiscount1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fielddiscount1.AreaIndex = 1
        Me.fielddiscount1.Caption = "ส่วนลด"
        Me.fielddiscount1.CellFormat.FormatString = "n"
        Me.fielddiscount1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fielddiscount1.FieldName = "discount"
        Me.fielddiscount1.Name = "fielddiscount1"
        Me.fielddiscount1.Width = 80
        '
        'fieldtotal1
        '
        Me.fieldtotal1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldtotal1.AreaIndex = 2
        Me.fieldtotal1.Caption = "ทั้งหมด"
        Me.fieldtotal1.CellFormat.FormatString = "n"
        Me.fieldtotal1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldtotal1.FieldName = "total"
        Me.fieldtotal1.Name = "fieldtotal1"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(926, 12)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(231, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "Report"
        '
        'DateStart
        '
        Me.DateStart.EditValue = Nothing
        Me.DateStart.Location = New System.Drawing.Point(500, 12)
        Me.DateStart.Name = "DateStart"
        Me.DateStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateStart.Size = New System.Drawing.Size(196, 20)
        Me.DateStart.StyleController = Me.LayoutControl1
        Me.DateStart.TabIndex = 5
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.EmptySpaceItem1, Me.LayoutControlItem1})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1169, 738)
        Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.DateStart
        Me.LayoutControlItem3.CustomizationFormText = "ตั้งแต่วันที่"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(439, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(249, 26)
        Me.LayoutControlItem3.Text = "ตั้งแต่วันที่"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(46, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.SimpleButton1
        Me.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(914, 0)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(235, 26)
        Me.LayoutControlItem4.Text = "LayoutControlItem4"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextToControlDistance = 0
        Me.LayoutControlItem4.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(439, 26)
        Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.PivotGridControl1
        Me.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(1149, 692)
        Me.LayoutControlItem1.Text = "LayoutControlItem1"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextToControlDistance = 0
        Me.LayoutControlItem1.TextVisible = False
        '
        'RevenuePanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "RevenuePanel"
        Me.Size = New System.Drawing.Size(1169, 738)
        CType(Me.PersonSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AccountSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PersonSet2 As FReport.PersonSet
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DateTo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents PivotGridControl1 As DevExpress.XtraPivotGrid.PivotGridControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents AccountSet1 As FReport.AccountSet
    Friend WithEvents fieldDOCID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldAN1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldVN1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldPCONID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldDATESERV1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldIPD1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldOPD1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldamount1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fielddiscount1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldtotal1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldMTRGTID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldSTRGTID1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldsbrgtid1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldMTRGTNAME1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldSTRGTNAME1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldSUBRGTNAME1 As DevExpress.XtraPivotGrid.PivotGridField

End Class
