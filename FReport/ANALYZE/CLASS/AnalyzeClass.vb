﻿Public Class AnalyzeClass
    Dim dtset As PersonSet
    Dim connect As ConnecDBRYH
    Dim dtset1 As AccountSet
    Public Sub New(ByRef dt As PersonSet)
        connect = ConnecDBRYH.NewConnection
        dtset = dt
    End Sub
    Public Sub New(ByRef dt As AccountSet)
        connect = ConnecDBRYH.NewConnection
        dtset1 = dt
    End Sub
    Public Sub getNewPatient(Start As Date, ToDate As Date)
        Dim sql As String
        sql = "SELECT date_format(fdate,'%M-%y') as month ,CHANGWAT,masampur.ampur,mastambon.tambon,1  AS Sumpatient FROM (  SELECT hn,fdate FROM person WHERE fdate >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND fdate  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "'  ) AS person JOIN (SELECT hn,ampid,CODECHANGWAT,tmbid FROM address WHERE addresstype = 1) as address ON  person.hn = address.hn LEFT JOIN  maschangwat ON maschangwat.codechangwat = address.codechangwat LEFT JOIN masampur  ON masampur.ampid = address.ampid LEFT JOIN mastambon ON mastambon.tmbid = address.tmbid  order by fdate desc;"
        dtset.Tables("PatientNew").Clear()
        connect.GetTable(sql, dtset.Tables("PatientNew"))
    End Sub
    Public Sub getVisitPatient(Start As Date, ToDate As Date)
        Dim sql As String
        sql = "SELECT an,vn,pconid,date_format(dateserv,'%M-%y') as dateserv,conname,ipd,opd FROM ( SELECT 0 AS AN,VN,contact.PCONID,DATESERV,conname, 0 AS IPD,1 AS OPD FROM  (SELECT frnservice.vn,pconid,date_serv as dateserv FROM (SELECT vn,date_serv FROM frnservice WHERE  date_serv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND date_serv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "'  ) AS frnservice  JOIN frncon ON frncon.vn = frnservice.vn  ) AS frnservice JOIN (SELECT pconid,conname FROM  contact  ) AS contact ON frnservice.pconid = contact.pconid ) AS T  UNION ALL SELECT an,vn,pconid,date_format(dateserv,'%M-%y') as dateserv,conname,ipd,opd   FROM ( SELECT AN, frnadmission.VN,contact.PCONID,DATESERV,conname, 1 AS IPD,0 AS OPD FROM  (SELECT frnadmission.vn,frnadmission.an,pconid,datetime_admit as dateserv FROM (SELECT vn,an,datetime_admit FROM  frnadmission WHERE  datetime_admit >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND datetime_admit  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "'  ) AS frnadmission JOIN frncon ON frncon.an = frnadmission.an ) AS frnadmission JOIN (SELECT pconid,conname FROM  contact  ) AS contact ON frnadmission.pconid = contact.pconid ) AS b"
        dtset.Tables("VisitPatient").Clear()

        connect.GetTable(sql, dtset.Tables("VisitPatient"))

    End Sub
    Public Sub getRevenue(Start As Date, ToDate As Date)
        Dim sql As String
        sql = " SELECT  docid ,an,vn,pconid, DATE_FORMAT(dateserv,'%M-%y') AS dateserv,ipd,  opd,amount,discount,total,totalamount.mtrgtid,totalamount.strgtid,totalamount.sbrgtid,mtrgtname,strgtname,subrgtname FROM ( SELECT * FROM  ( SELECT invdocid AS docid ,an,vn,invoicehd.pconid,dateserv, 1 AS ipd,0 AS opd,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM  (SELECT invdocid,an,vn,pconid,dateserv,amount,discount,total,mtrgtid,strgtid,sbrgtid FROM invoicehd  WHERE an is not null AND  dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateserv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0  ) AS invoicehd "
        sql += " UNION ALL  SELECT billdocid AS docid ,an,vn,receipthd.pconid,dateserv, 1 AS ipd,0 AS opd,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM  (SELECT billdocid,an,vn,pconid,dateserv,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM receipthd WHERE AN is not null AND  dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateserv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0   ) AS receipthd "
        sql += "  )  AS  IPD "
        sql += " UNION ALL "
        sql += " SELECT * FROM ( "
        sql += " SELECT invdocid AS DOCID ,an,vn,invoicehd.pconid,dateserv, 0 AS IPD,1 AS OPD,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM   (SELECT INVDOCID,an,vn,pconid,dateserv,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM invoicehd  WHERE vn is not null AND  dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateserv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "'  AND f_cancel = 0  ) AS  invoicehd "
        sql += " UNION ALL  SELECT billdocid AS DOCID ,an,vn,receipthd.pconid,dateserv, 0 AS IPD,1 AS OPD,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM  (SELECT billdocid,an,vn,pconid,dateserv,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM receipthd WHERE vn is not null AND  dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateserv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0  ) AS receipthd  ) AS OPD  ) AS totalamount "
        sql += " LEFT JOIN masmtypergt ON masmtypergt.mtrgtid = totalamount.mtrgtid LEFT JOIN masstypergt ON masstypergt.strgtid =totalamount.strgtid LEFT JOIN massubrgt ON massubrgt.sbrgtid = totalamount.sbrgtid "
        dtset1.Tables("profitmonthly").Clear()

        connect.GetTable(sql, dtset1.Tables("profitmonthly"))
    End Sub
    Public Sub getRevenueWard(Start As Date, ToDate As Date)
        Dim sql As String
        sql = " SELECT   docid ,an,vn,pconid, DATE_FORMAT(dateserv,'%M-%y') AS dateserv,ipd,  opd,amount,discount,total,IPD.mtrgtid,IPD.strgtid,IPD.sbrgtid,mtrgtname,strgtname,subrgtname  FROM  ( SELECT invdocid AS docid ,an,vn,invoicehd.pconid,dateserv, 1 AS ipd,0 AS opd,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM  (SELECT invdocid,an,vn,pconid,dateserv,amount,discount,total,mtrgtid,strgtid,sbrgtid FROM invoicehd  WHERE an is not null AND  dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateserv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0    ) AS invoicehd "
        sql += " UNION ALL  SELECT billdocid AS docid ,an,vn,receipthd.pconid,dateserv, 1 AS ipd,0 AS opd,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM  (SELECT billdocid,an,vn,pconid,dateserv,amount,discount,total,MTRGTID,STRGTID,sbrgtid  FROM receipthd WHERE AN is not null AND  dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateserv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "'   AND f_cancel = 0  ) AS receipthd "
        sql += "  )  AS  IPD "
        sql += " LEFT JOIN masmtypergt ON masmtypergt.mtrgtid = IPD.mtrgtid LEFT JOIN masstypergt ON masstypergt.strgtid =IPD.strgtid LEFT JOIN massubrgt ON massubrgt.sbrgtid = IPD.sbrgtid "
        dtset1.Tables("profitmonthly").Clear()

        connect.GetTable(sql, dtset1.Tables("profitmonthly"))
    End Sub
    Public Sub getRevenueIPDOPD(Start As Date, ToDate As Date)
        Dim sql As String
        sql = "  SELECT  docid ,an,vn,pconid, DATE_FORMAT(dateserv,'%M-%y') AS dateserv,amipd,disipd,totipd,amopd,disopd ,totopd,totalamount.mtrgtid,totalamount.strgtid,totalamount.sbrgtid,mtrgtname,strgtname,subrgtname FROM "
        sql += " ( SELECT * FROM  "
        sql += " ( SELECT invdocid AS docid ,an,vn,invoicehd.pconid,dateserv, amount AS  amipd,discount AS disipd,total AS totipd ,0 AS  amopd,0 AS disopd,0 AS totopd, mtrgtid,strgtid,sbrgtid "
        sql += "  FROM "
        sql += " (SELECT invdocid,an,vn,pconid,dateserv,amount  ,discount  ,total  ,mtrgtid,strgtid,sbrgtid FROM invoicehd  WHERE an is not null AND  dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateserv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "'  AND f_cancel = 0   ) AS invoicehd "
        sql += "      UNION ALL"
        sql += " SELECT billdocid AS docid ,an,vn,receipthd.pconid,dateserv,amount AS amipd ,discount AS disipd ,total AS totipd, 0 AS  amopd,0 AS disopd, 0 AS totopd,mtrgtid,strgtid,sbrgtid "
        sql += " FROM  (SELECT billdocid,an,vn,pconid,dateserv,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM receipthd WHERE an is not null AND  dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateserv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0   ) AS receipthd  ) AS IPD"
        sql += " UNION ALL  SELECT * FROM (  SELECT invdocid AS docid ,an,vn,invoicehd.pconid,dateserv, 0 AS  amipd,0 AS disipd,0 AS totipd     ,amount AS amopd ,discount AS disopd ,total AS totopd  ,mtrgtid,strgtid,sbrgtid    FROM   (SELECT invdocid,an,vn,pconid,dateserv,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM invoicehd  WHERE vn is not null AND dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateserv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0    ) AS  invoicehd "
        sql += " UNION ALL  SELECT billdocid AS docid ,an,vn,receipthd.pconid,dateserv, 0 AS  amipd,0 AS disipd,0 AS totipd ,amount AS amopd ,discount AS disopd ,total AS totopd ,mtrgtid,strgtid,sbrgtid  FROM  (SELECT billdocid,an,vn,pconid,dateserv,amount,discount,total,mtrgtid,strgtid,sbrgtid  FROM receipthd WHERE vn is not null AND dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND dateserv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "'  AND f_cancel = 0  ) AS receipthd  ) AS OPD  ) AS totalamount  LEFT JOIN masmtypergt ON masmtypergt.mtrgtid = totalamount.mtrgtid LEFT JOIN masstypergt ON masstypergt.strgtid =totalamount.strgtid LEFT JOIN massubrgt ON massubrgt.sbrgtid = totalamount.sbrgtid  "
        dtset1.Tables("ProfitShare").Clear()

        connect.GetTable(sql, dtset1.Tables("ProfitShare"))
    End Sub
    Public Sub getPersonStaticClinic(Start As Date, ToDate As Date)
        Dim sql As String
        sql = "SELECT  frnservice.vn,rectime,sentime,fintime,cliniccode,clinicname,person.hn,frnservice.date_serv,chiefcomp,CONCAT_WS(' ',hospemp.name ,hospemp.lname) AS DRNAME ,CONCAT_WS( ' ',stprename,person.name , person.lname ) AS PATIENT FROM  ( SELECT  rectime,sentime,fintime,clinic,vn,doctor  FROM  frnclinic  WHERE  sentime >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND sentime  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "'   ) AS frnclinic JOIN (SELECT vn,hn,date_serv,chiefcomp FROM  frnservice  WHERE  date_serv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND date_serv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' ) AS frnservice  ON frnservice.vn = frnclinic.vn JOIN person ON person.hn = frnservice.hn   JOIN masclinic ON frnclinic.clinic = masclinic.clinic  LEFT JOIN hospemp ON  hospemp.empid = frnclinic.doctor JOIN masprename ON person.prename = masprename.prename ;"
        dtset.Tables("VisitPatientClinic").Clear()
        connect.GetTable(sql, dtset.Tables("VisitPatientClinic"))
        '   connect.GetTable(sql, dtset1.Tables("ProfitShare"))

    End Sub
    Public Sub getMasclinic()
        Dim sql As String
        sql = "SELECT clinic,cliniccode,clinicname FROM masclinic WHERE status = 1"
        dtset.Tables("CLINIC").Clear()
        connect.GetTable(sql, dtset.Tables("CLINIC"))
    End Sub
    Public Sub getPersonDoctor(Start As Date, ToDate As Date)
        Dim sql As String
        sql = "SELECT frnservice.vn,frnservice.hn,date_serv,chiefcomp,doctor, CONCAT_WS('',hospemp.name , ' ' ,hospemp.lname) AS doctor_name , CONCAT_WS ('',masprename.ftprename , ' ',person.name , ' '  ,person.lname) AS person_name FROM  (SELECT * FROM  frnservice WHERE  date_serv >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND date_serv  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "'  ) AS frnservice JOIN  (SELECT vn,doctor FROM frnclinic WHERE doctor IS NOT NULL AND doctor > 0 AND  sentime >'" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "'  AND sentime  < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "'  ) AS frnclinic On frnclinic.vn = frnservice.vn JOIN hospemp ON hospemp.empid = frnclinic.doctor JOIN ( SELECT hn,prename,name,lname  FROM person ) AS person  ON frnservice.hn	 = person.hn JOIN masprename ON masprename.PRENAME = person.prename       GROUP BY frnclinic.vn,frnclinic.doctor;"
        dtset.Tables("DoctorVisit").Clear()
        connect.GetTable(sql, dtset.Tables("DoctorVisit"))
    End Sub
   
End Class
