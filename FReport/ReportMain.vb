﻿Public Class ReportMain

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        Dim drugExtPanel As New DrgExtForm
        'ReportviewerPanel.Controls.Add(extRaportCtrl)
        drugExtPanel.MdiParent = Me
        drugExtPanel.Dock = DockStyle.Fill
        drugExtPanel.WindowState = FormWindowState.Maximized
        drugExtPanel.Show()
        Me.RibbonControl1.SelectedPage = RibbonControl1.MergedPages(0)
    End Sub

 

    Private Sub ReportMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim db = ConnecDBRYH.NewConnection
        Dim sql As String = "SELECT ph.COMID,ph.CLINIC,ph.COM_NAME"
        sql += ",cl.CLINICCODE,cl.CLINICNAME,cl.dc ,stkid "
        sql += "FROM (SELECT COMID,CLINIC,COM_NAME FROM  comsphere WHERE(COM_NAME = '" & Environment.MachineName & "' AND `STATUS` = 1)) AS ph "
        sql += "LEFT JOIN (SELECT CLINIC,CLINICCODE,CLINICNAME,dc,stkid FROM  masclinic WHERE(`STATUS` = 1)) AS cl ON ph.CLINIC = cl.CLINIC;"
        Dim dt As New DataTable
        dt = db.GetTable(sql)
        If dt.Rows.Count > 0 Then
            ENVIRONMENTCLASS.MACHINE.Clinic = dt.Rows(0)("CLINIC").ToString.Trim
            ENVIRONMENTCLASS.MACHINE.ClinicCode = dt.Rows(0)("CLINICCODE").ToString.Trim
            ENVIRONMENTCLASS.MACHINE.ClinicName = dt.Rows(0)("CLINICNAME").ToString.Trim
            ENVIRONMENTCLASS.MACHINE.NextClinic = dt.Rows(0)("dc").ToString.Trim
            ENVIRONMENTCLASS.MACHINE.IDCOM = dt.Rows(0)("COMID").ToString.Trim
            ENVIRONMENTCLASS.MACHINE.Stkid = dt.Rows(0)("stkid").ToString.Trim
        End If
        db.Dispose()

    End Sub

    Private Sub MapMenu_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles MapMenu.ItemClick
        Dim testMapPanel As New testMap
        testMapPanel.MdiParent = Me
        testMapPanel.Dock = DockStyle.Fill
        testMapPanel.WindowState = FormWindowState.Maximized
        testMapPanel.Show()
    End Sub

    Private Sub btnInvoice_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnInvoice.ItemClick
        Dim AccountPanel As New AccountForm
        'ReportviewerPanel.Controls.Add(extRaportCtrl)
        AccountPanel.MdiParent = Me
        AccountPanel.Dock = DockStyle.Fill
        AccountPanel.WindowState = FormWindowState.Maximized
        AccountPanel.Show()
        Me.RibbonControl1.SelectedPage = RibbonControl1.MergedPages(0)
    End Sub

    Private Sub BarButtonItem6_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        Dim PersonStatisticPanel As New PersonStatisticForm
        'ReportviewerPanel.Controls.Add(extRaportCtrl)
        PersonStatisticPanel.MdiParent = Me
        PersonStatisticPanel.Dock = DockStyle.Fill
        PersonStatisticPanel.WindowState = FormWindowState.Maximized
        PersonStatisticPanel.Show()
        Me.RibbonControl1.SelectedPage = RibbonControl1.MergedPages(0)
    End Sub

    Private Sub BarButtonItem7_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem7.ItemClick
        Dim PersonStatisticPanel As New StkDrgForm
        'ReportviewerPanel.Controls.Add(extRaportCtrl)
        PersonStatisticPanel.MdiParent = Me
        PersonStatisticPanel.Dock = DockStyle.Fill
        PersonStatisticPanel.WindowState = FormWindowState.Maximized
        PersonStatisticPanel.Show()
        Me.RibbonControl1.SelectedPage = RibbonControl1.MergedPages(0)
    End Sub

    Private Sub BarButtonItem3_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        Dim RegDashboardPanel As New RegDashboardForm
        RegDashboardPanel.MdiParent = Me
        RegDashboardPanel.Dock = DockStyle.Fill
        RegDashboardPanel.WindowState = FormWindowState.Maximized
        RegDashboardPanel.Show()
        Me.RibbonControl1.SelectedPage = RibbonControl1.MergedPages(0)
    End Sub

    Private Sub Btndebtor_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles Btndebtor.ItemClick
        Dim DebtFormPanel As New DebtForm
        DebtFormPanel.MdiParent = Me
        DebtFormPanel.Dock = DockStyle.Fill
        DebtFormPanel.WindowState = FormWindowState.Maximized
        DebtFormPanel.Show()
        Me.RibbonControl1.SelectedPage = RibbonControl1.MergedPages(0)
    End Sub

    Private Sub BarButtonItem8_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem8.ItemClick
        Dim DetailFormPanel As New DetailForm
        DetailFormPanel.MdiParent = Me
        DetailFormPanel.Dock = DockStyle.Fill
        DetailFormPanel.WindowState = FormWindowState.Maximized
        DetailFormPanel.Show()
        Me.RibbonControl1.SelectedPage = RibbonControl1.MergedPages(0)
    End Sub
End Class