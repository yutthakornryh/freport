﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class OrderDtCategory
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.PivotGridControl1 = New DevExpress.XtraPivotGrid.PivotGridControl()
        Me.OrderSet1 = New FReport.OrderSet()
        Me.fieldprdcode1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldprdname1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fielddorder1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldname1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldprdprc1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldCOUNT = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldIPDPRC = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldOPDPRC = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.RadioGroup1 = New DevExpress.XtraEditors.RadioGroup()
        Me.txtAgent = New DevExpress.XtraEditors.LookUpEdit()
        Me.btnApplyFilter = New DevExpress.XtraEditors.SimpleButton()
        Me.dateTo = New DevExpress.XtraEditors.DateEdit()
        Me.dateStart = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OrderSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAgent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.CustomizationFormText = "วันที่"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(278, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(226, 26)
        Me.LayoutControlItem1.Text = "วันที่"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(31, 13)
        Me.LayoutControlItem1.TextToControlDistance = 5
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.CustomizationFormText = "ถึงวันที่"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(504, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(251, 26)
        Me.LayoutControlItem2.Text = "ถึงวันที่"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(31, 13)
        Me.LayoutControlItem2.TextToControlDistance = 5
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.PivotGridControl1)
        Me.LayoutControl1.Controls.Add(Me.RadioGroup1)
        Me.LayoutControl1.Controls.Add(Me.txtAgent)
        Me.LayoutControl1.Controls.Add(Me.btnApplyFilter)
        Me.LayoutControl1.Controls.Add(Me.dateTo)
        Me.LayoutControl1.Controls.Add(Me.dateStart)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1141, 656)
        Me.LayoutControl1.TabIndex = 2
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'PivotGridControl1
        '
        Me.PivotGridControl1.DataMember = "ORDERDT"
        Me.PivotGridControl1.DataSource = Me.OrderSet1
        Me.PivotGridControl1.Fields.AddRange(New DevExpress.XtraPivotGrid.PivotGridField() {Me.fieldprdcode1, Me.fieldprdname1, Me.fielddorder1, Me.fieldname1, Me.fieldprdprc1, Me.fieldCOUNT, Me.fieldIPDPRC, Me.fieldOPDPRC})
        Me.PivotGridControl1.Location = New System.Drawing.Point(2, 31)
        Me.PivotGridControl1.Name = "PivotGridControl1"
        Me.PivotGridControl1.Size = New System.Drawing.Size(1137, 623)
        Me.PivotGridControl1.TabIndex = 10
        '
        'OrderSet1
        '
        Me.OrderSet1.DataSetName = "OrderSet"
        Me.OrderSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'fieldprdcode1
        '
        Me.fieldprdcode1.AreaIndex = 0
        Me.fieldprdcode1.Caption = "CODE"
        Me.fieldprdcode1.FieldName = "prdcode"
        Me.fieldprdcode1.Name = "fieldprdcode1"
        '
        'fieldprdname1
        '
        Me.fieldprdname1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldprdname1.AreaIndex = 0
        Me.fieldprdname1.Caption = "PRODUCTNAME"
        Me.fieldprdname1.FieldName = "prdname"
        Me.fieldprdname1.Name = "fieldprdname1"
        Me.fieldprdname1.Width = 149
        '
        'fielddorder1
        '
        Me.fielddorder1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fielddorder1.AreaIndex = 0
        Me.fielddorder1.Caption = "วันที่"
        Me.fielddorder1.CellFormat.FormatString = "d"
        Me.fielddorder1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.fielddorder1.FieldName = "d_order"
        Me.fielddorder1.Name = "fielddorder1"
        '
        'fieldname1
        '
        Me.fieldname1.AreaIndex = 1
        Me.fieldname1.Caption = "ชื่อ - นามสกุล"
        Me.fieldname1.FieldName = "name"
        Me.fieldname1.Name = "fieldname1"
        '
        'fieldprdprc1
        '
        Me.fieldprdprc1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldprdprc1.AreaIndex = 1
        Me.fieldprdprc1.Caption = "ราคาได้รับ"
        Me.fieldprdprc1.CellFormat.FormatString = "n2"
        Me.fieldprdprc1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldprdprc1.FieldName = "prdprc"
        Me.fieldprdprc1.Name = "fieldprdprc1"
        '
        'fieldCOUNT
        '
        Me.fieldCOUNT.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldCOUNT.AreaIndex = 0
        Me.fieldCOUNT.Caption = "จำนวน"
        Me.fieldCOUNT.FieldName = "COUNT"
        Me.fieldCOUNT.Name = "fieldCOUNT"
        '
        'fieldIPDPRC
        '
        Me.fieldIPDPRC.AreaIndex = 3
        Me.fieldIPDPRC.FieldName = "IPDPRC"
        Me.fieldIPDPRC.Name = "fieldIPDPRC"
        '
        'fieldOPDPRC
        '
        Me.fieldOPDPRC.AreaIndex = 2
        Me.fieldOPDPRC.FieldName = "OPDPRC"
        Me.fieldOPDPRC.Name = "fieldOPDPRC"
        '
        'RadioGroup1
        '
        Me.RadioGroup1.Location = New System.Drawing.Point(155, 2)
        Me.RadioGroup1.Name = "RadioGroup1"
        Me.RadioGroup1.Properties.Columns = 2
        Me.RadioGroup1.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "วัน"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "เดือน")})
        Me.RadioGroup1.Size = New System.Drawing.Size(135, 25)
        Me.RadioGroup1.StyleController = Me.LayoutControl1
        Me.RadioGroup1.TabIndex = 9
        '
        'txtAgent
        '
        Me.txtAgent.Location = New System.Drawing.Point(333, 2)
        Me.txtAgent.Name = "txtAgent"
        Me.txtAgent.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.txtAgent.Properties.NullText = "ประเภท"
        Me.txtAgent.Size = New System.Drawing.Size(160, 20)
        Me.txtAgent.StyleController = Me.LayoutControl1
        Me.txtAgent.TabIndex = 8
        '
        'btnApplyFilter
        '
        Me.btnApplyFilter.Location = New System.Drawing.Point(865, 2)
        Me.btnApplyFilter.Name = "btnApplyFilter"
        Me.btnApplyFilter.Size = New System.Drawing.Size(274, 22)
        Me.btnApplyFilter.StyleController = Me.LayoutControl1
        Me.btnApplyFilter.TabIndex = 7
        Me.btnApplyFilter.Text = "Report"
        '
        'dateTo
        '
        Me.dateTo.EditValue = Nothing
        Me.dateTo.Location = New System.Drawing.Point(717, 2)
        Me.dateTo.Name = "dateTo"
        Me.dateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateTo.Properties.Mask.EditMask = "D"
        Me.dateTo.Size = New System.Drawing.Size(144, 20)
        Me.dateTo.StyleController = Me.LayoutControl1
        Me.dateTo.TabIndex = 5
        '
        'dateStart
        '
        Me.dateStart.EditValue = Nothing
        Me.dateStart.Location = New System.Drawing.Point(536, 2)
        Me.dateStart.Name = "dateStart"
        Me.dateStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateStart.Size = New System.Drawing.Size(138, 20)
        Me.dateStart.StyleController = Me.LayoutControl1
        Me.dateStart.TabIndex = 4
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.EmptySpaceItem1, Me.LayoutControlItem5})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1141, 656)
        Me.LayoutControlGroup1.Text = "LayoutControlGroup1"
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.dateStart
        Me.LayoutControlItem3.CustomizationFormText = "วันที่"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(495, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem1"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(181, 29)
        Me.LayoutControlItem3.Text = "วันที่"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(36, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.dateTo
        Me.LayoutControlItem4.CustomizationFormText = "ถึงวันที่"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(676, 0)
        Me.LayoutControlItem4.Name = "LayoutControlItem2"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(187, 29)
        Me.LayoutControlItem4.Text = "ถึงวันที่"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(36, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.btnApplyFilter
        Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem4"
        Me.LayoutControlItem6.Location = New System.Drawing.Point(863, 0)
        Me.LayoutControlItem6.Name = "LayoutControlItem4"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(278, 29)
        Me.LayoutControlItem6.Text = "LayoutControlItem4"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextToControlDistance = 0
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.txtAgent
        Me.LayoutControlItem7.CustomizationFormText = "หมวด"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(292, 0)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(203, 29)
        Me.LayoutControlItem7.Text = "ประเภท"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(36, 13)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.RadioGroup1
        Me.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8"
        Me.LayoutControlItem8.Location = New System.Drawing.Point(153, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(139, 29)
        Me.LayoutControlItem8.Text = "LayoutControlItem8"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextToControlDistance = 0
        Me.LayoutControlItem8.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(153, 29)
        Me.EmptySpaceItem1.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.PivotGridControl1
        Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 29)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(1141, 627)
        Me.LayoutControlItem5.Text = "LayoutControlItem5"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextToControlDistance = 0
        Me.LayoutControlItem5.TextVisible = False
        '
        'OrderDtCategory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "OrderDtCategory"
        Me.Size = New System.Drawing.Size(1141, 656)
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OrderSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadioGroup1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAgent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents txtAgent As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents btnApplyFilter As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dateTo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dateStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents RadioGroup1 As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents PivotGridControl1 As DevExpress.XtraPivotGrid.PivotGridControl
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents OrderSet1 As FReport.OrderSet
    Friend WithEvents fieldprdcode1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldprdname1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fielddorder1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldname1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldprdprc1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldCOUNT As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldIPDPRC As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldOPDPRC As DevExpress.XtraPivotGrid.PivotGridField

End Class
