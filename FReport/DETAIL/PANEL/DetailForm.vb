﻿Imports DevExpress.XtraReports.UI

Public Class DetailForm
    Dim foodDetailForm As FoodDetail
    Dim reportname As String
    Dim report As New XtraReport

    Dim orderDtcategory As OrderDtCategory


    Private Sub DetailForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None

    End Sub

    Private Sub menuFrnDrugPay_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles menuFrnDrugPay.LinkClicked
        foodDetailForm = New FoodDetail
        AddHandler foodDetailForm.btnApplyFilter.Click, AddressOf InvoicePanel
        foodDetailForm.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(foodDetailForm)
        reportname = "FoodDetail"
    End Sub

    Private Sub menuMonthlyDrgPay_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles menuMonthlyDrgPay.LinkClicked
        orderDtcategory = New OrderDtCategory
        '  AddHandler foodDetailForm.btnApplyFilter.Click, AddressOf InvoicePanel

        orderDtcategory.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(orderDtcategory)
        reportname = "orderDtcategory"

    End Sub
    Private Sub InvoicePanel(sender As Object, e As EventArgs)
        If reportname = "DebtPrintForm" Then
            Me.report = foodDetailForm.report
        ElseIf reportname = "Invoice" Then
            Me.report = orderDtcategory.report
        End If
        'RemoveHandler bj8Panel.btnApplyFilter.Click, AddressOf bj8PanelShowing
    End Sub

    Private Sub BarButtonItem6_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        If report IsNot Nothing Then
            FolderBrowserDialog1.Description = "Pick Folder to store PDF files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                ExportReport(report, FolderBrowserDialog1.SelectedPath & "\" & reportname & ".pdf", "pdf")
            End If
        End If


    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        If report IsNot Nothing Then
            FolderBrowserDialog1.Description = "Pick Folder to store Excecl files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                ExportReport(report, FolderBrowserDialog1.SelectedPath & "\" & reportname & ".xls", "xls")

            End If
        End If
    End Sub

    Private Sub BarButtonItem4_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        If report IsNot Nothing Then
            Dim printTool As New ReportPrintTool(report)
            printTool.Print()
            printTool.PrintingSystem.ShowMarginsWarning = False
        End If
    End Sub

    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        If report IsNot Nothing Then
            Dim printTool As New ReportPrintTool(report)
            printTool.PrintDialog()
        End If
    End Sub
End Class