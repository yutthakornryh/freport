﻿Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraEditors.Controls

Public Class OrderDtCategory
    Dim dtset As New OrderSet
    Public Property report As XtraReport
    Dim orderDT As New DETAILCLASS(dtset)
    Dim InvoiceCheck As Integer
    Private Sub btnApplyFilter_Click(sender As Object, e As EventArgs) Handles btnApplyFilter.Click
        dtset.Tables("ORDERDT").Clear()
        '   MsgBox(RadioGroup1.SelectedIndex)
        If RadioGroup1.SelectedIndex = 0 Then
            orderDT.OrderDetailDay(dateStart.EditValue, dateTo.EditValue, txtAgent.EditValue)
        ElseIf RadioGroup1.SelectedIndex = 1 Then
            orderDT.OrderDetail(dateStart.EditValue, dateTo.EditValue, txtAgent.EditValue)
        End If

        PivotGridControl1.DataSource = dtset

    End Sub

    Private Sub OrderDtCategory_Load(sender As Object, e As EventArgs) Handles Me.Load
        dateStart.EditValue = Now
        dateTo.EditValue = Now

        orderDT.getOrderCats()

        txtAgent.Properties.DataSource = dtset.Tables("PRDCAT")
        txtAgent.Properties.DisplayMember = "catname"
        txtAgent.Properties.ValueMember = "prdcat"

        Dim col3 As LookUpColumnInfoCollection = txtAgent.Properties.Columns
        col3.Add(New LookUpColumnInfo("catname", 0))
        col3.Add(New LookUpColumnInfo("prdcat", 0))

        txtAgent.Properties.BestFitMode = BestFitMode.BestFitResizePopup
        txtAgent.Properties.SearchMode = SearchMode.AutoComplete
        txtAgent.Properties.AutoSearchColumnIndex = 1
    End Sub
End Class
