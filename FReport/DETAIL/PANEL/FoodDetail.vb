﻿Imports DevExpress.XtraReports.UI

Public Class FoodDetail
    Dim dtset As New OrderSet
    Public Property report As XtraReport
    Dim orderDT As New DETAILCLASS(dtset)
    Dim InvoiceCheck As Integer
    Private Sub btnApplyFilter_Click(sender As Object, e As EventArgs) Handles btnApplyFilter.Click
        '   orderDT = New DETAILCLASS(dtset)_
        ' acclass.InvoiceDebt(dateStart.EditValue, dateTo.EditValue, txtAgent.EditValue)
        orderDT.orderFoodDetail(dateStart.EditValue, dateTo.EditValue)
        report = New FoodDetailReport
        report.DataSource = dtset
        report.Parameters("DateDescription").Value = "เอกสารค่าอาหาร ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text

        report.CreateDocument()
        DocumentViewer1.DocumentSource = report
    End Sub
End Class
