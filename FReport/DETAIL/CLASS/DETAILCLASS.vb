﻿Public Class DETAILCLASS
    Dim dtset As OrderSet
    Dim connect As ConnecDBRYH
    Dim typeStr As String
    Public Sub New(ByRef dt As OrderSet)
        connect = ConnecDBRYH.NewConnection
        dtset = dt
    End Sub
    Public Sub orderFoodDetail(Start As Date, ToDate As Date)
        Dim sql As String
        dtset.Tables("ORDERDT").Clear()
        sql = "SELECT * FROM fditem WHERE status = 1"
        Dim dt As New DataTable
        dt = connect.GetTable(sql)
        Dim sql1 As String
        For i As Integer = 0 To dt.Rows.Count - 1
            If IsDBNull(dt.Rows(i)("prdcodeb")) Then
            Else
                sql1 += " prdcode = " & dt.Rows(i)("prdcodeb").ToString & " OR "

            End If

            If IsDBNull(dt.Rows(i)("prdcodel")) Then
            Else
                sql1 += "  prdcode = " & dt.Rows(i)("prdcodel").ToString & " OR "

            End If

            If IsDBNull(dt.Rows(i)("prdcoded")) Then

            Else
                sql1 += " prdcode = " & dt.Rows(i)("prdcoded").ToString & " "

            End If

            If i = dt.Rows.Count - 1 Then
            Else
                If IsDBNull(dt.Rows(i)("prdcodeb")) Then
                Else
                    sql1 += "OR "

                End If
            End If
        Next

        sql = "SELECT prdorderdt.prdcode , masproduct.prdname , prdorder.an ,d_order,bedscode,bedsname,CONCAT_WS('',person.name , ' ' , person.lname) AS name ,prdprc FROM (SELECT * FROM prdorderdt WHERE usmktime > '" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "' AND usmktime < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0 AND ( " & sql1 & " ) ) AS prdorderdt JOIN masproduct ON masproduct.prdcode = prdorderdt.prdcode  JOIN  ( SELECT * FROM prdorder WHERE  d_order > '" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "' AND d_order < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0 ) AS prdorder ON prdorder.orderid = prdorderdt.orderid JOIN  (SELECT * FROM frnshift WHERE outtime IS NULL ) AS frnshift ON prdorder.an = frnshift.an JOIN sroomitem ON sroomitem.bedsid = frnshift.bedsid JOIN person ON person.hn = prdorder.hn ;"

        connect.GetTable(sql, dtset.Tables("ORDERDT"))
    End Sub
    Public Sub getOrderCats()

        Dim sql As String

        sql = "SELECT prdcat , catname FROM masprdcats "
        dtset.Tables("PRDCAT").Clear()
        connect.GetTable(sql, dtset.Tables("PRDCAT"))

    End Sub
    Public Sub OrderDetail(Start As Date, ToDate As Date, ByVal Category As String)
        Dim sql As String
        sql = "SELECT prdorderdt.prdcode , masproduct.prdname , prdorder.an ,date_format(d_order,'%M-%y') AS d_order ,bedscode,bedsname,CONCAT_WS('',person.name , ' ' , person.lname) AS name , qty * prdprc AS prdprc,ipdprc,opdprc, qty AS 'COUNT' FROM (SELECT * FROM prdorderdt WHERE usmktime > '" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "' AND usmktime < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0  AND prdcat = " & Category & " ) AS prdorderdt JOIN masproduct ON masproduct.prdcode = prdorderdt.prdcode  JOIN  ( SELECT * FROM prdorder WHERE  d_order > '" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "' AND d_order < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0 AND prdcat =" & Category & " ) AS prdorder ON prdorder.orderid = prdorderdt.orderid JOIN  (SELECT * FROM frnshift WHERE outtime IS NULL ) AS frnshift ON prdorder.an = frnshift.an JOIN sroomitem ON sroomitem.bedsid = frnshift.bedsid JOIN person ON person.hn = prdorder.hn ;"
        connect.GetTable(sql, dtset.Tables("ORDERDT"))

    End Sub
    Public Sub OrderDetailDay(Start As Date, ToDate As Date, ByVal Category As String)
        Dim sql As String
        sql = "SELECT prdorderdt.prdcode , masproduct.prdname , prdorder.an ,date_format(d_order,'%d-%M-%y') AS d_order ,bedscode,bedsname,CONCAT_WS('',person.name , ' ' , person.lname) AS name , qty * prdprc AS prdprc,ipdprc,opdprc, qty AS 'COUNT' FROM (SELECT * FROM prdorderdt WHERE usmktime > '" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "' AND usmktime < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0  AND prdcat = " & Category & " ) AS prdorderdt JOIN masproduct ON masproduct.prdcode = prdorderdt.prdcode  JOIN  ( SELECT * FROM prdorder WHERE  d_order > '" & Start.Year & Start.ToString("-MM-dd") & " 00:00:01" & "' AND d_order < '" & ToDate.Year & ToDate.ToString("-MM-dd") & " 00:00:01" & "' AND f_cancel = 0 AND prdcat =" & Category & " ) AS prdorder ON prdorder.orderid = prdorderdt.orderid JOIN  (SELECT * FROM frnshift WHERE outtime IS NULL ) AS frnshift ON prdorder.an = frnshift.an JOIN sroomitem ON sroomitem.bedsid = frnshift.bedsid JOIN person ON person.hn = prdorder.hn ;"
        connect.GetTable(sql, dtset.Tables("ORDERDT"))
    End Sub
End Class
