﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReportMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReportMain))
        Me.RibbonControl1 = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.btnDrugMenu = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.MapMenu = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.btnInvoice = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem7 = New DevExpress.XtraBars.BarButtonItem()
        Me.Btndebtor = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem8 = New DevExpress.XtraBars.BarButtonItem()
        Me.regPage = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RegDate = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup5 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup6 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.drugPage = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup4 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.FinancePage = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.FNC = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl1
        '
        Me.RibbonControl1.ExpandCollapseItem.Id = 0
        Me.RibbonControl1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl1.ExpandCollapseItem, Me.btnDrugMenu, Me.BarButtonItem1, Me.BarButtonItem2, Me.BarButtonItem3, Me.BarButtonItem4, Me.MapMenu, Me.BarButtonItem5, Me.btnInvoice, Me.BarButtonItem6, Me.BarButtonItem7, Me.Btndebtor, Me.BarButtonItem8})
        Me.RibbonControl1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl1.MaxItemId = 4
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.regPage, Me.drugPage, Me.FinancePage})
        Me.RibbonControl1.Size = New System.Drawing.Size(1264, 140)
        '
        'btnDrugMenu
        '
        Me.btnDrugMenu.Caption = "รายงานห้องยา"
        Me.btnDrugMenu.CategoryGuid = New System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537")
        Me.btnDrugMenu.Glyph = Global.FReport.My.Resources.Resources._new
        Me.btnDrugMenu.Id = 1
        Me.btnDrugMenu.Name = "btnDrugMenu"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "รายงานภายนอก"
        Me.BarButtonItem1.Glyph = Global.FReport.My.Resources.Resources.projectfile_16x16
        Me.BarButtonItem1.Id = 2
        Me.BarButtonItem1.LargeGlyph = Global.FReport.My.Resources.Resources.projectfile_32x32
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "รายงานสถิติ"
        Me.BarButtonItem2.Glyph = Global.FReport.My.Resources.Resources.stackedbar_32x32
        Me.BarButtonItem2.Id = 3
        Me.BarButtonItem2.LargeGlyph = Global.FReport.My.Resources.Resources.stackedbar_32x32
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "รายงานสถิติ OPD TIME"
        Me.BarButtonItem3.Glyph = Global.FReport.My.Resources.Resources.stackedbar_16x16
        Me.BarButtonItem3.Id = 4
        Me.BarButtonItem3.LargeGlyph = Global.FReport.My.Resources.Resources.stackedbar_32x32
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "test"
        Me.BarButtonItem4.Enabled = False
        Me.BarButtonItem4.Glyph = Global.FReport.My.Resources.Resources.piestylepie_16x16
        Me.BarButtonItem4.Id = 5
        Me.BarButtonItem4.LargeGlyph = Global.FReport.My.Resources.Resources.piestylepie_32x32
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'MapMenu
        '
        Me.MapMenu.Caption = "แผนที่"
        Me.MapMenu.Id = 7
        Me.MapMenu.LargeGlyph = Global.FReport.My.Resources.Resources.country_32x32
        Me.MapMenu.Name = "MapMenu"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "รายงานภายใน"
        Me.BarButtonItem5.Glyph = CType(resources.GetObject("BarButtonItem5.Glyph"), System.Drawing.Image)
        Me.BarButtonItem5.Id = 8
        Me.BarButtonItem5.LargeGlyph = CType(resources.GetObject("BarButtonItem5.LargeGlyph"), System.Drawing.Image)
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'btnInvoice
        '
        Me.btnInvoice.Caption = "รายงานใบแจ้งหนี้"
        Me.btnInvoice.CategoryGuid = New System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537")
        Me.btnInvoice.Id = 1
        Me.btnInvoice.LargeGlyph = Global.FReport.My.Resources.Resources.textbox_32x32
        Me.btnInvoice.Name = "btnInvoice"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "รายงานบริหาร"
        Me.BarButtonItem6.CategoryGuid = New System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537")
        Me.BarButtonItem6.Glyph = CType(resources.GetObject("BarButtonItem6.Glyph"), System.Drawing.Image)
        Me.BarButtonItem6.Id = 2
        Me.BarButtonItem6.LargeGlyph = CType(resources.GetObject("BarButtonItem6.LargeGlyph"), System.Drawing.Image)
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'BarButtonItem7
        '
        Me.BarButtonItem7.Caption = "คลังยา"
        Me.BarButtonItem7.CategoryGuid = New System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537")
        Me.BarButtonItem7.Id = 3
        Me.BarButtonItem7.LargeGlyph = Global.FReport.My.Resources.Resources.pill
        Me.BarButtonItem7.Name = "BarButtonItem7"
        '
        'Btndebtor
        '
        Me.Btndebtor.Caption = "ลูกหนี้"
        Me.Btndebtor.CategoryGuid = New System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537")
        Me.Btndebtor.Glyph = Global.FReport.My.Resources.Resources.currency_16x16
        Me.Btndebtor.Id = 1
        Me.Btndebtor.LargeGlyph = Global.FReport.My.Resources.Resources.currency_32x32
        Me.Btndebtor.Name = "Btndebtor"
        '
        'BarButtonItem8
        '
        Me.BarButtonItem8.Caption = "รายงานค่าใช้จ่าย"
        Me.BarButtonItem8.CategoryGuid = New System.Guid("6ffddb2b-9015-4d97-a4c1-91613e0ef537")
        Me.BarButtonItem8.Glyph = CType(resources.GetObject("BarButtonItem8.Glyph"), System.Drawing.Image)
        Me.BarButtonItem8.Id = 3
        Me.BarButtonItem8.LargeGlyph = CType(resources.GetObject("BarButtonItem8.LargeGlyph"), System.Drawing.Image)
        Me.BarButtonItem8.Name = "BarButtonItem8"
        '
        'regPage
        '
        Me.regPage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RegDate, Me.RibbonPageGroup5, Me.RibbonPageGroup6})
        Me.regPage.Name = "regPage"
        Me.regPage.Text = "รายงานสถิติ"
        '
        'RegDate
        '
        Me.RegDate.ItemLinks.Add(Me.BarButtonItem6)
        Me.RegDate.ItemLinks.Add(Me.BarButtonItem3)
        Me.RegDate.Name = "RegDate"
        '
        'RibbonPageGroup5
        '
        Me.RibbonPageGroup5.ItemLinks.Add(Me.BarButtonItem7)
        Me.RibbonPageGroup5.Name = "RibbonPageGroup5"
        Me.RibbonPageGroup5.Text = "คลังยา"
        '
        'RibbonPageGroup6
        '
        Me.RibbonPageGroup6.ItemLinks.Add(Me.BarButtonItem8)
        Me.RibbonPageGroup6.Name = "RibbonPageGroup6"
        Me.RibbonPageGroup6.Text = "Detail ค่าใช้จ่าย"
        '
        'drugPage
        '
        Me.drugPage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1, Me.RibbonPageGroup4})
        Me.drugPage.Name = "drugPage"
        Me.drugPage.Text = "รายงานห้องยา"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.Glyph = CType(resources.GetObject("RibbonPageGroup1.Glyph"), System.Drawing.Image)
        Me.RibbonPageGroup1.ItemLinks.Add(Me.BarButtonItem1)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.ShowCaptionButton = False
        '
        'RibbonPageGroup4
        '
        Me.RibbonPageGroup4.ItemLinks.Add(Me.BarButtonItem5)
        Me.RibbonPageGroup4.Name = "RibbonPageGroup4"
        '
        'FinancePage
        '
        Me.FinancePage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.FNC, Me.RibbonPageGroup3})
        Me.FinancePage.Name = "FinancePage"
        Me.FinancePage.Text = "รายงานบัญชี"
        '
        'FNC
        '
        Me.FNC.ItemLinks.Add(Me.btnInvoice)
        Me.FNC.Name = "FNC"
        Me.FNC.Text = "รายงารการเงิน"
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.ItemLinks.Add(Me.Btndebtor)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.Text = "ลูกหนี้"
        '
        'DefaultLookAndFeel1
        '
        Me.DefaultLookAndFeel1.LookAndFeel.SkinName = "Office 2010 Blue"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.btnInvoice)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "รายงารการเงิน"
        '
        'ReportMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1264, 682)
        Me.Controls.Add(Me.RibbonControl1)
        Me.IsMdiContainer = True
        Me.Name = "ReportMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RibbonControl1 As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents drugPage As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents btnDrugMenu As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents regPage As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RegDate As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents MapMenu As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup4 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents btnInvoice As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents FinancePage As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents FNC As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents BarButtonItem7 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup5 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents Btndebtor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem8 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup6 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
End Class
