﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class testMap
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ImageTilesLayer1 As DevExpress.XtraMap.ImageTilesLayer = New DevExpress.XtraMap.ImageTilesLayer()
        Dim BingMapDataProvider1 As DevExpress.XtraMap.BingMapDataProvider = New DevExpress.XtraMap.BingMapDataProvider()
        Me.mapPanel = New DevExpress.XtraEditors.PanelControl()
        Me.MapControl = New DevExpress.XtraMap.MapControl()
        CType(Me.mapPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mapPanel.SuspendLayout()
        CType(Me.MapControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mapPanel
        '
        Me.mapPanel.Controls.Add(Me.MapControl)
        Me.mapPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.mapPanel.Location = New System.Drawing.Point(0, 0)
        Me.mapPanel.Name = "mapPanel"
        Me.mapPanel.Size = New System.Drawing.Size(1232, 464)
        Me.mapPanel.TabIndex = 1
        '
        'MapControl
        '
        Me.MapControl.CenterPoint = New DevExpress.XtraMap.GeoPoint(7.0882R, 100.3105R)
        Me.MapControl.Dock = System.Windows.Forms.DockStyle.Fill
        BingMapDataProvider1.BingKey = "AkX4w0OhYScmcX9I5XU5kkIsE4Uo7Wja4_ROMvcWW4A7crPPdg1DnvBBcW6eF_kZ"
        BingMapDataProvider1.Kind = DevExpress.XtraMap.BingMapKind.Road
        ImageTilesLayer1.DataProvider = BingMapDataProvider1
        Me.MapControl.Layers.Add(ImageTilesLayer1)
        Me.MapControl.Location = New System.Drawing.Point(2, 2)
        Me.MapControl.Name = "MapControl"
        Me.MapControl.Size = New System.Drawing.Size(1228, 460)
        Me.MapControl.TabIndex = 0
        Me.MapControl.ZoomLevel = 6.0R
        '
        'testMap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1232, 464)
        Me.Controls.Add(Me.mapPanel)
        Me.Name = "testMap"
        Me.Text = "testMap"
        CType(Me.mapPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mapPanel.ResumeLayout(False)
        CType(Me.MapControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents mapPanel As DevExpress.XtraEditors.PanelControl
    Friend WithEvents MapControl As DevExpress.XtraMap.MapControl
End Class
