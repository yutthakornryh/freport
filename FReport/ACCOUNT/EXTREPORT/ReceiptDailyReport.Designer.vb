﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class ReceiptDailyReport
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.XrPivotGrid1 = New DevExpress.XtraReports.UI.XRPivotGrid()
        Me.AccountSet1 = New FReport.AccountSet()
        Me.fieldHN1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldbilldocid1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldTOTAL = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldDISCOUNT1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldfrnamount1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldaccmname1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldCONTACT1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.DateDescription = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Header = New DevExpress.XtraReports.Parameters.Parameter()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.AccountSet2 = New FReport.AccountSet()
        Me.XrControlStyle1 = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.CalculatedField1 = New DevExpress.XtraReports.UI.CalculatedField()
        Me.fieldName = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.CalculatedField2 = New DevExpress.XtraReports.UI.CalculatedField()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel8 = New DevExpress.XtraReports.UI.XRLabel()
        CType(Me.AccountSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AccountSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPivotGrid1})
        Me.Detail.HeightF = 133.75!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPivotGrid1
        '
        Me.XrPivotGrid1.CellStyleName = "XrControlStyle1"
        Me.XrPivotGrid1.DataMember = "ReceiptDaily"
        Me.XrPivotGrid1.DataSource = Me.AccountSet1
        Me.XrPivotGrid1.Fields.AddRange(New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField() {Me.fieldHN1, Me.fieldbilldocid1, Me.fieldName, Me.fieldTOTAL, Me.fieldDISCOUNT1, Me.fieldfrnamount1, Me.fieldaccmname1, Me.fieldCONTACT1})
        Me.XrPivotGrid1.FieldValueStyleName = "XrControlStyle1"
        Me.XrPivotGrid1.LocationFloat = New DevExpress.Utils.PointFloat(0.00003178914!, 10.00001!)
        Me.XrPivotGrid1.Name = "XrPivotGrid1"
        Me.XrPivotGrid1.OLAPConnectionString = ""
        Me.XrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3
        Me.XrPivotGrid1.SizeF = New System.Drawing.SizeF(1010.0!, 95.62498!)
        Me.XrPivotGrid1.TotalCellStyleName = "XrControlStyle1"
        '
        'AccountSet1
        '
        Me.AccountSet1.DataSetName = "AccountSet"
        Me.AccountSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'fieldHN1
        '
        Me.fieldHN1.Appearance.Cell.BackColor = System.Drawing.Color.White
        Me.fieldHN1.Appearance.Cell.BorderColor = System.Drawing.Color.White
        Me.fieldHN1.Appearance.Cell.ForeColor = System.Drawing.Color.White
        Me.fieldHN1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldHN1.AreaIndex = 2
        Me.fieldHN1.Caption = "HN"
        Me.fieldHN1.FieldName = "HN"
        Me.fieldHN1.Name = "fieldHN1"
        Me.fieldHN1.Width = 80
        '
        'fieldbilldocid1
        '
        Me.fieldbilldocid1.Appearance.Cell.BackColor = System.Drawing.Color.White
        Me.fieldbilldocid1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldbilldocid1.AreaIndex = 0
        Me.fieldbilldocid1.Caption = "เลขที่ใบเสร็จ"
        Me.fieldbilldocid1.FieldName = "billdocid"
        Me.fieldbilldocid1.Name = "fieldbilldocid1"
        '
        'fieldTOTAL
        '
        Me.fieldTOTAL.Appearance.Cell.BackColor = System.Drawing.Color.White
        Me.fieldTOTAL.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldTOTAL.AreaIndex = 4
        Me.fieldTOTAL.Caption = "ยอดชำระ"
        Me.fieldTOTAL.FieldName = "TOTAL"
        Me.fieldTOTAL.Name = "fieldTOTAL"
        Me.fieldTOTAL.ValueFormat.FormatString = "n"
        Me.fieldTOTAL.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldTOTAL.Width = 90
        '
        'fieldDISCOUNT1
        '
        Me.fieldDISCOUNT1.Appearance.Cell.BackColor = System.Drawing.Color.White
        Me.fieldDISCOUNT1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldDISCOUNT1.AreaIndex = 5
        Me.fieldDISCOUNT1.Caption = "ส่วนลด"
        Me.fieldDISCOUNT1.FieldName = "DISCOUNT"
        Me.fieldDISCOUNT1.Name = "fieldDISCOUNT1"
        Me.fieldDISCOUNT1.ValueFormat.FormatString = "n"
        Me.fieldDISCOUNT1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldDISCOUNT1.Width = 75
        '
        'fieldfrnamount1
        '
        Me.fieldfrnamount1.Appearance.Cell.BackColor = System.Drawing.Color.White
        Me.fieldfrnamount1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldfrnamount1.AreaIndex = 0
        Me.fieldfrnamount1.Caption = "Price"
        Me.fieldfrnamount1.CellFormat.FormatString = "n2"
        Me.fieldfrnamount1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldfrnamount1.FieldName = "frnamount"
        Me.fieldfrnamount1.GrandTotalCellFormat.FormatString = "n"
        Me.fieldfrnamount1.GrandTotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldfrnamount1.Name = "fieldfrnamount1"
        Me.fieldfrnamount1.TotalCellFormat.FormatString = "n2"
        Me.fieldfrnamount1.TotalCellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldfrnamount1.TotalValueFormat.FormatString = "n2"
        Me.fieldfrnamount1.TotalValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldfrnamount1.ValueFormat.FormatString = "n2"
        Me.fieldfrnamount1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldfrnamount1.Width = 85
        '
        'fieldaccmname1
        '
        Me.fieldaccmname1.Appearance.Cell.BackColor = System.Drawing.Color.White
        Me.fieldaccmname1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldaccmname1.AreaIndex = 0
        Me.fieldaccmname1.Caption = "ประเภทการชำระ"
        Me.fieldaccmname1.FieldName = "accm_name"
        Me.fieldaccmname1.Name = "fieldaccmname1"
        Me.fieldaccmname1.Width = 90
        '
        'fieldCONTACT1
        '
        Me.fieldCONTACT1.Appearance.Cell.BackColor = System.Drawing.Color.White
        Me.fieldCONTACT1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldCONTACT1.AreaIndex = 1
        Me.fieldCONTACT1.Caption = "CONTACT"
        Me.fieldCONTACT1.FieldName = "CONTACT"
        Me.fieldCONTACT1.Name = "fieldCONTACT1"
        Me.fieldCONTACT1.Width = 80
        '
        'TopMargin
        '
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel2, Me.XrLabel1})
        Me.TopMargin.HeightF = 100.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.DateDescription, "Text", "")})
        Me.XrLabel2.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 67.00001!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(1010.0!, 23.0!)
        Me.XrLabel2.StylePriority.UseFont = False
        Me.XrLabel2.StylePriority.UseTextAlignment = False
        Me.XrLabel2.Text = "XrLabel2"
        Me.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'DateDescription
        '
        Me.DateDescription.Description = "Parameter1"
        Me.DateDescription.Name = "DateDescription"
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.Header, "Text", "")})
        Me.XrLabel1.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(0.0!, 10.00001!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(1010.0!, 37.58334!)
        Me.XrLabel1.StylePriority.UseFont = False
        Me.XrLabel1.StylePriority.UseTextAlignment = False
        Me.XrLabel1.Text = "XrLabel1"
        Me.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter
        '
        'Header
        '
        Me.Header.Description = "Header"
        Me.Header.Name = "Header"
        '
        'BottomMargin
        '
        Me.BottomMargin.HeightF = 30.00005!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'AccountSet2
        '
        Me.AccountSet2.DataSetName = "AccountSet"
        Me.AccountSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'XrControlStyle1
        '
        Me.XrControlStyle1.BackColor = System.Drawing.Color.White
        Me.XrControlStyle1.BorderColor = System.Drawing.Color.Empty
        Me.XrControlStyle1.Name = "XrControlStyle1"
        Me.XrControlStyle1.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        '
        'CalculatedField1
        '
        Me.CalculatedField1.DataMember = "ReceiptDaily"
        Me.CalculatedField1.DisplayName = "SumDiscount"
        Me.CalculatedField1.Expression = "[].Sum([DISCOUNT])"
        Me.CalculatedField1.Name = "CalculatedField1"
        '
        'fieldName
        '
        Me.fieldName.Appearance.Cell.BackColor = System.Drawing.Color.White
        Me.fieldName.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldName.AreaIndex = 3
        Me.fieldName.FieldName = "Name"
        Me.fieldName.Name = "fieldName"
        Me.fieldName.Width = 120
        '
        'XrLabel3
        '
        Me.XrLabel3.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ReceiptDaily.CalculatedField1", "{0:n2}")})
        Me.XrLabel3.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(447.2917!, 55.95834!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(272.5!, 23.0!)
        Me.XrLabel3.StylePriority.UseFont = False
        Me.XrLabel3.Text = "XrLabel3"
        '
        'CalculatedField2
        '
        Me.CalculatedField2.DataMember = "ReceiptDaily"
        Me.CalculatedField2.DisplayName = "Sumtotal"
        Me.CalculatedField2.Expression = "[].Sum([TOTAL])"
        Me.CalculatedField2.Name = "CalculatedField2"
        '
        'XrLabel5
        '
        Me.XrLabel5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding("Text", Nothing, "ReceiptDaily.CalculatedField2", "{0:n2}")})
        Me.XrLabel5.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(447.2917!, 10.00001!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(272.5!, 23.0!)
        Me.XrLabel5.StylePriority.UseFont = False
        Me.XrLabel5.Text = "XrLabel5"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel8, Me.XrLabel7, Me.XrLabel6, Me.XrLabel4, Me.XrLabel5, Me.XrLabel3})
        Me.ReportFooter.HeightF = 100.0!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'XrLabel4
        '
        Me.XrLabel4.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(329.1667!, 10.00001!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(100.0!, 23.0!)
        Me.XrLabel4.StylePriority.UseFont = False
        Me.XrLabel4.Text = "ยอดรวมชำระ"
        '
        'XrLabel6
        '
        Me.XrLabel6.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(329.1667!, 55.95834!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(118.125!, 23.0!)
        Me.XrLabel6.StylePriority.UseFont = False
        Me.XrLabel6.Text = "ยอดรวมส่วนลด"
        '
        'XrLabel7
        '
        Me.XrLabel7.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(731.25!, 10.00001!)
        Me.XrLabel7.Multiline = True
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(118.125!, 23.0!)
        Me.XrLabel7.StylePriority.UseFont = False
        Me.XrLabel7.Text = "บาท" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'XrLabel8
        '
        Me.XrLabel8.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.XrLabel8.LocationFloat = New DevExpress.Utils.PointFloat(731.25!, 55.95834!)
        Me.XrLabel8.Multiline = True
        Me.XrLabel8.Name = "XrLabel8"
        Me.XrLabel8.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel8.SizeF = New System.Drawing.SizeF(118.125!, 23.0!)
        Me.XrLabel8.StylePriority.UseFont = False
        Me.XrLabel8.Text = "บาท" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'ReceiptDailyReport
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportFooter})
        Me.CalculatedFields.AddRange(New DevExpress.XtraReports.UI.CalculatedField() {Me.CalculatedField1, Me.CalculatedField2})
        Me.DataSource = Me.AccountSet1
        Me.Landscape = True
        Me.Margins = New System.Drawing.Printing.Margins(44, 46, 100, 30)
        Me.PageHeight = 850
        Me.PageWidth = 1100
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.Header, Me.DateDescription})
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.XrControlStyle1})
        Me.Version = "14.1"
        CType(Me.AccountSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AccountSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrPivotGrid1 As DevExpress.XtraReports.UI.XRPivotGrid
    Friend WithEvents AccountSet1 As FReport.AccountSet
    Friend WithEvents fieldHN1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldbilldocid1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldDISCOUNT1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldfrnamount1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldaccmname1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldCONTACT1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents AccountSet2 As FReport.AccountSet
    Friend WithEvents XrControlStyle1 As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Header As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents DateDescription As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents fieldTOTAL As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents CalculatedField1 As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents fieldName As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents CalculatedField2 As DevExpress.XtraReports.UI.CalculatedField
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel8 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
End Class
