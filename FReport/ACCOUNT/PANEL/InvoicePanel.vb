﻿Imports DevExpress.XtraReports.UI

Public Class InvoicePanel
    Dim dtset As AccountSet
    Public Property report As XtraReport
    Dim acclass As AccountClass
    Dim InvoiceCheck As Integer
    Public Sub New(ByVal Invoice As Integer)

        ' This call is required by the designer.
        InitializeComponent()
        InvoiceCheck = Invoice
        ' Add any initialization after the InitializeComponent() call.
        If InvoiceCheck = 1 Then
            LabelControl2.Text = "รายงานใบแจ้งนี้ประจำวัน IPD"
        ElseIf InvoiceCheck = 2 Then
            LabelControl2.Text = "รายงานใบแจ้งนี้ IPD ที่ยกเลิก"

        ElseIf InvoiceCheck = 3 Then
            LabelControl2.Text = "รายงานใบแจ้งนี้ประจำวัน OPD"

        ElseIf InvoiceCheck = 4 Then
            LabelControl2.Text = "รายงานใบแจ้งนี้ OPD ที่ยกเลิก"
        ElseIf InvoiceCheck = 5 Then
            LabelControl2.Text = "รายงานใบแจ้งนี้"
        ElseIf InvoiceCheck = 6 Then
            LabelControl2.Text = "รายงานใบแจ้งนี้ที่ยกเลิก"

        End If

    End Sub

    Private Sub btnApplyFilter_Click(sender As Object, e As EventArgs) Handles btnApplyFilter.Click
        If InvoiceCheck = 1 Then
            acclass.getInvoiceDailyIPD(dateStart.EditValue, dateTo.EditValue)
            report = New InvoiceDailyReport
            report.DataSource = dtset
            report.Parameters("Header").Value = "รายงานใบแจ้งนี้ประจำวัน IPD"
            report.Parameters("DateDescription").Value = "ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text

            report.CreateDocument()
            DocumentViewer1.DocumentSource = report
        ElseIf InvoiceCheck = 2 Then
            acclass.getInvoiceDailyCancelIPD(dateStart.EditValue, dateTo.EditValue)
            report = New InvoiceDailyReport
            report.DataSource = dtset
            report.Parameters("Header").Value = "รายงานใบแจ้งนี้ IPD ที่ยกเลิก"
            report.Parameters("DateDescription").Value = "ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text
            report.CreateDocument()
            DocumentViewer1.DocumentSource = report
        ElseIf InvoiceCheck = 3 Then
            acclass.getInvoiceDailyOPD(dateStart.EditValue, dateTo.EditValue)
            report = New InvoiceDailyReport
            report.DataSource = dtset
            report.Parameters("Header").Value = "รายงานใบแจ้งนี้ประจำวัน OPD"
            report.Parameters("DateDescription").Value = "ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text
            report.CreateDocument()
            DocumentViewer1.DocumentSource = report
        ElseIf InvoiceCheck = 4 Then
            acclass.getInvoiceDailyCancelOPD(dateStart.EditValue, dateTo.EditValue)
            report = New InvoiceDailyReport
            report.DataSource = dtset
            report.Parameters("Header").Value = "รายงานใบแจ้งนี้ OPD ที่ยกเลิก"
            report.Parameters("DateDescription").Value = "ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text
            report.CreateDocument()
            DocumentViewer1.DocumentSource = report
        ElseIf InvoiceCheck = 5 Then
            acclass.getInvoiceDaily(dateStart.EditValue, dateTo.EditValue)
            report = New InvoiceDailyReport
            report.DataSource = dtset
            report.Parameters("Header").Value = "รายงานใบแจ้งนี้ประจำวัน"
            report.Parameters("DateDescription").Value = "ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text
            report.CreateDocument()
            DocumentViewer1.DocumentSource = report
        ElseIf InvoiceCheck = 6 Then

            acclass.getInvoiceDailyCancel(dateStart.EditValue, dateTo.EditValue)
            report = New InvoiceDailyReport
            report.DataSource = dtset
            report.Parameters("Header").Value = "รายงานใบแจ้งนี้ประจำวัน"
            report.Parameters("DateDescription").Value = "ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text
            report.CreateDocument()
            DocumentViewer1.DocumentSource = report
        End If

    End Sub

    Private Sub InvoicePanel_Load(sender As Object, e As EventArgs) Handles Me.Load
        dateStart.EditValue = Now
        dateTo.EditValue = Now
        dtset = New AccountSet
        acclass = New AccountClass(dtset)
    End Sub

    Private Sub dateStart_EditValueChanged(sender As Object, e As EventArgs) Handles dateStart.EditValueChanged

    End Sub

    Private Sub dateTo_EditValueChanged(sender As Object, e As EventArgs) Handles dateTo.EditValueChanged

    End Sub

    Private Sub DocumentViewer1_Load(sender As Object, e As EventArgs) Handles DocumentViewer1.Load

    End Sub

    Private Sub LayoutControl1_Click(sender As Object, e As EventArgs) Handles LayoutControl1.Click

    End Sub

    Private Sub LabelControl2_Click(sender As Object, e As EventArgs) Handles LabelControl2.Click

    End Sub
End Class
