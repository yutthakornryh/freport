﻿Imports DevExpress.XtraReports.UI

Public Class ReceiptPanel
    Dim dtset As New AccountSet
    Public Property report As XtraReport
    Dim acclass As AccountClass
    Dim recieptCheck As Integer
    Public Sub New(ByVal receiptid As Integer)

        ' This call is required by the designer.
        InitializeComponent()
        recieptCheck = receiptid
        ' Add any initialization after the InitializeComponent() call.
        If recieptCheck = 1 Then

            LabelControl1.Text = "รายงานทางการเงิน ( ใบนำส่งเงิน ) ผู้ป่วยนอก"
        ElseIf recieptCheck = 2 Then
            LabelControl1.Text = "รายงานทางการเงิน ( ใบนำส่งเงิน ) ผู้ป่วยใน"


        End If

    End Sub

    Private Sub ReceiptPanel_Load(sender As Object, e As EventArgs) Handles Me.Load

        dateStart.EditValue = Now
        dateTo.EditValue = Now


    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles btnApplyFilter.Click

        acclass = New AccountClass(dtset)
        report = New ReceiptDailyReport
        If recieptCheck = 1 Then
            acclass.getReceiptOPD(dateStart.EditValue, dateTo.EditValue)

            report.DataSource = dtset
            report.Parameters("Header").Value = "รายงานทางการเงิน ( ใบนำส่งเงิน ) ผู้ป่วยนอก"
            report.Parameters("DateDescription").Value = "ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text
        ElseIf recieptCheck = 2 Then
            acclass.getReceiptIPD(dateStart.EditValue, dateTo.EditValue)

            report.DataSource = dtset
            report.Parameters("Header").Value = "รายงานทางการเงิน ( ใบนำส่งเงิน ) ผู้ป่วยใน"
            report.Parameters("DateDescription").Value = "ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text
        ElseIf recieptCheck = 3 Then
            acclass.getReceiptCancelIPD(dateStart.EditValue, dateTo.EditValue)

            report.DataSource = dtset
            report.Parameters("Header").Value = "รายงานทางการเงิน ( ใบนำส่งเงิน ) ยกเลิก  ผู้ป่วยใน"
            report.Parameters("DateDescription").Value = "ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text
        ElseIf recieptCheck = 4 Then
            acclass.getReceiptCancelOPD(dateStart.EditValue, dateTo.EditValue)

            report.DataSource = dtset
            report.Parameters("Header").Value = "รายงานทางการเงิน ( ใบนำส่งเงิน ) ยกเลิก ผู้ป่วยนอก"
            report.Parameters("DateDescription").Value = "ตั้งแต่วันที่ " + dateStart.Text + "  ถึง วันที่ " + dateTo.Text


        End If

        report.CreateDocument()
        DocumentViewer1.DocumentSource = report
    End Sub
End Class
