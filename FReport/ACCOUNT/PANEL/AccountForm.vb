﻿Imports DevExpress.XtraReports.UI

Public Class AccountForm
    Dim report As XtraReport
    Dim InvPanel As InvoicePanel
    Dim Receipt As ReceiptPanel
    Dim localPath As String = "F://FProject//"
    Dim reportname As String
    Private Sub AccountForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub RibbonControl1_Click(sender As Object, e As EventArgs) Handles RibbonControl1.Click

    End Sub

    Private Sub NavBarItem1_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem1.LinkClicked

        InvPanel = New InvoicePanel(1)
        AddHandler InvPanel.btnApplyFilter.Click, AddressOf InvoicePanel
        InvPanel.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(InvPanel)
        reportname = "Invoice"
    End Sub
    Private Sub InvoicePanel(sender As Object, e As EventArgs)
        Me.report = InvPanel.report
        'RemoveHandler bj8Panel.btnApplyFilter.Click, AddressOf bj8PanelShowing
    End Sub

    Private Sub ReceiptPanel(sender As Object, e As EventArgs)
        Me.report = Receipt.report
        'RemoveHandler bj8Panel.btnApplyFilter.Click, AddressOf bj8PanelShowing
    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        If report IsNot Nothing Then
            FolderBrowserDialog1.Description = "Pick Folder to store Excecl files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                ExportReport(report, FolderBrowserDialog1.SelectedPath & "\" & reportname & ".xls", "xls")

            End If
        End If
    End Sub

    Private Sub BarButtonItem6_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        If report IsNot Nothing Then
            FolderBrowserDialog1.Description = "Pick Folder to store PDF files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                ExportReport(report, FolderBrowserDialog1.SelectedPath & "\" & reportname & ".pdf", "pdf")

            End If
        End If
    End Sub

    Private Sub BarButtonItem3_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs)
        If report IsNot Nothing Then
            FolderBrowserDialog1.Description = "Pick Folder to store jpg files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                ExportReport(report, FolderBrowserDialog1.SelectedPath & "\" & reportname & ".jpg", "jpg")

            End If
        End If
    End Sub

    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        If report IsNot Nothing Then
            Dim printTool As New ReportPrintTool(report)
            printTool.PrintDialog()
        End If
    End Sub

    Private Sub BarButtonItem4_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        If report IsNot Nothing Then
            Dim printTool As New ReportPrintTool(report)
            printTool.Print()
            printTool.PrintingSystem.ShowMarginsWarning = False
        End If
    End Sub



    Private Sub NavBarItem5_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem5.LinkClicked
        InvPanel = New InvoicePanel(4)
        AddHandler InvPanel.btnApplyFilter.Click, AddressOf InvoicePanel
        InvPanel.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(InvPanel)
        reportname = "Invoice"
    End Sub

    Private Sub NavBarItem6_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem6.LinkClicked
        InvPanel = New InvoicePanel(3)
        AddHandler InvPanel.btnApplyFilter.Click, AddressOf InvoicePanel
        InvPanel.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(InvPanel)
        reportname = "Invoice"
    End Sub

    Private Sub NavBarItem7_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem7.LinkClicked
        InvPanel = New InvoicePanel(2)
        AddHandler InvPanel.btnApplyFilter.Click, AddressOf InvoicePanel
        InvPanel.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(InvPanel)
        reportname = "Invoice"
    End Sub

    Private Sub NavBarItem2_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs)

    End Sub

    Private Sub NavBarItem3_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem3.LinkClicked
        Receipt = New ReceiptPanel(2)
        AddHandler Receipt.btnApplyFilter.Click, AddressOf ReceiptPanel
        Receipt.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(Receipt)
        reportname = "Receipt"
    End Sub

    Private Sub NavBarItem4_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem4.LinkClicked
        Receipt = New ReceiptPanel(1)
        AddHandler Receipt.btnApplyFilter.Click, AddressOf ReceiptPanel
        Receipt.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(Receipt)
        reportname = "Receipt"
    End Sub

    Private Sub NavBarControl1_Click(sender As Object, e As EventArgs) Handles NavBarControl1.Click
        InvPanel = New InvoicePanel(5)
        AddHandler InvPanel.btnApplyFilter.Click, AddressOf InvoicePanel
        InvPanel.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(InvPanel)
        reportname = "Invoice"
    End Sub

    Private Sub NavBarItem8_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem8.LinkClicked
        Receipt = New ReceiptPanel(3)
        AddHandler Receipt.btnApplyFilter.Click, AddressOf ReceiptPanel
        Receipt.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(Receipt)
        reportname = "Receipt"
    End Sub

    Private Sub NavBarItem2_LinkClicked_1(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem2.LinkClicked
        Receipt = New ReceiptPanel(4)
        AddHandler Receipt.btnApplyFilter.Click, AddressOf ReceiptPanel
        Receipt.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(Receipt)
        reportname = "Receipt"
    End Sub

    Private Sub NavBarItem10_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem10.LinkClicked
        InvPanel = New InvoicePanel(6)
        AddHandler InvPanel.btnApplyFilter.Click, AddressOf InvoicePanel
        InvPanel.Dock = DockStyle.Fill
        ReportPanel.Controls.Clear()
        ReportPanel.Controls.Add(InvPanel)
        reportname = "Invoice"
    End Sub
End Class