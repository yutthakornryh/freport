﻿Public Class AccountClass
    Dim dtset As AccountSet
    Dim connect As ConnecDBRYH

    Public Sub New(ByRef dset As AccountSet)
        dtset = dset
        connect = ConnecDBRYH.NewConnection

    End Sub
    Public Sub getInvoiceDailyOPD(Start As Date, Todate As Date)
        Dim sql As String
        ' MsgBox(Start.Year)
        sql = "SELECT  invoicehd.invdocid AS INVOICEID ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,invoicehd.amount,discount,total,CONCAT_WS('', company_name , ' ' ,contact.conname  ) AS CONTACT ,dateserv  ,CONCAT_WS('',hospemp.name , ' ' , hospemp.lname ) AS usmkname  FROM  (SELECT * FROM invoicehd WHERE dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " " & Start.ToString("HH:mm") & ":00" & "'  AND dateserv < '" & Todate.Year & Todate.ToString("-MM-dd") & " " & Todate.ToString("HH:mm") & ":59" & "' AND f_cancel = 0 AND an is null  )  AS invoicehd JOIN  (SELECT conname,pconid FROM  contact ) AS contact   ON  invoicehd.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON invoicehd.hn	 = person.hn JOIN masprename ON masprename.PRENAME = person.prename JOIN hospemp ON hospemp.empid =  invoicehd.empid    JOIN masptypergt ON (coalesce(invoicehd.mtrgtid, '') = coalesce(masptypergt.mtrgtid, '')) AND (coalesce(invoicehd.strgtid, '') = coalesce(masptypergt.strgtid, ''))AND (coalesce(invoicehd.sbrgtid, '') = coalesce(masptypergt.sbrgtid, ''))  JOIN agent ON agent.agentcode = masptypergt.agentcode ;"
        dtset.Tables("InvoiceDaily").Clear()
        connect.GetTable(sql, dtset.Tables("InvoiceDaily"))
    End Sub
    Public Sub getInvoiceDailyIPD(Start As Date, Todate As Date)
        Dim sql As String
        ' MsgBox(Start.Year)
        sql = "SELECT  invoicehd.invdocid AS INVOICEID ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,invoicehd.amount,discount,total,CONCAT_WS('', company_name , ' ' ,contact.conname  ) AS CONTACT ,dateserv  ,CONCAT_WS('',hospemp.name , ' ' , hospemp.lname ) AS usmkname  FROM  (SELECT * FROM invoicehd WHERE dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " " & Start.ToString("HH:mm") & ":00" & "'  AND dateserv < '" & Todate.Year & Todate.ToString("-MM-dd") & " " & Todate.ToString("HH:mm") & ":59" & "' AND f_cancel = 0 AND vn is null )  AS invoicehd JOIN  (SELECT conname,pconid FROM  contact ) AS contact ON  invoicehd.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON invoicehd.hn	 = person.hn JOIN masprename ON masprename.PRENAME = person.prename JOIN hospemp ON hospemp.empid =  invoicehd.empid JOIN masptypergt ON (coalesce(invoicehd.mtrgtid, '') = coalesce(masptypergt.mtrgtid, '')) AND (coalesce(invoicehd.strgtid, '') = coalesce(masptypergt.strgtid, ''))AND (coalesce(invoicehd.sbrgtid, '') = coalesce(masptypergt.sbrgtid, ''))  JOIN agent ON agent.agentcode = masptypergt.agentcode ;"
        dtset.Tables("InvoiceDaily").Clear()
        connect.GetTable(sql, dtset.Tables("InvoiceDaily"))
    End Sub

    Public Sub getInvoiceDaily(Start As Date, Todate As Date)
        Dim sql As String
  

        sql = "SELECT  invoicehd.invdocid AS INVOICEID ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,invoicehd.amount,discount,total,CONCAT_WS('', company_name , ' ' ,contact.conname  ) AS CONTACT ,dateserv  ,CONCAT_WS('',hospemp.name , ' ' , hospemp.lname ) AS usmkname , bedscode AS bed , CASE WHEN(invoicehd.an IS NULL) THEN 'OPD' ELSE 'IPD' END AS IO     FROM  (SELECT * FROM invoicehd WHERE f_cancel = 0 AND dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " " & Start.ToString("HH:mm") & ":00" & "'     AND dateserv < '" & Todate.Year & Todate.ToString("-MM-dd") & " " & Todate.ToString("HH:mm") & ":59" & "'  )  AS invoicehd JOIN  (SELECT conname,pconid FROM  contact ) AS contact ON  invoicehd.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON invoicehd.hn	 = person.hn JOIN masprename ON masprename.PRENAME = person.prename JOIN hospemp ON hospemp.empid =  invoicehd.empid JOIN masptypergt ON (coalesce(invoicehd.mtrgtid, '') = coalesce(masptypergt.mtrgtid, '')) AND (coalesce(invoicehd.strgtid, '') = coalesce(masptypergt.strgtid, ''))AND (coalesce(invoicehd.sbrgtid, '') = coalesce(masptypergt.sbrgtid, ''))  JOIN agent ON agent.agentcode = masptypergt.agentcode LEFT JOIN  (SELECT * FROM frnshift  GROUP BY an   ) AS frnshift  ON invoicehd.an = frnshift.an LEFT JOIN sroomitem ON sroomitem.bedsid = frnshift.bedsid  ORDER BY invdocid ; "
        dtset.Tables("InvoiceDaily").Clear()
        connect.GetTable(sql, dtset.Tables("InvoiceDaily"))
    End Sub
    Public Sub getInvoiceDailyCancel(Start As Date, Todate As Date)
        Dim sql As String


        sql = "SELECT  invoicehd.invdocid AS INVOICEID ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,invoicehd.amount,discount,total,CONCAT_WS('', company_name , ' ' ,contact.conname  ) AS CONTACT ,dateserv  ,CONCAT_WS('',hospemp.name , ' ' , hospemp.lname ) AS usmkname , bedscode AS bed , CASE WHEN(invoicehd.an IS NULL) THEN 'OPD' ELSE 'IPD' END AS IO     FROM  (SELECT * FROM invoicehd WHERE f_cancel = 1 AND dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " " & Start.ToString("HH:mm") & ":00" & "'     AND dateserv < '" & Todate.Year & Todate.ToString("-MM-dd") & " " & Todate.ToString("HH:mm") & ":59" & "'  )  AS invoicehd JOIN  (SELECT conname,pconid FROM  contact ) AS contact ON  invoicehd.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON invoicehd.hn	 = person.hn JOIN masprename ON masprename.PRENAME = person.prename JOIN hospemp ON hospemp.empid =  invoicehd.empid JOIN masptypergt ON (coalesce(invoicehd.mtrgtid, '') = coalesce(masptypergt.mtrgtid, '')) AND (coalesce(invoicehd.strgtid, '') = coalesce(masptypergt.strgtid, ''))AND (coalesce(invoicehd.sbrgtid, '') = coalesce(masptypergt.sbrgtid, ''))  JOIN agent ON agent.agentcode = masptypergt.agentcode LEFT JOIN  (SELECT * FROM frnshift  GROUP BY an   ) AS frnshift  ON invoicehd.an = frnshift.an LEFT JOIN sroomitem ON sroomitem.bedsid = frnshift.bedsid  ORDER BY invdocid ; "
        dtset.Tables("InvoiceDaily").Clear()
        connect.GetTable(sql, dtset.Tables("InvoiceDaily"))
    End Sub
    Public Sub InvoiceDebt(Start As Date, Todate As Date, AgentCode As String)
        Dim sql As String

        sql = "SELECT  invoicehd.invdocid AS INVOICEID ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,invoicehd.amount,discount,total,CONCAT_WS('', company_name , ' ' ,contact.conname  ) AS CONTACT ,dateserv  FROM  (SELECT * FROM invoicehd WHERE dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " " & Start.ToString("HH:mm") & ":00" & "'  AND dateserv < '" & Todate.Year & Todate.ToString("-MM-dd") & " " & Todate.ToString("HH:mm") & ":59" & "' AND f_cancel = 0  )    AS invoicehd JOIN  (SELECT conname,pconid FROM  contact ) AS contact   ON  invoicehd.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON invoicehd.hn	 = person.hn LEFT JOIN masprename ON masprename.PRENAME = person.prename  JOIN masptypergt ON (coalesce(invoicehd.mtrgtid, '') = coalesce(masptypergt.mtrgtid, '')) AND (coalesce(invoicehd.strgtid, '') = coalesce(masptypergt.strgtid, ''))AND (coalesce(invoicehd.sbrgtid, '') = coalesce(masptypergt.sbrgtid, ''))  JOIN agent ON agent.agentcode = masptypergt.agentcode ORDER BY invdocid ;"
        dtset.Tables("InvoiceDaily").Clear()
        connect.GetTable(Sql, dtset.Tables("InvoiceDaily"))
    End Sub
    Public Sub getAgent()
        Dim sql As String
        sql = "SELECT agentcode,company_name FROM agent WHERE agentype = 4;"
        dtset.Tables("Agent").Clear()
        connect.GetTable(sql, dtset.Tables("Agent"))
    End Sub

    Public Sub getInvoiceDailyCancelIPD(Start As Date, Todate As Date)
        Dim sql As String

        sql = "SELECT  invoicehd.invdocid AS INVOICEID ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,invoicehd.amount,discount,total,CONCAT_WS('', company_name , ' ' ,contact.conname  ) AS CONTACT ,dateserv  ,CONCAT_WS('',hospemp.name , ' ' , hospemp.lname ) AS usmkname FROM  (SELECT * FROM invoicehd WHERE dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " " & Start.ToString("HH:mm") & ":00" & "'  AND dateserv < '" & Todate.Year & Todate.ToString("-MM-dd") & " " & Todate.ToString("HH:mm") & ":59" & "' AND f_cancel = 0 AND vn is null )    AS invoicehd JOIN  (SELECT conname,pconid FROM  contact ) AS contact   ON  invoicehd.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON invoicehd.hn	 = person.hn LEFT JOIN masprename ON masprename.PRENAME = person.prename   JOIN hospemp ON hospemp.empid =  invoicehd.empid  JOIN masptypergt ON (coalesce(invoicehd.mtrgtid, '') = coalesce(masptypergt.mtrgtid, '')) AND (coalesce(invoicehd.strgtid, '') = coalesce(masptypergt.strgtid, ''))AND (coalesce(invoicehd.sbrgtid, '') = coalesce(masptypergt.sbrgtid, ''))  JOIN agent ON agent.agentcode = masptypergt.agentcode  ;"
        dtset.Tables("InvoiceDaily").Clear()
        connect.GetTable(sql, dtset.Tables("InvoiceDaily"))
    End Sub
    Public Sub getInvoiceDailyCancelOPD(Start As Date, Todate As Date)
        Dim sql As String

        sql = "SELECT  invoicehd.invdocid AS INVOICEID ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,invoicehd.amount,discount,total,CONCAT_WS('', company_name , ' ' ,contact.conname  )  AS CONTACT ,dateserv  ,CONCAT_WS('',hospemp.name , ' ' , hospemp.lname ) AS usmkname  FROM  (SELECT * FROM invoicehd WHERE dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " " & Start.ToString("HH:mm") & ":00" & "'  AND dateserv < '" & Todate.Year & Todate.ToString("-MM-dd") & " " & Todate.ToString("HH:mm") & ":59" & "' AND f_cancel = 0 AND an is null )   AS invoicehd JOIN  (SELECT conname,pconid FROM  contact ) AS contact   ON  invoicehd.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON invoicehd.hn	 = person.hn LEFT JOIN masprename ON masprename.PRENAME = person.prename   JOIN hospemp ON hospemp.empid =  invoicehd.empid   JOIN masptypergt ON (coalesce(invoicehd.mtrgtid, '') = coalesce(masptypergt.mtrgtid, '')) AND (coalesce(invoicehd.strgtid, '') = coalesce(masptypergt.strgtid, ''))AND (coalesce(invoicehd.sbrgtid, '') = coalesce(masptypergt.sbrgtid, ''))  JOIN agent ON agent.agentcode = masptypergt.agentcode ;"
        dtset.Tables("InvoiceDaily").Clear()
        connect.GetTable(sql, dtset.Tables("InvoiceDaily"))
    End Sub


    Public Sub getReceiptOPD(Start As Date, Todate As Date)
        Dim sql As String
        sql = "SELECT  receipthd.BILLDOCID AS billdocid ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,receipthd.amount,discount,total,CONCAT_WS(' ' ,contact.conname )  AS CONTACT ,dateserv , frnreceipt.ACCMID,frnreceipt.amount AS 'frnamount' , accm_name  ,CONCAT_WS('',hospemp.name , ' ' , hospemp.lname ) AS usmkname  FROM  (SELECT * FROM receipthd WHERE dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " " & Start.ToString("HH:mm") & ":00" & "'  AND dateserv < '" & Todate.Year & Todate.ToString("-MM-dd") & " " & Todate.ToString("HH:mm") & ":59" & "' AND f_cancel = 0 AND an is null )     AS receipthd  JOIN  (SELECT conname,pconid FROM  contact ) AS contact   ON  receipthd.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON receipthd.hn	 = person.hn LEFT JOIN masprename ON masprename.PRENAME = person.prename  JOIN frnreceipt ON frnreceipt.BILLDOCID = receipthd.BILLDOCID JOIN masacceptm ON masacceptm.ACCMID = frnreceipt.ACCMID JOIN hospemp ON hospemp.empid =  frnreceipt.empid   ;"
        dtset.Tables("ReceiptDaily").Clear()
        connect.GetTable(sql, dtset.Tables("ReceiptDaily"))
    End Sub
    Public Sub getReceiptCancelOPD(Start As Date, Todate As Date)
        Dim sql As String
        sql = "SELECT  receipthd.BILLDOCID AS billdocid ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,receipthd.amount,discount,total,CONCAT_WS(' ' ,contact.conname )  AS CONTACT ,dateserv , frnreceipt.ACCMID,frnreceipt.amount AS 'frnamount' , accm_name  ,CONCAT_WS('',hospemp.name , ' ' , hospemp.lname ) AS usmkname  FROM  (SELECT * FROM receipthd WHERE dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " " & Start.ToString("HH:mm") & ":00" & "'  AND dateserv < '" & Todate.Year & Todate.ToString("-MM-dd") & " " & Todate.ToString("HH:mm") & ":59" & "' AND f_cancel = 1 AND an is null )     AS receipthd  JOIN  (SELECT conname,pconid FROM  contact ) AS contact   ON  receipthd.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON receipthd.hn	 = person.hn LEFT JOIN masprename ON masprename.PRENAME = person.prename  JOIN frnreceipt ON frnreceipt.BILLDOCID = receipthd.BILLDOCID JOIN masacceptm ON masacceptm.ACCMID = frnreceipt.ACCMID JOIN hospemp ON hospemp.empid =  frnreceipt.empid   ;"
        dtset.Tables("ReceiptDaily").Clear()
        connect.GetTable(sql, dtset.Tables("ReceiptDaily"))
    End Sub
    Public Sub getReceiptIPD(Start As Date, Todate As Date)

        Dim sql As String
        sql = "SELECT  receipthd.BILLDOCID AS billdocid ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,receipthd.amount,discount,total,CONCAT_WS('',contact.conname )  AS CONTACT ,dateserv , frnreceipt.ACCMID,frnreceipt.amount AS 'frnamount' , accm_name   ,CONCAT_WS('',hospemp.name , ' ' , hospemp.lname ) AS usmkname  FROM  (SELECT * FROM receipthd WHERE dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " " & Start.ToString("HH:mm") & ":00" & "'  AND dateserv < '" & Todate.Year & Todate.ToString("-MM-dd") & " " & Todate.ToString("HH:mm") & ":59" & "' AND f_cancel = 0 AND vn is null   )  AS receipthd JOIN  (SELECT conname,pconid FROM  contact ) AS contact   ON  receipthd.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON receipthd.hn	 = person.hn LEFT JOIN masprename ON masprename.PRENAME = person.prename  JOIN frnreceipt ON frnreceipt.BILLDOCID = receipthd.BILLDOCID JOIN masacceptm ON masacceptm.ACCMID = frnreceipt.ACCMID  JOIN hospemp ON hospemp.empid =  frnreceipt.empid   ;"
        dtset.Tables("ReceiptDaily").Clear()
        connect.GetTable(sql, dtset.Tables("ReceiptDaily"))
    End Sub

    Public Sub getReceiptCancelIPD(Start As Date, Todate As Date)

        Dim sql As String
        sql = "SELECT  receipthd.BILLDOCID AS billdocid ,person.hn, CONCAT_WS('',ftprename, ' ',person.name,' ',person.lname) as name,receipthd.amount,discount,total,CONCAT_WS('',contact.conname )  AS CONTACT ,dateserv , frnreceipt.ACCMID,frnreceipt.amount AS 'frnamount' , accm_name   ,CONCAT_WS('',hospemp.name , ' ' , hospemp.lname ) AS usmkname  FROM  (SELECT * FROM receipthd WHERE dateserv >'" & Start.Year & Start.ToString("-MM-dd") & " " & Start.ToString("HH:mm") & ":00" & "'  AND dateserv < '" & Todate.Year & Todate.ToString("-MM-dd") & " " & Todate.ToString("HH:mm") & ":59" & "' AND f_cancel = 1 AND vn is null   )  AS receipthd JOIN  (SELECT conname,pconid FROM  contact ) AS contact   ON  receipthd.pconid = contact.PCONID JOIN  ( SELECT hn,prename,name,lname  FROM person ) AS person  ON receipthd.hn	 = person.hn LEFT JOIN masprename ON masprename.PRENAME = person.prename  JOIN frnreceipt ON frnreceipt.BILLDOCID = receipthd.BILLDOCID JOIN masacceptm ON masacceptm.ACCMID = frnreceipt.ACCMID  JOIN hospemp ON hospemp.empid =  frnreceipt.empid   ;"
        dtset.Tables("ReceiptDaily").Clear()
        connect.GetTable(sql, dtset.Tables("ReceiptDaily"))
    End Sub
    Public Sub getProfitWard(Todate As Date)
        Dim sql As String
        sql = "SELECT an,hn FROM frnadmission  WHERE datetime_disch IS NULL;"
        Dim dt As New DataTable
        dt = connect.GetTable(sql)
        For i As Integer = 0 To dt.Rows.Count - 1

            sql = "SELECT * FROM "
        Next

    End Sub
End Class
